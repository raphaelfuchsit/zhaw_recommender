# Install steps:
1. `npm install -g @vue/cli`
2. `npm i`
3. `npm run serve`

# dor-webapp

## Front-End Documentation
The Web-Application is built with Vue.js. 
The system for development is the Vue CLI, which allows to setup a Single Page App with Vue.
For more documentation, see the following page: https://cli.vuejs.org/guide/

### Installation of Vue & Yarn
To further develop this Application, the use of the Vue CLI is needed.

The following **requirement** is needed for the following steps: **Node.js**

Download & Install: https://nodejs.org/en/


The following setup will install Vue and yarn globally on your computer (administrator privileges are needed):

`npm install -g @vue/cli`

`npm install -g yarn`

### Local Development / Start the application
To start the application, you have to switch to the directory "dor-webapp" (because of the package.json).
After that, you can use the script "serve" to start the Node.js development server on your localhost. 
When you start it for the first time, you need to install the needed dependencies first (yarn install).

`cd dor-webapp`

`yarn install`

`yarn serve`

With the command `vue ui` a UI in the Browser will be opened that allows you to manage the application.

### Use of Mock Data
When you want to use only the Web-Application without real data and calculations, then you can use mocked example data.
To enable them, go to the following file:

`/src/api`

There you can set following property to true:

`let isUsingMockData = true`

With that, an API for all the GET-Requests will be used, that gets data from local JSON files.
These files are saved at the following place: 

`/public/mock-data`

### Deployment on a Server
To deploy this application on a server, you need to create a production build first:

```
cd dor-webapp
yarn run build
```

This will create a "dist" folder, which has all the needed files for a deployment. 
It could be deployed on Netlify for example: [Netlify Deployment](https://docs.netlify.com/site-deploys/create-deploys/)

### Deployment on a Heroku

1. checkout heroku git repository

git clone https://git.heroku.com/zhaw-dor.git

2. run deployment using heroku_deploy.sh script:

./dor-webapp/heroku_deploy.sh local