#!/bin/bash
APP="dor-webapp"

# vars for local heroku deployment
eval LOCAL_HEROKU_REPO="~/heroku/zhaw-dor"
eval LOCAL_GIT_REPO="~/git/digital-online-recommender"

echo "creating heroku artefact for app: $APP"

if [ "$1" == "" ]; then
	cp -vp $APP/heroku/* $APP/
	tar -C $APP --exclude='./heroku*' -czvf $APP.tgz ./
else
	# cleanup heroku app directory
	echo "starting local heroku deployment using heroku app directory: $LOCAL_HEROKU_REPO"
	rm -R $LOCAL_HEROKU_REPO/*
	
	# prepare heroku app directory
	cp -vRp $LOCAL_GIT_REPO/$APP/* $LOCAL_HEROKU_REPO/
    cp -vp  $LOCAL_HEROKU_REPO/heroku/* $LOCAL_HEROKU_REPO
    rm -R   $LOCAL_HEROKU_REPO/heroku*
    
    echo "going to deploy app: $APP"
    
    # starting heroku deployment
    cd $LOCAL_HEROKU_REPO
    git add .
    git commit . -m "deployment dor-webapp"
    git push https://git.heroku.com/zhaw-dor.git master
fi

echo "created heroku artefact for app: $APP"

exit 0