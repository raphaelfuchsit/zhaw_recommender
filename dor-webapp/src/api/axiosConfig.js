import axios from 'axios'

/* API for Live-Data from the deployed Back-End on Heroku */
let herokuApi = axios.create({
    baseURL: 'https://dor-backend-2.herokuapp.com/',
    auth: {username: 'dor-admin', password: '%R[9w756V.3R2jtQ'}
});

/* API for Mock-Data */
let mockApi = axios.create({
    baseURL: '/mock-data'
});

let isUsingMockData = false; // Set this flag to true, to use the Application with static Mock-Data.

export {
    herokuApi,
    mockApi,
    isUsingMockData
}