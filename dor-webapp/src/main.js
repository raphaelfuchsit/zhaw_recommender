import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faCheck,
    faChevronDown,
    faChevronLeft,
    faChevronRight,
    faChevronUp,
    faEdit,
    faEnvelope,
    faExclamationTriangle,
    faExternalLinkAlt,
    faFrown,
    faInfoCircle,
    faMeh,
    faPollH,
    faPrint,
    faSmile,
    faStar as fasStar,
    faStarHalfAlt,
    faTimes,
    faUserCheck,
    faVoteYea
} from '@fortawesome/free-solid-svg-icons'
import {faStar as farStar} from "@fortawesome/free-regular-svg-icons"
import {faChrome, faEdge, faFirefox, faInternetExplorer} from "@fortawesome/free-brands-svg-icons"
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome"

import store from "./state-management/store"

import ConsultationStart from "./components/ConsultationStart"
import NotFound from "./components/helper-components/NotFound"
import InitialForm from "./components/InitialForm"
import QuestionPage from "./components/QuestionPage"
import Results from "./components/result-page/Results"
import ContactForm from "./components/ContactForm";
import AcceptanceCriteria from "./components/AcceptanceCriteria";
import Feedback from "./components/Feedback";

Vue.use(BootstrapVue);
Vue.use(VueRouter);

/* FontAwesome library */
library.add(faPrint);
library.add(faEnvelope);
library.add(faChevronLeft);
library.add(faChevronRight);
library.add(faStarHalfAlt);
library.add(faVoteYea);
library.add(faTimes);
library.add(faPollH);
library.add(faInfoCircle);
library.add(faChevronUp);
library.add(faChevronDown);
library.add(faExternalLinkAlt);
library.add(faSmile);
library.add(faMeh);
library.add(faFrown);
library.add(faCheck);
library.add(faUserCheck);
library.add(faEdit);
library.add(faExclamationTriangle, fasStar, farStar);
library.add(faEdge, faFirefox, faChrome, faInternetExplorer);
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

const routes = [
    {path: '/', redirect: '/start'},
    {path: '/start', component: ConsultationStart},
    {path: '/initiale-angaben', name:'initial',  component: InitialForm},
    {path: '/zulassungskriterien', component: AcceptanceCriteria},
    {path: '/page/:id', name: 'page', component: QuestionPage},
    {path: '/ergebnisse', name: 'result', component: Results},
    {path: '/feedback', component: Feedback},
    {path: '/kontakt', name: 'contact', component: ContactForm},
    {path: '*', component: NotFound}
];

const router = new VueRouter({
    routes
});

new Vue({
    router,
    data: {
        sharedState: store.state
    },
    render: h => h(App),
}).$mount('#app');