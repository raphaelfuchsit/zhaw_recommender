let store = {
    debug: false,
    version: 2.1,
    state: {
        isInitSessionFinished: null,
        tableNumber: 1,
        progressValue: 0,
        initialForm: {
            selectedProfessionalField: null,
            selectedProfessionalExperience: null,
            selectedFurtherEducate: null,
            selectedGender: null,
        },
        isIE11: false,
        recommenderResult: null,
        recommenderResultOutput: null,
        isLoading: false
    },
    overallSteps: 100/14,
    setIsInitSessionFinished (newValue) {
        /* eslint-disable no-console */
        if (this.debug) console.log('setIsInitSessionFinished triggered with', newValue)
        this.state.isInitSessionFinished = newValue
    },
    setTableNumber (newValue) {
        this.state.tableNumber = newValue
    },
    increaseTableNumber (value) {
        this.state.tableNumber += value
    },
    decreaseTableNumber (value) {
        this.state.tableNumber -= value
    },
    getProgressValue () {
        return this.state.progressValue;
    },
    setProgressValue (newValue) {
        this.state.progressValue = newValue
    },
    increaseProgressValue (value){
        console.log(value)
        this.state.progressValue += this.overallSteps;
    },
    decreaseProgressValue (value) {
        console.log(value)
        this.state.progressValue -= this.overallSteps;
    },
    setInitialForm (professionalField, professionalExp, furtherEducate, gender) {
        this.state.initialForm.selectedProfessionalField = professionalField;
        this.state.initialForm.selectedProfessionalExperience = professionalExp;
        this.state.initialForm.selectedFurtherEducate = furtherEducate;
        this.state.initialForm.selectedGender = gender;
    },
    resetInitialForm () {
        this.state.initialForm.selectedProfessionalField = null;
        this.state.initialForm.selectedProfessionalExperience = null;
        this.state.initialForm.selectedFurtherEducate = null;
        this.state.initialForm.selectedGender = null;
    },
    setIsIE11 (newValue) {
        this.state.isIE11 = newValue;
    }
}

export default store
