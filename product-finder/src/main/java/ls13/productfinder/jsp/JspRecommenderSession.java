package ls13.productfinder.jsp;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.RecommendationResult;
import ls13.productfinder.api.RecommenderSession;
import ls13.productfinder.api.RecommenderSession.SortOrder;
import ls13.productfinder.api.Variable;
import ls13.productfinder.api.Variable.VarType;
import ls13.productfinder.core.RecommenderSessionImpl;

/**
 * JSP wrapper for a {@link RecommenderSessionImpl} 
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class JspRecommenderSession {

	private static final Logger LOGGER = LoggerFactory.getLogger(JspRecommenderSession.class);
	
	private static final String SORT_ORDER_DESC = "desc";
	private static final String SORT_ORDER_ASC = "asc";
	
	public static final String P_SORT_ORDER = "SORT_ORDER";
	public static final String P_SORT_BY = "SORT_BY";
	
	private final RecommenderSession delegate;
	
	private String sortOrder;
	private String sortBy;
	
	/**
	 * 
	 * @param delegate
	 */
	public JspRecommenderSession(RecommenderSession delegate) {
		this.delegate = delegate;
	}
	
	/**
	 * Processes the given request and sets the variables for the current session
	 * 
	 * @param request
	 * @param lastSortBy
	 */
	public void prepareRequest(HttpServletRequest request, PageContext pageContext) {
		
		// process engine variables
		for (Variable variable : delegate.getVariables()) {
			final String varName = variable.getName();
			final VarType varType = variable.getType();
			
			// Get the values from the request (depends on type - array or not) and
			// set values for session.
			final Object varValue;
			if (varType.isArray()) {
				varValue = request.getParameterValues(varName);
			} else {
				varValue = request.getParameter(varName);
			}
			
			if (varValue != null) {
				delegate.setInput(varName, varValue);
			}	
			
			LOGGER.debug("processing request - var: {}, type: {}, value: {}", varName, varType, varValue);
		}
		
		// process sort settings
		sortBy = Optional.ofNullable(request.getParameter(P_SORT_BY))	// check request
				.orElse(Optional.ofNullable(							// check last sortBy from session
						pageContext.getAttribute(P_SORT_BY, PageContext.SESSION_SCOPE))
						.map(Object::toString)
						.orElse("p_price"));							// use default
		
		sortOrder = "p_price".equals(sortBy) ? SORT_ORDER_ASC : SORT_ORDER_DESC;
		
		pageContext.setAttribute(P_SORT_BY, sortBy, PageContext.SESSION_SCOPE);
		pageContext.setAttribute(P_SORT_ORDER, sortOrder, PageContext.SESSION_SCOPE);
		
		LOGGER.debug("processing request - sort by: {}, sort order: {}", sortBy, sortOrder);
	}
	
	/**
	 * A method that computes the set of matching products for the current user
	 * requirements and stores it internally
	 * @param number of products for relaxation
	 * @return a handle to the last recommendation result
	 */
	public RecommendationResult computeRecommendationResult(int relax) throws ProductFinderException {
		return delegate.computeRecommendationResult(relax);
	}
	
	/**
	 * Returns the list of products matching a certain expression
	 * @param expression the expression to be evaluated
	 * @return the list of matching products
	 * @throws ProductFinderException
	 */
	public List<Product> getProductsForExpression(String expression) throws ProductFinderException {
		return delegate.getProductsForExpression(expression);
	}
	
	/**
	 * 
	 * @throws ProductFinderException
	 */
	public void sortResult() throws ProductFinderException {
		delegate.sortResults(sortBy, SortOrder.forName(sortOrder));
	}
	
	/**
	 * @return Returns the lastResult.
	 */
	public RecommendationResult getLastResult() {
		return delegate.getLastResult();
	}
	
	/**
	 * Computes a personalized infotext variant
	 * @param name the name of the text
	 * @return the personalized variant or "" if no variant applies
	 * @throws ProductFinderException if text is not defined in model
	 */
	public String getInfoText(String name) throws ProductFinderException {
		return delegate.getInfoText(name, null);
	}
}
