package ls13.productfinder.jsp;

import javax.servlet.jsp.PageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.RecommenderEngine;
import ls13.productfinder.core.RecommenderEngineImpl;
import ls13.productfinder.xml.loader.XmlKnowledgeBaseLoader;
import ls13.productfinder.xml.loader.XmlProductCatalogLoader;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class JspRecommenderEngine {

	private static final Logger LOGGER = LoggerFactory.getLogger(JspRecommenderEngine.class);
	
	public static final String P_SESSION = "R_SESSION";
	public static final String P_ENGINE = "R_ENGINE";
	
	private static final Object LOCK = new Object();
	
	/**
	 * 
	 * @param pageContext
	 * @return recommender session
	 * @throws ProductFinderException 
	 */
	public static JspRecommenderSession getOrCreate(PageContext pageContext) throws ProductFinderException {
		
		// Next, create a user session, if we do not already have one. This time, we store
		// the object in the session scope for this user.
		JspRecommenderSession userSession = (JspRecommenderSession) pageContext.getAttribute(
				P_SESSION, PageContext.SESSION_SCOPE);
				
		if (userSession != null) {
			return userSession;
		}
		
		RecommenderEngine recommenderEngine = (RecommenderEngine) pageContext.getAttribute(P_ENGINE, PageContext.APPLICATION_SCOPE);
		if (recommenderEngine == null) {
			synchronized (LOCK) {
				recommenderEngine = (RecommenderEngine) pageContext.getAttribute(P_ENGINE, PageContext.APPLICATION_SCOPE);
				if (recommenderEngine == null) {
					//-----------------------------------------------------------------------------------
					// Initialization (loading of data) done on first page visit
					//-----------------------------------------------------------------------------------
					// First step: Make sure that the model, which is shared by all clients
					// is already initialized.
					// Our strategy is to simply have one shared instance in the application
					
					// Load data from defined sources in the web application
					final String path = pageContext.getServletContext().getRealPath(".") + "/data";
					final String knowledgeBaseFile = path + "/rules.xml";
					final String productFile = path + "/digicams.xml";
					
					LOGGER.debug("creating recommender engine using product catalog: {}, knowledge base: {}",
							productFile, knowledgeBaseFile);
					
					recommenderEngine = new RecommenderEngineImpl(new XmlKnowledgeBaseLoader(knowledgeBaseFile),
							new XmlProductCatalogLoader(productFile));
					
					// Store it in the global scope
					pageContext.setAttribute(P_ENGINE, recommenderEngine, PageContext.APPLICATION_SCOPE);
				}
			}
		}

		// No session yet available: Create one and store it.
		LOGGER.debug("creating new user session");
		
		// create a new one for this engine and store it in the scope
		userSession = new JspRecommenderSession(recommenderEngine.newRecommenderSession());
		pageContext.setAttribute(P_SESSION, userSession, PageContext.SESSION_SCOPE);
		return userSession;
	}
}
