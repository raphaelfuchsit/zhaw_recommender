package ls13.productfinder.tests;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;

import com.google.common.base.Preconditions;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class TestUtils {

	private static final String OWN_CLASS_NAME = TestUtils.class.getName();
	
	/**
	 * calculates and returns the full path to a file in the package of the calling class
	 * @param fileName file name, must not be null
	 * @return absolute path to file
	 * @throws Exception
	 */
	public static String getPackageRessourceLocation(String fileName) throws Exception {
		return getPackageRessourceLocation(fileName, getCallerPackage());
	}
	
	private static Package getCallerPackage() throws Exception {
		final String className = Arrays.stream(new Exception().getStackTrace())
				.filter(e -> !OWN_CLASS_NAME.equals(e.getClassName()))
				.findFirst()
				.map(StackTraceElement::getClassName)
				.orElseThrow(() -> new Exception("[ERROR] Could not detect name of calling class"));
		final Class<?> clazz = Class.forName(className);
		return clazz.getPackage();
	}
	
	/**
	 * calculates and returns the full path to a file in a package
	 * @param fileName file name, must not be null
	 * @param pkg package, must not be null
	 * @return absolute path to file
	 * @throws Exception
	 */
	public static String getPackageRessourceLocation(String fileName, Package pkg) throws Exception {
		Preconditions.checkNotNull(fileName, "Parameter fileName must not be null");
		Preconditions.checkNotNull(pkg, "Parameter pkg must not be null");
		
		final String location = '/' + pkg.getName().replace('.', '/') + '/' + fileName;
		final URI uri = findResource(location);
		if (uri == null) {
			throw new Exception("Could not locate resource " + location + " in package " + pkg);
		}
		return new File(uri).getAbsolutePath();
	}
	
	/**
	 * uses a class' classloader and the local fs to locate a resource.<br>
	 * The FS is only checked when the class' classloader is unable to find the resource.<br>
	 * @param resourceName name of the resource
	 * @param clazz class to be used for the lookup
	 * @return URI of the resource or null, if no resource could be located using the given name
	 */
	public static URI findResource(String resourceName, Class<?> clazz) {
		Preconditions.checkNotNull(resourceName, "Parameter resourceName must not be null");
		Preconditions.checkNotNull(clazz, "Parameter clazz must not be null");
		
		// check classloader first
		final URL resourceUrl = clazz.getResource(resourceName);
		
		if (resourceUrl != null) {
			try {
				return resourceUrl.toURI();
			} catch (URISyntaxException e) {
				throw new RuntimeException("Error during conversion of url " + resourceUrl + " to uri", e);
			}
		}
		
		// did not work => try fs
		final File file = new File(resourceName);
		if (file.isFile()) {
			return file.toURI();
		}
		
		// not found
		return null;
	}
	
	/**
	 * uses this class' classloader and the local fs to locate a resource.<br>
	 * The FS is only checked when the class' classloader is unable to find the resource.<br>
	 * @param ressourceName name of the resource
	 * @return URI of the resource or null, if no resource could be located using the given name
	 */
	public static URI findResource(String ressourceName) {
		return findResource(ressourceName, TestUtils.class);
	}
}
