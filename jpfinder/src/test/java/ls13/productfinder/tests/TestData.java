package ls13.productfinder.tests;

import java.util.Arrays;
import java.util.List;

import ls13.productfinder.api.Variable;
import ls13.productfinder.api.Variable.VarType;
import ls13.productfinder.beans.KnowledgeBase;
import ls13.productfinder.beans.ProductCatalog;
import ls13.productfinder.xml.loader.XmlKnowledgeBaseLoader;
import ls13.productfinder.xml.loader.XmlProductCatalogLoader;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class TestData {
	
	public static final List<Variable> VARIABLES = Arrays.asList(
			Variable.builder().setName("p_flash_types").setType(VarType.STRING_ARRAY).build(),
			Variable.builder().setName("p_lcd").setType(VarType.STRING).build(),
			Variable.builder().setName("p_lcd_size").setType(VarType.DOUBLE).build(),
			Variable.builder().setName("p_optical_zoom").setType(VarType.DOUBLE).build(),
			Variable.builder().setName("p_digital_zoom").setType(VarType.DOUBLE).build(),
			Variable.builder().setName("p_resolution").setType(VarType.DOUBLE).build(),
			Variable.builder().setName("p_image").setType(VarType.STRING).build(),
			Variable.builder().setName("p_extras").setType(VarType.STRING_ARRAY).build(),
			Variable.builder().setName("p_summary").setType(VarType.STRING).build(),
			Variable.builder().setName("p_name").setType(VarType.STRING).build(),
			Variable.builder().setName("p_manufacturer").setType(VarType.STRING).build(),
			Variable.builder().setName("p_price").setType(VarType.DOUBLE).build(),
			Variable.builder().setName("p_rating").setType(VarType.DOUBLE).build()
			);

	/**
	 * 
	 * @return test {@link KnowledgeBase}
	 * @throws Exception
	 */
	public KnowledgeBase getKnowledgeBase() throws Exception {
		return new XmlKnowledgeBaseLoader(TestUtils.getPackageRessourceLocation("rules.xml")).loadKnowledgeBase();
	}
	
	/**
	 * 
	 * @param productProperties 
	 * @return test {@link KnowledgeBase}
	 * @throws Exception
	 */
	public static ProductCatalog getProductCatalog(List<Variable> productProperties) throws Exception {
		return new XmlProductCatalogLoader(TestUtils.getPackageRessourceLocation("digicams.xml"))
				.readProductData(productProperties);
	}
}
