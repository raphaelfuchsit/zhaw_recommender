package ls13.productfinder.core;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Doubles;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.RecommendationResult;
import ls13.productfinder.api.Variable;
import ls13.productfinder.beans.FilterRule;
import ls13.productfinder.tests.TestData;

/**
 * 
 * @author streuch1
 *
 */
public class FilterBasedMatcherImplTest {
	
	@Test
	public void testOk01() throws Exception {
		
		final List<FilterRule> filters = Arrays.asList(
					FilterRule.builder()
						.setName("fi")
						.setCondition("c_pref_min_resolution == 'medium'")
						.setFilter("p_resolution < 4")
						.setPriority(5)
						.build()
				);
		
		final List<Variable> variables = TestData.VARIABLES;
		final List<Product> products = TestData.getProductCatalog(variables).getProducts();
				
		final FilterBasedMatcherImpl fbm = new FilterBasedMatcherImpl(ScriptableImpl.createCompiler(), products, filters);
		
		final RecommendationResult result = fbm.computeRecommendationResult(ImmutableMap.<String, Object>builder()
				.put("c_pref_min_resolution", "medium")
				.build(), -1);
		
		Assert.assertNotNull(result);
		
		Assert.assertEquals(1, result.getAppliedFilters().size());
		Assert.assertEquals(0, result.getRelaxedFilters().size());
		Assert.assertEquals(0, result.getMatchingProducts().size());
	}
	
	@Test
	public void testOk02() throws Exception {
		
		final List<FilterRule> filters = Arrays.asList(
					FilterRule.builder()
						.setName("fi")
						.setCondition("c_pref_min_resolution == 'medium'")
						.setFilter("p_resolution < 99")
						.setPriority(5)
						.build()
				);
		
		final List<Variable> variables = TestData.VARIABLES;
		final List<Product> products = TestData.getProductCatalog(variables).getProducts();
				
		final FilterBasedMatcherImpl fbm = new FilterBasedMatcherImpl(ScriptableImpl.createCompiler(), products, filters);
		
		final RecommendationResult result = fbm.computeRecommendationResult(ImmutableMap.<String, Object>builder()
				.put("c_pref_min_resolution", "medium")
				.build(), 1);
		
		Assert.assertNotNull(result);
		
		final long expected = products.stream()
			.map(e -> e.get("p_resolution"))
			.map(Object::toString)
			.map(Doubles::tryParse)
			.filter(e -> e < 99)
			.count();
		

		Assert.assertEquals(1, result.getAppliedFilters().size());
		Assert.assertEquals(0, result.getRelaxedFilters().size());
		Assert.assertEquals(expected, result.getMatchingProducts().size());
	}
	
	@Test
	public void testNoFiltering01() throws Exception {
		
		final List<FilterRule> filters = Arrays.asList(
					FilterRule.builder()
						.setName("fi")
						.setCondition("c_pref_min_resolution == 'medium'")
						.setFilter("p_resolution < 99")
						.setPriority(5)
						.build()
				);
		
		final List<Variable> variables = TestData.VARIABLES;
		final List<Product> products = TestData.getProductCatalog(variables).getProducts();
				
		final FilterBasedMatcherImpl fbm = new FilterBasedMatcherImpl(ScriptableImpl.createCompiler(), products, filters);
		
		final RecommendationResult result = fbm.computeRecommendationResult(ImmutableMap.<String, Object>builder()
				.put("c_pref_min_resolution", "invalid-condition")
				.build(), 1);
		
		Assert.assertNotNull(result);
		
		Assert.assertEquals(0, result.getAppliedFilters().size());
		Assert.assertEquals(0, result.getRelaxedFilters().size());
		Assert.assertEquals(products.size(), result.getMatchingProducts().size());
	}
	
	@Test
	public void testRelaxation01() throws Exception {
		
		final List<FilterRule> filters = Arrays.asList(
					FilterRule.builder()
						.setName("f1")
						.setCondition("c_pref_min_resolution == 'medium'")
						.setFilter("p_resolution < 4")	// <-- returns empty result, should be removed during relaxation
						.setPriority(5)
						.build()
				);
		
		final List<Variable> variables = TestData.VARIABLES;
		final List<Product> products = TestData.getProductCatalog(variables).getProducts();
				
		final FilterBasedMatcherImpl fbm = new FilterBasedMatcherImpl(ScriptableImpl.createCompiler(), products, filters);
		
		final RecommendationResult result = fbm.computeRecommendationResult(ImmutableMap.<String, Object>builder()
				.put("c_pref_min_resolution", "medium")
				.build(), 1);
		
		Assert.assertNotNull(result);
		
		// expected no filter to be used
		Assert.assertEquals(0, result.getAppliedFilters().size());
		Assert.assertEquals(1, result.getRelaxedFilters().size());
		Assert.assertEquals("f1", result.getRelaxedFilters().get(0).getName());
		
		Assert.assertEquals(products.size(), result.getMatchingProducts().size());
	}
	
	@Test
	public void testRelaxation02() throws Exception {
		
		final List<FilterRule> filters = Arrays.asList(
					FilterRule.builder()
						.setName("f1")
						.setCondition("c_pref_min_resolution == 'medium'")
						.setFilter("p_resolution < 4")	// <-- returns empty result, should be removed during relaxation
						.setPriority(5)
						.build(),
					FilterRule.builder()
						.setName("f2")
						.setCondition("c_pref_min_resolution == 'medium'")
						.setFilter("p_resolution < 9")
						.setPriority(0)	// <-- forced filter
						.build()	
				);
		
		final List<Variable> variables = TestData.VARIABLES;
		final List<Product> products = TestData.getProductCatalog(variables).getProducts();
				
		final FilterBasedMatcherImpl fbm = new FilterBasedMatcherImpl(ScriptableImpl.createCompiler(), products, filters);
		
		final RecommendationResult result = fbm.computeRecommendationResult(ImmutableMap.<String, Object>builder()
				.put("c_pref_min_resolution", "medium")
				.build(), 1);
		
		Assert.assertNotNull(result);
		
		// expected only f2 to be used
		Assert.assertEquals(1, result.getAppliedFilters().size());
		Assert.assertEquals("f2", result.getAppliedFilters().get(0).getName());
		Assert.assertEquals(1, result.getRelaxedFilters().size());
		Assert.assertEquals("f1", result.getRelaxedFilters().get(0).getName());
		
		final long expected = products.stream()
			.map(e -> e.get("p_resolution"))
			.map(Object::toString)
			.map(Doubles::tryParse)
			.filter(e -> e < 9)
			.count();
		
		Assert.assertEquals(expected, result.getMatchingProducts().size());
	}
}
