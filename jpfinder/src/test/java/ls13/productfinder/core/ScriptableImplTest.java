package ls13.productfinder.core;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class ScriptableImplTest {
	
	@Test
	public void testMultiThreading01() throws Exception {
		
		final String jsCode = "c_pref_min_resolution == 'high'";
		
		final Random random = new Random();
		final Map<Boolean, Map<String, Object>> testCases = ImmutableMap.<Boolean, Map<String, Object>>builder()
				.put(Boolean.TRUE, ImmutableMap.<String, Object>builder().put("c_pref_min_resolution", "high").build())
				.put(Boolean.FALSE, ImmutableMap.<String, Object>builder().put("c_pref_min_resolution", "low").build())
				.build();
		
		final ScriptableImpl<Boolean> scriptable = new ScriptableImpl<>(jsCode, Boolean.class, false, null, Collections.emptyList());
		
		final List<Callable<Void>> callables = IntStream.range(0, 20)
				.mapToObj(i -> new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						final boolean expected = random.nextBoolean();
						final Map<String, Object> variableBindings = testCases.get(expected);
						
						for (int j = 0; j < 10_000; j++) {
							Assert.assertEquals(expected, scriptable.execute(variableBindings, false));
						}
						return null;
					}
				})
				.collect(Collectors.toList());
		
		for (Future<Void> future : Executors.newFixedThreadPool(10).invokeAll(callables)) {
			future.get();
		}
	}
	
	@Test
	public void testFunction01() throws Exception {
		
		final String var = "p_price";
		final String jsCode = "function __utility() {\n" + 
				"				// dynamic code sample\n" + 
				"				if (p_price < 200) {\n" +
				"					print('(10) price: ' + p_price);" + 
				"					return 10;\n" + 
				"				} else {\n" + 
				"					print('(05) price: ' + p_price);" + 
				"					return 5;\n" + 
				"				}\n" + 
				"			}";
		
		System.out.println();
		
		final Map<Integer, Map<String, Object>> testCases = ImmutableMap.<Integer, Map<String, Object>>builder()
				.put(5, ImmutableMap.<String, Object>builder()
						.put(var, 300)
						.build())
				.put(10, ImmutableMap.<String, Object>builder()
						.put(var, 100)
						.build())
				.build();
		
		final ScriptableImpl<Integer> scriptable = new ScriptableImpl<>(jsCode, Integer.class, true, null, Collections.emptyList());
		
		for (Entry<Integer, Map<String, Object>> test : testCases.entrySet()) {
			Assert.assertEquals(test.getKey(), scriptable.execute(test.getValue(), false));
		}
	}
}
