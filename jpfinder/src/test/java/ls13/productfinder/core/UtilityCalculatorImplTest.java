package ls13.productfinder.core;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.Variable;
import ls13.productfinder.beans.JsFunction;
import ls13.productfinder.tests.TestData;

/**
 * 
 * @author streuch1
 *
 */
public class UtilityCalculatorImplTest {
	
	@Test
	public void testStatic01() throws Exception {
		
		final JsFunction utilityFunction = JsFunction.builder()
				.setFunctionName("my-utility-function")
				.setDynamicCalculation(false)
				.setFunctionCode("function __utility() {\n" + 
				"				// dynamic code sample\n" + 
				"				if (p_price < 200) {\n" + 
				"					return 10;\n" + 
				"				}\n" + 
				"				else {\n" + 
				"					return 5;\n" + 
				"				}\n" + 
				"			}")
				.build();
		
		final List<Variable> variables = TestData.VARIABLES;
		final List<Product> products = TestData.getProductCatalog(variables).getProducts();
		
		final UtilityCalculatorImpl uc = new UtilityCalculatorImpl(ScriptableImpl.createCompiler(), utilityFunction);
		
		Assert.assertTrue(uc.requiresStaticCalculation());
		Assert.assertFalse(uc.requiresDynamicCalculation());
		
		final List<Product> result = uc.computeStaticUtilities(products);
		
		Assert.assertNotNull(result);
		Assert.assertTrue(!result.isEmpty());
		
		for (Product product : result) {
			final Object utilVal = product.getProperties().get(Product.UTILITY_DENOMINATOR);
			Assert.assertNotNull(utilVal);
			
			final double expected = 200 >= (Double) product.getProperties().get("p_price") ? 10 : 5;
			Assert.assertEquals(expected, utilVal);
		}
	}
	
	@Test
	public void testDynamic01() throws Exception {
		
		final JsFunction utilityFunction = JsFunction.builder()
				.setFunctionName("my-utility-function")
				.setDynamicCalculation(true)
				.setFunctionCode("function __utility() {\n" + 
				"				// dynamic code sample\n" + 
				"				if (p_price < 200) {\n" + 
				"					return 10;\n" + 
				"				}\n" + 
				"				if (c_arg == 1) {\n" + 
				"					return 1;\n" + 
				"				}\n" +
				"				else {\n" + 
				"					return 5;\n" + 
				"				}\n" + 
				"			}")
				.build();
		
		final List<Variable> variables = TestData.VARIABLES;
		final List<Product> products = TestData.getProductCatalog(variables).getProducts();
		
		final UtilityCalculatorImpl uc = new UtilityCalculatorImpl(ScriptableImpl.createCompiler(), utilityFunction);
		
		Assert.assertFalse(uc.requiresStaticCalculation());
		Assert.assertTrue(uc.requiresDynamicCalculation());
		
		final List<Product> result = uc.computeDynamicUtilities(products, ImmutableMap.<String, Object>builder()
				.put("c_arg", 1)
				.build());
		
		Assert.assertNotNull(result);
		Assert.assertTrue(!result.isEmpty());
		
		for (Product product : result) {
			final Object utilVal = product.getProperties().get(Product.UTILITY_DENOMINATOR);
			Assert.assertNotNull(utilVal);
			
			final double expected = 200 >= (Double) product.getProperties().get("p_price") ? 10 : 1;
			Assert.assertEquals("test failed for product: " + product, expected, utilVal);
		}
	}
}
