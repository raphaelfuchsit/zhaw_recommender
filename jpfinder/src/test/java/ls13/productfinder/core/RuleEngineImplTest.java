package ls13.productfinder.core;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.BusinessRule;

/**
 * 
 * @author streuch1
 *
 */
public class RuleEngineImplTest {
	
	@Test
	public void testOk01() throws Exception {
				
		final List<BusinessRule> businessRules = Arrays.asList(
					BusinessRule.builder()
						.setName("my-business-rule-1")
						.setCodeIf("c_arg1 == 100")
						.setCodeThen("c_arg2 = 'rule-1-matched'")
						.build()
				);
		
		final RuleEngineImpl re = new RuleEngineImpl(ScriptableImpl.createCompiler(), businessRules, 5);
		
		final Map<String, Object> result = re.applyRules(ImmutableMap.<String, Object>builder()
				.put("c_arg1", 100)
				.put("c_arg2", "no-match")
				.build());
		
		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.size());
		Assert.assertEquals(100, result.get("c_arg1"));
		Assert.assertEquals("rule-1-matched", result.get("c_arg2"));
	}
	
	@Test
	public void testOk02() throws Exception {
		
		final List<BusinessRule> businessRules = Arrays.asList(
					BusinessRule.builder()
						.setName("my-business-rule-1")
						.setCodeIf("c_arg1 == 100")
						.setCodeThen("c_arg2 = 'rule-1-matched'")
						.build(),
					BusinessRule.builder()
						.setName("my-business-rule-2")
						.setCodeIf("c_arg2 == 'rule-1-matched'")
						.setCodeThen("c_arg3 = 10")
						.build()	
				);
		
		final RuleEngineImpl re = new RuleEngineImpl(ScriptableImpl.createCompiler(), businessRules, 5);
		
		final Map<String, Object> result = re.applyRules(ImmutableMap.<String, Object>builder()
				.put("c_arg1", 100)
				.put("c_arg2", "no-match")
				.put("c_arg3", 2)
				.build());
		
		Assert.assertNotNull(result);
		Assert.assertEquals(3, result.size());
		Assert.assertEquals(100, result.get("c_arg1"));
		Assert.assertEquals("rule-1-matched", result.get("c_arg2"));
		Assert.assertEquals(10, result.get("c_arg3"));
	}
	
	@Test(expected=ProductFinderException.class)
	public void testError01() throws Exception {
		
		final List<BusinessRule> businessRules = Arrays.asList(
					BusinessRule.builder()
						.setName("my-business-rule-1")
						.setCodeIf("c_arg1 == 100")
						.setCodeThen("c_arg2 = 'rule-1-matched'")
						.build(),
					BusinessRule.builder()
						.setName("my-business-rule-2")
						.setCodeIf("c_arg2 == 'rule-1-matched'")
						.setCodeThen("c_arg3 = 10")
						.build()	
				);
		
		final RuleEngineImpl re = new RuleEngineImpl(ScriptableImpl.createCompiler(), businessRules, 2);
		
		// expected max iteration exception
		re.applyRules(ImmutableMap.<String, Object>builder()
				.put("c_arg1", 100)
				.put("c_arg2", "no-match")
				.put("c_arg3", 2)
				.build());
	}
}
