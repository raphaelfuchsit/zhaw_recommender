package ls13.productfinder.core;

import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.Variable;
import ls13.productfinder.beans.JsFunction;
import ls13.productfinder.tests.TestData;

/**
 * 
 * @author streuch1
 *
 */
public class SimilarityCalculatorImplTest {

	@Test
	public void test01() throws Exception {

		final JsFunction similarityFunction = JsFunction.builder()
				.setFunctionName("my-similarity-function")
				.setFunctionCode("function __similarity() {\n" + "				// dynamic code sample\n"
						+ "				return java.lang.Math.abs(p1.get('p_price') - p2.get('p_price'));\n"
						+ "			}")
				.build();

		final List<Variable> variables = TestData.VARIABLES;
		final List<Product> products = TestData.getProductCatalog(variables).getProducts();
		final Product testProduct = products.get(new Random().nextInt(products.size()));

		final SimilarityCalculatorImpl sc = new SimilarityCalculatorImpl(ScriptableImpl.createCompiler(), products,
				similarityFunction);
		final List<Product> result = sc.getSimilarProducts(testProduct, -1);

		final Double testPrice = (Double) testProduct.get("p_price");

		Double lastSimValue = null;
		for (Product product : result) {
			final Double simVal = (Double) product.getProperties().get(Product.SIMILARITY_DENOMINATOR);
			Assert.assertNotNull(simVal);

			final double resultPrice = (Double) product.get("p_price");

			final Double expected = Math.abs(testPrice - resultPrice);
			Assert.assertEquals(expected, simVal);

			if (lastSimValue != null) {
				Assert.assertTrue(lastSimValue <= simVal);
			}
			lastSimValue = simVal;
		}
	}

	@Test
	public void test02() throws Exception {

		final JsFunction similarityFunction = JsFunction.builder()
				.setFunctionName("my-similarity-function")
				.setFunctionCode("function __similarity() {\n" + "				// dynamic code sample\n"
						+ "				return java.lang.Math.abs(p1.get('p_price') - p2.get('p_price'));\n"
						+ "			}")
				.build();

		final List<Variable> variables = TestData.VARIABLES;
		final List<Product> products = TestData.getProductCatalog(variables).getProducts();
		final Product testProduct = products.get(new Random().nextInt(products.size()));

		final SimilarityCalculatorImpl sc = new SimilarityCalculatorImpl(ScriptableImpl.createCompiler(), products,
				similarityFunction);
		final List<Product> result = sc.getSimilarProducts(testProduct, 10);

		Assert.assertEquals(10, result.size());

		final Double testPrice = (Double) testProduct.get("p_price");

		Double lastSimValue = null;
		for (Product product : result) {
			final Double simVal = (Double) product.getProperties().get(Product.SIMILARITY_DENOMINATOR);
			Assert.assertNotNull(simVal);

			final double resultPrice = (Double) product.get("p_price");

			final Double expected = Math.abs(testPrice - resultPrice);
			Assert.assertEquals(expected, simVal);

			if (lastSimValue != null) {
				Assert.assertTrue(lastSimValue <= simVal);
			}
			lastSimValue = simVal;
		}
	}
}
