package ls13.productfinder.core;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.InfoText;
import ls13.productfinder.beans.TextVariant;

/**
 * 
 * @author streuch1
 *
 */
public class InfoTextManagerImplTest {
	
	@Test
	public void testTextVariant01() throws Exception {
		
		final List<InfoText> infoTexts = createInforTexts();
		
		final String expected = "You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.";
		
		final InfoTextManagerImpl itm = new InfoTextManagerImpl(ScriptableImpl.createCompiler(), infoTexts);

		
		final String result = itm.getText("interests", ImmutableMap.<String, Object>builder()
				.put("c_pref_interest", "dunno")
				.build(), null);
		
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testDefaultText01() throws Exception {
		
		final List<InfoText> infoTexts = createInforTexts();
		
		final String expected = "You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.";
		
		final InfoTextManagerImpl itm = new InfoTextManagerImpl(ScriptableImpl.createCompiler(), infoTexts);

		
		final String result = itm.getText("interests", ImmutableMap.<String, Object>builder()
				.put("c_pref_interest", "n/a")
				.build(), null);
		
		Assert.assertEquals(expected, result);
	}
	
	@Test(expected=ProductFinderException.class)
	public void testError01() throws Exception {
		
		final List<InfoText> infoTexts = createInforTexts();
		
		final InfoTextManagerImpl itm = new InfoTextManagerImpl(ScriptableImpl.createCompiler(), infoTexts);

		// expected exception, undefined name
		itm.getText("invalid-value", ImmutableMap.<String, Object>builder()
				.put("c_pref_interest", "n/a")
				.build(), null);
	}
	
	
	
	private List<InfoText> createInforTexts() {
		final List<InfoText> infoTexts = Arrays.asList(
					InfoText.builder()
						.setName("interests")
						.setDefaultText("You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.")
						.setTextVariants(Arrays.asList(
									TextVariant.builder()
										.setCondition("c_pref_interest == 'dunno'")
										.setText("You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.")
										.build(),
									TextVariant.builder()
										.setCondition("c_pref_interest == 'beginner'")
										.setText("You have told me that you have a strong interest in photography, so I shall recommend you a camera that fulfils some minimal standards for that.")
										.build(),
									TextVariant.builder()
										.setCondition("c_pref_interest == 'medium'")
										.setText("As you have specified to have an increased interest in photography, I propose a camera that fulfils an expert's requirements.")
										.build(),
									TextVariant.builder()
										.setCondition("c_pref_interest == 'expert'")
										.setText("As you have specified to be an expert in the domain, I will recommend a camera that fulfils above-the-average requirements.")
										.build()
								))
						.build()
				);
		return infoTexts;
	}
}
