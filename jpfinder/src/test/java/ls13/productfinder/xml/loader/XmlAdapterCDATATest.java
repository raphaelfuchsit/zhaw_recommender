package ls13.productfinder.xml.loader;

import org.junit.Assert;
import org.junit.Test;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.xml.loader.XmlAdapterCDATA;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class XmlAdapterCDATATest {

	@Test
	public void testOk01() throws Exception {
		
		final String input = "<![CDATA[\n" + 
				"	function __utility() {\n" + 
				"		//out.println(\"p_price:\" + p_price);\n" + 
				"		// dynamic code sample\n" + 
				"		/* \n" + 
				"		if (isContainedIn('Sony',c_pref_manufacturer)) {\n" + 
				"			return 100;\n" + 
				"		}\n" + 
				"		if (c_pref_interest == 'low') {\n" + 
				"			return 0;\n" + 
				"		}\n" + 
				"		*/\n" + 
				"		if (p_price < 200) {\n" + 
				"			return 10;\n" + 
				"		}\n" + 
				"		else {\n" + 
				"			return 5;\n" + 
				"		}\n" + 
				"	}\n" + 
				"]]>";
		
		final String expected = "function __utility() {\n" + 
				"		//out.println(\"p_price:\" + p_price);\n" + 
				"		// dynamic code sample\n" + 
				"		/* \n" + 
				"		if (isContainedIn('Sony',c_pref_manufacturer)) {\n" + 
				"			return 100;\n" + 
				"		}\n" + 
				"		if (c_pref_interest == 'low') {\n" + 
				"			return 0;\n" + 
				"		}\n" + 
				"		*/\n" + 
				"		if (p_price < 200) {\n" + 
				"			return 10;\n" + 
				"		}\n" + 
				"		else {\n" + 
				"			return 5;\n" + 
				"		}\n" + 
				"	}";
		
		final String result = new XmlAdapterCDATA().unmarshal(input);
		
		Assert.assertEquals(expected, result);
	}
	
	@Test(expected=ProductFinderException.class)
	public void testError01() throws Exception {
		
		final String input = "INVALID DATA <![CDATA[\n";
		
		new XmlAdapterCDATA().unmarshal(input);
	}
}
