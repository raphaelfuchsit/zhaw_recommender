package ls13.productfinder.xml.loader;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import org.junit.Assert;
import org.junit.Test;

import ls13.productfinder.tests.TestUtils;
import ls13.productfinder.xml.beans.XmlFilterRule;
import ls13.productfinder.xml.beans.XmlInfoText;
import ls13.productfinder.xml.beans.XmlProductProperty;
import ls13.productfinder.xml.beans.XmlRecommendationRules;
import ls13.productfinder.xml.beans.XmlTextVariant;
import ls13.productfinder.xml.beans.XmlUserRequirement;
import ls13.productfinder.xml.beans.XmlUtilityFunction;
import ls13.productfinder.xml.beans.XmlVariable;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class XmlFileParserTest {

	@Test
	public void testOkEngine01() throws Exception {
		 
		final String xmlFile = "testRules.xml";
        
		final XmlRecommendationRules recommendationRules = XmlFileParser.parse(TestUtils.getPackageRessourceLocation(xmlFile), XmlRecommendationRules.class);
		
		Assert.assertTrue(!recommendationRules.getVariables().isEmpty());
	}
	
	@Test
	public void testOkVariable01() throws Exception {
		 
		final String xmlData = "<Variable name=\"p_flash_types\" type=\"String[]\"/>";
        
		final XmlVariable result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlVariable.class);
		
		Assert.assertEquals("p_flash_types", result.getName());
		Assert.assertEquals("String[]", result.getType());
	}
	
	@Test
	public void testOkVariable02() throws Exception {
		 
		final String xmlData = "<Variable name=\"p_flash_types\" type=\"String[]\">\n" + 
				"	<Option>A</Option>\n" + 
				"	<Option>B</Option>\n" + 
				"	<Option>C</Option>\n" + 
				"</Variable>";
        
		final XmlVariable result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlVariable.class);
		
		Assert.assertEquals("p_flash_types", result.getName());
		Assert.assertEquals("String[]", result.getType());
		Assert.assertEquals(3, result.getOptions().size());
	}
	
	@Test
	public void testOkFilterRule01() throws Exception {
		 
		final String xmlData = "<FilterRule \n" + 
				"	name=\"f1\"\n" + 
				"	condition=\"c_pref_min_resolution == 'medium'\" \n" + 
				"	filter=\"p_resolution &lt; 4\"\n" + 
				"	priority = \"5\"\n" + 
				"	explanation = \"Of course, this camera fulfils your requirements with respect to the minimum resolution.\"\n" + 
				"	excuse = \"The proposed camera has a lot of features you desired, but it does have an above-average resolution.\"\n" + 
				"/>";
        
		final XmlFilterRule result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlFilterRule.class);
		
		Assert.assertEquals("f1", result.getName());
		Assert.assertEquals("c_pref_min_resolution == 'medium'", result.getCondition());
	}
	
	@Test
	public void testOkTextVariant01() throws Exception {
		 
		final String xmlData = "<TextVariant condition=\"c_pref_interest == 'dunno'\" text=\"You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.\"/>";
        
		final XmlTextVariant result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlTextVariant.class);
		
		Assert.assertEquals("c_pref_interest == 'dunno'", result.getCondition());
		Assert.assertEquals("You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.", result.getText());
	}
	
	@Test
	public void testOkInfoText01() throws Exception {
		 
		final String xmlData = "<InfoText name=\"interests\" defaulttext=\"You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.\">\n" + 
				"	<TextVariant condition=\"c_pref_interest == 'dunno'\" text=\"You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.\"/>\n" + 
				"	<TextVariant condition=\"c_pref_interest == 'beginner'\" text=\"You have told me that you have a strong interest in photography, so I shall recommend you a camera that fulfils some minimal standards for that.\"/>\n" + 
				"	<TextVariant condition=\"c_pref_interest == 'medium'\" text=\"As you have specified to have an increased interest in photography, I propose a camera that fulfils an expert's requirements.\"/>\n" + 
				"	<TextVariant condition=\"c_pref_interest == 'expert'\" text=\"As you have specified to be an expert in the domain, I will recommend a camera that fulfils above-the-average requirements.\"/>\n" + 
				"</InfoText>";
        
		final XmlInfoText result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlInfoText.class);
		
		Assert.assertEquals("interests", result.getName());
		Assert.assertEquals("You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.", result.getDefaulttext());
		Assert.assertNotNull(result.getTextVariants());
	}
	
	@Test
	public void testOkInfoText02() throws Exception {
		 
		final String xmlData = "<InfoText name=\"interests\" defaulttext=\"You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.\">\n" + 
				"</InfoText>";
        
		final XmlInfoText result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlInfoText.class);
		
		Assert.assertEquals("interests", result.getName());
		Assert.assertEquals("You have not specified your interests in photography, so I will propose cameras fulfilling standard requirements for an average user.", result.getDefaulttext());
	}
	
	@Test
	public void testOkUtilityFunction01() throws Exception {
		 
		final String xmlData = "<UtilityFunction type=\"static\">\n" + 
				"<![CDATA[\n" + 
				"	function __utility() {\n" + 
				"		//out.println(\"p_price:\" + p_price);\n" + 
				"		// dynamic code sample\n" + 
				"		/* \n" + 
				"		if (isContainedIn('Sony',c_pref_manufacturer)) {\n" + 
				"			return 100;\n" + 
				"		}\n" + 
				"		if (c_pref_interest == 'low') {\n" + 
				"			return 0;\n" + 
				"		}\n" + 
				"		*/\n" + 
				"		if (p_price < 200) {\n" + 
				"			return 10;\n" + 
				"		}\n" + 
				"		else {\n" + 
				"			return 5;\n" + 
				"		}\n" + 
				"	}\n" + 
				"]]>\n" + 
				"</UtilityFunction>";
        
		final XmlUtilityFunction result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlUtilityFunction.class);
		
		Assert.assertEquals("static", result.getType());
		Assert.assertNotNull(result.getFunctionSource());
	}
	
	@Test
	public void testOkProductProperty01() throws Exception {
		 
		final String xmlData = "<ProductProperty name=\"p_flash_types\" type=\"String[]\"/>";
        
		final XmlProductProperty result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlProductProperty.class);
		
		Assert.assertEquals("p_flash_types", result.getName());
		Assert.assertEquals("String[]", result.getType());
		Assert.assertFalse(result.toVariable().isPreference());
	}
	
	@Test
	public void testOkProductProperty02() throws Exception {
		 
		final String xmlData = "<ProductProperty name=\"p_flash_types\" type=\"String[]\">\n" + 
				"	<Option>A</Option>\n" + 
				"	<Option>B</Option>\n" + 
				"	<Option>C</Option>\n" + 
				"</ProductProperty>";
        
		final XmlProductProperty result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlProductProperty.class);
		
		Assert.assertEquals("p_flash_types", result.getName());
		Assert.assertEquals("String[]", result.getType());
		Assert.assertEquals(3, result.getOptions().size());
		Assert.assertFalse(result.toVariable().isPreference());
	}
	
	@Test
	public void testOkUserRequirement01() throws Exception {
		 
		final String xmlData = "<UserRequirement name=\"p_flash_types\" type=\"String[]\"/>";
        
		final XmlUserRequirement result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlUserRequirement.class);
		
		Assert.assertEquals("p_flash_types", result.getName());
		Assert.assertEquals("String[]", result.getType());
		Assert.assertTrue(result.toVariable().isPreference());
	}
	
	@Test
	public void testOkUserRequirement02() throws Exception {
		 
		final String xmlData = "<UserRequirement name=\"p_flash_types\" type=\"String[]\">\n" + 
				"	<Option>A</Option>\n" + 
				"	<Option>B</Option>\n" + 
				"	<Option>C</Option>\n" + 
				"</UserRequirement>";
        
		final XmlUserRequirement result = XmlFileParser.parse(new ByteArrayInputStream(xmlData.getBytes(StandardCharsets.UTF_8)), XmlUserRequirement.class);
		
		Assert.assertEquals("p_flash_types", result.getName());
		Assert.assertEquals("String[]", result.getType());
		Assert.assertEquals(3, result.getOptions().size());
		Assert.assertTrue(result.toVariable().isPreference());
	}
}
