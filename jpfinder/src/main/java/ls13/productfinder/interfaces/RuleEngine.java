package ls13.productfinder.interfaces;

import java.util.Map;

import ls13.productfinder.api.ProductFinderException;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface RuleEngine {

	/**
	 * Applies the defined business rules
	 * 
	 * @param user input the current set of variables
	 * @return new user input
	 * @throws ProductFinderException
	 */
	public Map<String, Object> applyRules(Map<String, Object> userInputs) throws ProductFinderException;
}
