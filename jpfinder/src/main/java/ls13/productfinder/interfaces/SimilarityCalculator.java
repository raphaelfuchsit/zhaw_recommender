package ls13.productfinder.interfaces;

import java.util.List;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.Variable;
import ls13.productfinder.api.Variable.VarType;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface SimilarityCalculator {
	
	public static final Variable SIMILARITY_DOMINATOR_VAR = Variable.builder()
			.setName(Product.SIMILARITY_DENOMINATOR)
			.setType(VarType.DOUBLE)
			.build();
	
	/**
	 * Evaluates the similarity function for all products in the database
	 * @param testProduct the product to compare with
	 * @param the top-n to be returned or -1 to get all products
	 * @return a sorted list of all products
	 * @throws ProductFinderException
	 */
	public List<Product> getSimilarProducts(Product testProduct, int howmany) throws ProductFinderException;
}
