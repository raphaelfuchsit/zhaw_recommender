package ls13.productfinder.interfaces;

import java.util.Map;

import ls13.productfinder.api.ProductFinderException;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface Scriptable<T> {

	/**
	 * 
	 * @param variableBindings
	 * @return script result
	 * @throws ProductFinderException
	 */
	public default T execute(Map<String, Object> variableBindings) throws ProductFinderException {
		return execute(variableBindings, false);
	}
	
	/**
	 * 
	 * @param variableBindings
	 * @param updateBindings if set, binding values are updated with values from script context after execution
	 * @return script result
	 * @throws ProductFinderException
	 */
	public T execute(Map<String, Object> variableBindings, boolean updateBindings) throws ProductFinderException;
	
	
	
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	public static interface ScriptableCompiler {
		
		/**
		 * 
		 * @param code
		 * @param resultClazz
		 * @param registerFunction if true, code is registered as function
		 * @param description used in log messages and exception trace or null
		 * @return
		 * @throws ProductFinderException
		 */
		public <T> Scriptable<T> compile(String code, Class<T> resultClazz, boolean registerFunction, Object description) throws ProductFinderException;
	}
}
