package ls13.productfinder.interfaces;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.KnowledgeBase;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface KnowledgeBaseLoader {

	/**
	 * Reads the knowledge base
	 * 
	 * @return knowledge base
	 * @throws ProductFinderException
	 */
	public KnowledgeBase loadKnowledgeBase()throws ProductFinderException;
}
