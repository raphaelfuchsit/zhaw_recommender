package ls13.productfinder.interfaces;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.Variable;
import ls13.productfinder.api.Variable.VarType;
import ls13.productfinder.core.ProductComparator;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface UtilityCalculator {
	
	public static final Variable UTILITY_DOMINATOR_VAR = Variable.builder()
			.setName(Product.UTILITY_DENOMINATOR)
			.setType(VarType.DOUBLE)
			.build();
	
	/** sorts product in descending order using the {{@link #UTILITY_DOMINATOR_VAR} property */
	public static final Comparator<Product> UTILITY_COMPARATOR = new ProductComparator(UTILITY_DOMINATOR_VAR.getName(), true).reversed();
	
	/**
	 * @return if true, use {@link #computeStaticUtilities(List, List)} to process static utility scores
	 */
	public boolean requiresStaticCalculation();
	
	/**
	 * This method accepts a list of products and computes
	 * the utility score by using the defined utility function. 
	 * The actual value is added as a special property {@link Product#UTILITY_DENOMINATOR} 
	 * (double) of the product properties.
	 * 
	 * @param products the list of products to be evaluated
	 * @return the augmented product list
	 */
	public List<Product> computeStaticUtilities(List<Product> products) throws ProductFinderException;
	
	/**
	 * 
	 * @return if true, use {{@link #computeDynamicUtilities(List, List, Map)} to process static utility scores
	 */
	public boolean requiresDynamicCalculation();
	
	/**
	 * This method accepts a list of products and computes
	 * the dynamic user specific utility score by using the defined utility function. 
	 * The actual value is added as a special property {@link Product#UTILITY_DENOMINATOR} 
	 * (double) of the product properties.
	 * 
	 * @param products the set of products
	 * @param input from user session
	 * @return a copy of the products augmented with personal utilities 
	 * @throws ProductFinderException
	 */
	public List<Product> computeDynamicUtilities(List<Product> products, Map<String, Object> userInputs) throws ProductFinderException;
}
