package ls13.productfinder.interfaces;

import java.util.Map;

import ls13.productfinder.api.ProductFinderException;

/**
 * Interface to generate generic descriptions
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface DescriptionGenerator {
	
	/**
	 * 
	 * @param userInputs
	 * @return explanation
	 * @throws ProductFinderException
	 */
	public String create(Map<String, Object> userInputs) throws ProductFinderException;
}
