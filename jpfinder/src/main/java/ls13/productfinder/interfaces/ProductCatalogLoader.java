package ls13.productfinder.interfaces;

import java.util.List;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.Variable;
import ls13.productfinder.beans.ProductCatalog;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface ProductCatalogLoader {

	/**
	 * Reads the list of products using the given variables
	 * 
	 * @param variables variables from knowledge base
	 * @return product catalog
	 * @throws ProductFinderException
	 */
	public ProductCatalog readProductData(List<Variable> variables) throws ProductFinderException;
}
