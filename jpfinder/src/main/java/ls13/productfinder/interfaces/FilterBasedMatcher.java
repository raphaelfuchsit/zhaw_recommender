package ls13.productfinder.interfaces;

import java.util.List;
import java.util.Map;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.RecommendationResult;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface FilterBasedMatcher {

	/**
	 * Computes the recommendation result
	 * 
	 * @param userInputs
	 * @param relax the number of products to be retrieved (currently only one prouct is garantueed)
	 * @return recommendation result
	 * @throws ProductFinderException
	 */
	public RecommendationResult computeRecommendationResult(Map<String, Object> userInputs, int relax) throws ProductFinderException;
	
	/**
	 * Computes a list of products for a given expression
	 * 
	 * @param expression filter expression 
	 * @return a list of matching products
	 * @throws ProductFinderExpression
	 */
	public List<Product> getProductsForExpression(String expression) throws ProductFinderException;
}
