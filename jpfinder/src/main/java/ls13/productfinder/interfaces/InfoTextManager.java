package ls13.productfinder.interfaces;

import java.util.Map;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface InfoTextManager {

	/**
	 * Returns the applicable variant of an info text
	 * @param name of info text
	 * @param userInputs
	 * @param product the product used to generate the text or null
	 * @return the text variant or an empty string, if no condition matches
	 * @throws ProductFinderException
	 */
	public String getText(String name, Map<String, Object> userInputs, Product product) throws ProductFinderException;
}
