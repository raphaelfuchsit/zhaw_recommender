package ls13.productfinder.api;

import java.util.List;
import java.util.Map;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface RecommenderSession {

	/**
	 * A method that computes the set of matching products for the current user
	 * requirements and stores it internally
	 * @param number of products for relaxation or -1 to disable relaxation
	 * @return a handle to the last recommendation result
	 */
	public RecommendationResult computeRecommendationResult(int relax) throws ProductFinderException;

	/**
	 * Returns the list of products matching a certain expression
	 * @param expression the expression to be evaluated
	 * @return the list of matching products
	 * @throws ProductFinderException
	 */
	public List<Product> getProductsForExpression(String expression) throws ProductFinderException;
	
	/**
	 * Computes a personalized infotext variant
	 * @param name the name of the text
	 * @param product the product used to generate the text or null
	 * @return the personalized variant or "" if no variant applies
	 * @throws ProductFinderException if text is not defined in model
	 */
	public String getInfoText(String name, Product product) throws ProductFinderException;

	/**
	 * Sorts the elements in the current result according to a defined single-valued property. As a
	 * default, sort order is ascending 
	 * @param property the sort criteron
	 * @param direction "asc|desc" 
	 * @throws ProductFinderException if property not known, or property is multivalued
	 */
	public void sortResults(String property, SortOrder sortOrder) throws ProductFinderException;
	
	/**
	 * 
	 * @return variables
	 */
	public List<Variable> getVariables();
	
	/**
	 * Sets the value for the variable with the given name
	 * 
	 * @param variable
	 * @param value
	 */
	public void setInput(String variable, Object value);
	
	/**
	 * Clears the input values
	 */
	public void clearInput();
	
	/**
	 * @return Returns the lastResult.
	 */
	public RecommendationResult getLastResult();
	
	/**
	 * @return the userInputs
	 */
	public Map<String, Object> getUserInputs();
	
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	public static enum SortOrder {
		ASC,
		DESC;
		
		public static final SortOrder forName(String name) {
			return "desc".equalsIgnoreCase(name) ? DESC : ASC;
		}
	}
}
