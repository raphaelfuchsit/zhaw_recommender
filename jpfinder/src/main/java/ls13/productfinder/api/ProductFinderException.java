package ls13.productfinder.api;

import java.util.Arrays;

/**
 * <p>Title:  Application-specific exception class</p>
 * <p>Description:  Exception root class</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author DJ
 * @version 1.0
 */
public class ProductFinderException extends Exception {
	public static final long serialVersionUID = 0L;;
	
	public ProductFinderException(String msg, Throwable t) {
		super(msg,t);
	}
	public ProductFinderException(String msg) {
		super(msg);
	}
	
	public ProductFinderException(String msg, Object... args) {
		super(String.format(msg, checkThrowable(args)), extractThrowable(args));
	}
	
	private static Object[] checkThrowable(Object[] args) {
		if (args == null || args.length == 0) {
			return args;
		}
		
		if (args[args.length-1] instanceof Throwable) {
			return Arrays.copyOf(args, args.length-1);
		}
		return args;
	}
	
	private static Throwable extractThrowable(Object[] args) {
		if (args == null || args.length == 0) {
			return null;
		}
		
		if (args[args.length-1] instanceof Throwable) {
			return (Throwable) args[args.length-1];
		}
		return null;
	}
}
