package ls13.productfinder.api;

import java.util.HashMap;
import java.util.Map;

import ls13.productfinder.interfaces.DescriptionGenerator;

/**
 * Filter description
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 */
public class Filter {
	
	private final String name;
	private final int priority;
	
	private final DescriptionGenerator explanationGenerator;
	private final DescriptionGenerator excuseGenerator;
	private final Map<String, Object> userInputs;

	public Filter(String name, int priority, Map<String, Object> userInputs, DescriptionGenerator explanationGenerator,
			DescriptionGenerator excuseGenerator) {
		this.name = name;
		this.priority = priority;
		this.userInputs = userInputs;
		this.explanationGenerator = explanationGenerator;
		this.excuseGenerator = excuseGenerator;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}
	/**
	 * 
	 * @param product
	 * @return explanation
	 * @throws ProductFinderException
	 */
	public String getExplanation(Product product) throws ProductFinderException {
		return process(explanationGenerator, product);
	}
	/**
	 * 
	 * @param product
	 * @return excuse
	 * @throws ProductFinderException
	 */
	public String getExcuse(Product product) throws ProductFinderException {
		return process(excuseGenerator, product);
	}
	
	private String process(DescriptionGenerator descriptionGenerator, Product product) throws ProductFinderException {
		final Map<String, Object> properties = new HashMap<>(userInputs);
		if (product != null) {
			properties.putAll(product.getProperties());
		}
		return descriptionGenerator.create(properties);
	}
	
	@Override
	public String toString() {
		return "Filter [name=" + name + ", priority=" + priority + "]";
	}
}
