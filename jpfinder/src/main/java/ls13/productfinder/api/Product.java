package ls13.productfinder.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

import com.google.auto.value.AutoValue;

/**
 * Bean representing a product and its properties
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@AutoValue
public abstract class Product {
	
	/** product property containing the utility score */
	public static final String UTILITY_DENOMINATOR = "__UTILITY__";
	/** product property containing the similarity score */
	public static final String SIMILARITY_DENOMINATOR = "__SIMILARITY__";
	
	public static final Product EMPTY_PRODUCT = builder()
			.setProperties(Collections.emptyMap())
			.build();

	public final Object get(String property) {
		if (property == null) {
			return null;
		}
		return getProperties().get(property);
	}
	
	public abstract Map<String, Object> getProperties();
	
	@Override
	public final String toString() {
		final StringBuffer sb = new StringBuffer();
		sb.append("Product [");
		
		boolean first = true;
		for (Entry<String, Object> entry : getProperties().entrySet()) {
			if (!first) {
				sb.append(", ");
			} else {
				first = false;
			}
			
			sb.append(entry.getKey()).append("=");
			if (entry.getValue() instanceof Object[]) {
				sb.append(Arrays.toString((Object[]) entry.getValue()));
			} else {
				sb.append(entry.getValue());
			}
		}
		
		sb.append("]");
		return sb.toString();
	}
	
	public static Builder builder() {
		return new AutoValue_Product.Builder();
	}
	
	@AutoValue.Builder
	public abstract static class Builder {
		public abstract Builder setProperties(Map<String, Object> value);
		public abstract Product build();
	}
}
