package ls13.productfinder.api;

import java.util.List;

import ls13.productfinder.beans.KnowledgeBase;
import ls13.productfinder.beans.ProductCatalog;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface RecommenderEngine {

	/**
	 * @return new recommender session
	 */
	public RecommenderSession newRecommenderSession();
	
	/**
	 * Loads (or reloads) both the product catalog as well as the knowledge base
	 * and initializes the engine
	 * 
	 * @throws ProductFinderException
	 */
	public void loadModel() throws ProductFinderException;
	
	/**
	 * list of user variables
	 * 
	 * @return registered user variables
	 */
	public List<Variable> getUserVariables();
	
	/**
	 * @return the ProductCatalog
	 */
	public ProductCatalog getProductCatalog();
	
	/**
	 * @return the KnowledgeBase
	 */
	public KnowledgeBase getKnowledgeBase();
	
	/**
	 * 
	 * @param property
	 * @return 
	 */
	public Variable getVariable(String property);
	
	/**
	 * A method that computes the set of matching products for the current user
	 * requirements and stores it internally
	 * 
	 * @param recommenderSession
	 * @param number of products for relaxation
	 * @return a handle to the last recommendation result
	 */
	
	public RecommendationResult computeRecommendationResult(RecommenderSession recommenderSession, int relax) throws ProductFinderException;
	
	/**
	 * Returns the list of products matching a certain expression
	 * 
	 * @param expression the expression to be evaluated
	 * @return the list of matching products
	 * @throws ProductFinderException
	 */
	public List<Product> getProductsForFilterExpression(String expression) throws ProductFinderException;
	
	/**
	 * Returns the applicable variant of an info text
	 * @param userInputs
	 * @param name of info text
	 * @param product the product used to generate the text or null
	 * @return the text variant or an empty string, if no condition matches
	 * @throws ProductFinderException
	 */
	public String getText(RecommenderSession recommenderSession, String name, Product product) throws ProductFinderException;
}
