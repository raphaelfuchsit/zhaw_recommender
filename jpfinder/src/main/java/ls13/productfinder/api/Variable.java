package ls13.productfinder.api;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.StringUtils;

import com.google.auto.value.AutoValue;
import com.google.common.base.Splitter;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@AutoValue
public abstract class Variable {

	private static final String MULTI_VALUE_SEPARATOR = "#";
	
	public abstract String getName();
	public abstract VarType getType();
	/** possible values for this variable or null */
	public abstract List<Object> getOptions();
	/** marks variable as customer preference (requirement) or product property */
	public abstract boolean isPreference();
	
	public static Builder builder() {
		return new AutoValue_Variable.Builder()
				.setPreference(false)
				.setOptions(Collections.emptyList());
	}
	
	@AutoValue.Builder
	public abstract static class Builder {
		public abstract Builder setName(String value);
		public abstract Builder setType(VarType value);
		public abstract Builder setOptions(List<Object> value);
		public abstract Builder setPreference(boolean value);
		public abstract Variable build();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	public static enum VarType {
		STRING("string", false, false, s -> s),
		STRING_ARRAY("string[]", true, false, s -> parserArray(s, stream -> stream.toArray(i -> new String[i]))),
		INTEGER("int", false, true, Integer::parseInt),
		INTEGER_ARRAY("int[]", true, true, s -> parserArray(s, stream -> stream.map(Integer::parseInt).toArray(i -> new Integer[i]))),
		INTEGER_MAP("map[String,Integer]", true, true, VarType::throwUnsupportedException),	// TODO implement map parser
		LONG("long", false, true, Long::parseLong),
		LONG_ARRAY("long[]", true, true, s -> parserArray(s, stream -> stream.map(Long::parseLong).toArray(i -> new Long[i]))),
		DOUBLE("double", false, true, Double::parseDouble),
		DOUBLE_ARRAY("double[]", true, true, s -> parserArray(s, stream -> stream.map(Double::parseDouble).toArray(i -> new Double[i])))
		;
		
		private final boolean array;
		private final String id;
		private final Function<String, Object> valueParser;
		private final boolean numeric;
		
		private VarType(String id, boolean isArray, boolean numeric, Function<String, Object> valueParser) {
			this.id = id;
			this.array = isArray;
			this.valueParser = valueParser;
			this.numeric = numeric;
		}

		/**
		 * @return the array
		 */
		public boolean isArray() {
			return array;
		}

		/**
		 * @return the id
		 */
		public String getId() {
			return id;
		}
		
		public boolean isNumeric() {
			return numeric;
		}
		
		/**
		 * 
		 * @param name
		 * @return
		 */
		public static VarType forName(String name) {
			for (VarType varType : VarType.values()) {
				if (varType.id.equalsIgnoreCase(name)) {
					return varType;
				}
			}
			
			// map all other to string
			return VarType.STRING;
		}		
				
		/**
		 * 
		 * @param variable
		 * @param value
		 * @return
		 * @throws ProductFinderException
		 */
		public Object parseValue(String variable, String value) throws ProductFinderException {
			if (StringUtils.trimToNull(value) == null) {
				return this == VarType.STRING ? value : null;
			}
			
			try {
				return valueParser.apply(value);
			} catch (Exception e) {
				throw new ProductFinderException("Failed to parse value for variable: %s, type: %s, value: %s", variable, this, value, e);
			}	
		}
		
		/**
		 * 
		 * @param s
		 * @param valueParser
		 * @return array
		 */
		private static Object parserArray(String s, Function<Stream<String>, Object> valueParser) {
			return valueParser.apply(StreamSupport.stream(Splitter.on(MULTI_VALUE_SEPARATOR)
					.split(s)
					.spliterator(), false)
				);
		}
		
		private static Object throwUnsupportedException(String value) {
			throw new UnsupportedOperationException(String.format("Parser for value: %s is not implemented!!!", value));
		}
	}
}
