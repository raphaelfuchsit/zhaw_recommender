package ls13.productfinder.api;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * <p>Title:  Recommendation result</p>
 * <p>Description:  Class holding the results of the last matching task</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author DJ
 * @version 1.0
 */
public class RecommendationResult {
	
	/** The list of applied filters */
	private final List<Filter> appliedFilters;
	/** The list of relaxed filters */
	private final List<Filter> relaxedFilters;
	/** The list of matching products */
	private final List<Product> matchingProducts;
	/** The best matching product */
	private final Product bestProduct;
	
	/**
	 * Creates an empty recommendation result
	 *
	 */
	public RecommendationResult() {
		appliedFilters = Collections.emptyList();
		relaxedFilters = Collections.emptyList();
		matchingProducts = Collections.emptyList();
		bestProduct = Product.EMPTY_PRODUCT;
	}
	
	/**
	 * Creates a recommendation result record
	 * @param appliedFilters list of applied filters
	 * @param relaxedFilters list of relaxed filters
	 * @param matchingProducts list of matching products
	 */
	public RecommendationResult(List<Filter> appliedFilters, List<Filter> relaxedFilters, List<Product> matchingProducts) {
		this.appliedFilters = Optional.ofNullable(appliedFilters).orElse(Collections.emptyList());
		this.relaxedFilters = Optional.ofNullable(relaxedFilters).orElse(Collections.emptyList());
		this.matchingProducts = Optional.ofNullable(matchingProducts).orElse(Collections.emptyList());
		this.bestProduct = this.matchingProducts.stream()
				.findFirst()
				.orElse(Product.EMPTY_PRODUCT);
	}

	/**
	 * @return the appliedFilters
	 */
	public List<Filter> getAppliedFilters() {
		return appliedFilters;
	}

	/**
	 * @return the relaxedFilters
	 */
	public List<Filter> getRelaxedFilters() {
		return relaxedFilters;
	}

	/**
	 * @return the matchingProducts
	 */
	public List<Product> getMatchingProducts() {
		return matchingProducts;
	}
	
	/**
	 * 
	 * @return best matching product
	 */
	public Product getBestProduct() {
		return bestProduct;
	}
	
	@Override
	public String toString() {
		return "RecommendationResult ["
				+ "matchingProducts=" + matchingProducts.size() 
				+ ", appliedFilters=" + appliedFilters.stream().map(Filter::getName).collect(Collectors.toList())
				+ ", relaxedFilters=" + relaxedFilters.stream().map(Filter::getName).collect(Collectors.toList()) 
				+ ", bestProduct=" + bestProduct 
				+ "]";
	}
}
