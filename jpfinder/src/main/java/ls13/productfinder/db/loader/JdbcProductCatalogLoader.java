package ls13.productfinder.db.loader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.Variable;
import ls13.productfinder.beans.ProductCatalog;
import ls13.productfinder.core.ProductParser;
import ls13.productfinder.core.ProductParser.ProductProperties;
import ls13.productfinder.interfaces.ProductCatalogLoader;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class JdbcProductCatalogLoader implements ProductCatalogLoader {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JdbcProductCatalogLoader.class);
	
    private final DataSource dataSource;
    private final String sql;


	public JdbcProductCatalogLoader(DataSource dataSource, String sql) {
		this.dataSource = dataSource;
		this.sql = sql;
	}

	@Override
	public ProductCatalog readProductData(List<Variable> variables) throws ProductFinderException {
		
		LOGGER.debug("Loading products from DB using SQL: {}", sql);
		
		try (Connection connection = dataSource.getConnection()) {
		
			final PreparedStatement statement = connection.prepareStatement(sql);
			final ResultSet resultSet = statement.executeQuery();
			
			final ResultSetMetaData metaData = resultSet.getMetaData();
			final Map<Integer, String> columnIndex = new LinkedHashMap<>();
			final int properties = metaData.getColumnCount()+1;
			
			LOGGER.debug("Preparing {} product properties...", properties);
			
			for (int i = 1; i < properties; i++) {
				final String propertyName = StringUtils.lowerCase(metaData.getColumnLabel(i));
				columnIndex.put(i, propertyName);
				LOGGER.debug("   {}. property: {}", i, propertyName);
			}
			
			final Map<String, Variable> propertiesMap = Maps.uniqueIndex(variables, Variable::getName);
			
			final List<Product> products = new ArrayList<>();
			while (resultSet.next()) {
				// process product properties
				final List<ProductProperties> list = new ArrayList<>();
				for (Entry<Integer, String> entry : columnIndex.entrySet()) {
					list.add(new ProductProperties(entry.getValue(), resultSet.getString(entry.getKey())));
				}
				products.add(ProductParser.processProperties(list, propertiesMap));
			}
			
			return ProductCatalog.builder()
					.setProducts(products)
					.build();
		
		} catch (Exception e) {
			throw new ProductFinderException("Failed to execute select statement. SQL: " + sql, e);
		}
	}

	@Override
	public String toString() {
		return "JdbcProductCatalogLoader [sql=" + sql + "]";
	}
}
