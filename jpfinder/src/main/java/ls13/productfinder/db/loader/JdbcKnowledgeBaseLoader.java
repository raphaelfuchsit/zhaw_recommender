package ls13.productfinder.db.loader;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.KnowledgeBase;
import ls13.productfinder.interfaces.KnowledgeBaseLoader;
import ls13.productfinder.xml.beans.XmlRecommendationRules;
import ls13.productfinder.xml.loader.XmlFileParser;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class JdbcKnowledgeBaseLoader implements KnowledgeBaseLoader {

	private static final Logger LOGGER = LoggerFactory.getLogger(JdbcKnowledgeBaseLoader.class);
	
	private final DataSource dataSource;
	private final String sql;
	
	public JdbcKnowledgeBaseLoader(DataSource dataSource, String sql) {
		this.dataSource = dataSource;
		this.sql = sql;
	}
	
	@Override
	public KnowledgeBase loadKnowledgeBase() throws ProductFinderException {
		
		LOGGER.debug("Loading rules XML from DB using SQL: {}", sql);
		
		try (Connection connection = dataSource.getConnection()) {
		
			final PreparedStatement statement = connection.prepareStatement(sql);
			final ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				
				final String rulesXml = resultSet.getString(1);
				
				LOGGER.debug("Loaded rules XML from DB. content:\n{}", rulesXml);
				
				return XmlFileParser.parse(new ByteArrayInputStream(rulesXml.getBytes(StandardCharsets.UTF_8)), XmlRecommendationRules.class).toKnowledgeBase();
			}
			
			throw new ProductFinderException("Select statement returned empty result set.");
		
		} catch (Exception e) {
			throw new ProductFinderException("Failed to execute select statement. SQL: " + sql, e);
		}
	}

	@Override
	public String toString() {
		return "JdbcKnowledgeBaseLoader [sql=" + sql + "]";
	}
}
