package ls13.productfinder.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.BusinessRule;
import ls13.productfinder.interfaces.RuleEngine;
import ls13.productfinder.interfaces.Scriptable;
import ls13.productfinder.interfaces.Scriptable.ScriptableCompiler;
import ls13.productfinder.utils.Utils;


/**
 * A very simple rule engine
 * @author dietmar
 *
 */
public class RuleEngineImpl implements RuleEngine {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RuleEngineImpl.class);
	
	private final int maxIterations;
	
	/** The list of business rules */
	private final List<CompiledBusinessRule> businessRules;

	/**
	 * 
	 * @param engine
	 * @param businessRules
	 * @param maxIterations
	 * @throws ProductFinderException 
	 */
	public RuleEngineImpl(ScriptableCompiler scriptableCompiler, List<BusinessRule> businessRules, int maxIterations) throws ProductFinderException {
		this.maxIterations = maxIterations;
		this.businessRules = compile(scriptableCompiler, Optional.ofNullable(businessRules).orElse(Collections.emptyList()));
	}
	
	/**
	 * Pre-compiles the business rules
	 * @param businessRules 
	 * @param scriptableCompiler 
	 * @throws ProductFinderException
	 */
	private List<CompiledBusinessRule> compile(ScriptableCompiler scriptableCompiler, List<BusinessRule> businessRules) throws ProductFinderException {
		BusinessRule lastBr = null;
		try {
			final List<CompiledBusinessRule> list = new ArrayList<>();
			for (BusinessRule br : businessRules) {
				lastBr = br;
				list.add(new CompiledBusinessRule(br.getName(), 
						br.getCodeIf(), scriptableCompiler.compile(br.getCodeIf(), Boolean.class, false, String.format("BusinessRule: %s,  if condition", br.getName())), 
						br.getCodeThen(), scriptableCompiler.compile(br.getCodeThen(), Object.class, false, String.format("BusinessRule: %s,  then condition", br.getName()))));	
			}
			return list;
			
		} catch (ProductFinderException e) {
			throw new ProductFinderException("[FATAL] Failed to compile business rule: " + lastBr, e);
		}	
	}

	/**
	 * Applies the defined business rules
	 * 
	 * @param user input the current set of variables
	 * @return new user input
	 */
	@Override
	public Map<String, Object> applyRules(Map<String, Object> userInputs) throws ProductFinderException {
		
		if (businessRules.isEmpty()) {
			return userInputs;
		}
		
		// A local list of all previously found states
		// remember the original state in the list
		Map<String, Object> currentValues = new HashMap<>(userInputs);
		final List<Map<String, Object>> previousStates = new ArrayList<>();
		previousStates.add(currentValues);
		int counter = 0;
		while (true) {
			
			// Break operation if too many iterations
			if (++counter > maxIterations) {
				throw new ProductFinderException("Rule engine problem: Maximum number of iterations %s reached.", maxIterations);
			}
			
			LOGGER.debug("[Iteration: {}] start processing current inputs: {}", counter, currentValues);
			
			// Iterate over the business rules and check if there is something to do
			final Map<String, Object> newValues = new HashMap<>(userInputs);
			for (CompiledBusinessRule br : businessRules) {
				final Boolean result = br.getCompiledIf().execute(currentValues);
				if (result != null && result) {
					// run then script and process results
					LOGGER.debug("[Iteration: {}] business rule: {} matched. {}", counter, br.getName(), br);
					
					final Map<String, Object> variableBindings = new HashMap<>(currentValues);
					br.getCompiledThen().execute(variableBindings, true);
					
					boolean valueUpdated = false;
					for (Entry<String, Object> entry : variableBindings.entrySet()) {
						final String variable = entry.getKey();
						final Object resultValue = entry.getValue();
						final Object origValue = userInputs.get(variable);
						// update value if it has changed
						if (!Objects.equals(resultValue, origValue)) {
							newValues.put(variable, resultValue);
							valueUpdated = true;
							LOGGER.debug("[Iteration: {}] business rule: {} applied to variable: {}, old value: {}, current value: {}", 
									counter, br.getName(), variable, origValue, resultValue);
						}
					}
					
					if (!valueUpdated) {
						LOGGER.debug("[Iteration: {}] business rule: {} matched, but not updates applied.", counter, br.getName());
					}
				}
			}
			
			// Store the new values
			final Map<String, Object> lastValues = previousStates.get(previousStates.size()-1);
			previousStates.add(newValues);
			
			// Compare the new values with the last values
			if (Utils.identical(newValues, lastValues)) {
				// no more changes, ow we are done. Return the last new values
				return newValues;
			}
			
			// Find out whether there are loops in the state
			// Do not compare the first two ones
			if (previousStates.size() > 2) {
				// Compare with all but the last state
				for (int i = 0; i < previousStates.size() - 1; i++) {
					final Map<String, Object> oldMap = previousStates.get(i);
					// We already had that situation before
					if (Utils.identical(oldMap,newValues)) {
						throw new ProductFinderException("Loop in value state in rule engine - check your business rules (Iteration: %s)", counter);
					}
				}
			}
			
			currentValues = new HashMap<>(newValues);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	private static class CompiledBusinessRule {
		
		/** the name of the rule */
		private final String name;
		/** The script code of the "if" part */
		private final String codeIf;
		/** The script code of the "then" part */
		private final String codeThen;
		/** The compiled condition */
		private final Scriptable<Boolean> compiledIf;
		/** The compiled consequent */
		private final Scriptable<Object> compiledThen;

		public CompiledBusinessRule(String name, String codeIf, Scriptable<Boolean> compiledIf, String codeThen,
				Scriptable<Object> compiledThen) {
			this.name = name;
			this.codeIf = codeIf;
			this.codeThen = codeThen;
			this.compiledIf = compiledIf;
			this.compiledThen = compiledThen;
		}
		
		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}
		/**
		 * @return the compiledIf
		 */
		public Scriptable<Boolean> getCompiledIf() {
			return compiledIf;
		}
		/**
		 * @return the compiledThen
		 */
		public Scriptable<Object> getCompiledThen() {
			return compiledThen;
		}
		
		@Override
		public String toString() {
			return "CompiledBusinessRule [name=" + name + ", codeIf=" + codeIf + ", codeThen=" + codeThen + "]";
		}
	}
}
