package ls13.productfinder.core;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ls13.productfinder.interfaces.DescriptionGenerator;
import ls13.productfinder.interfaces.Scriptable;
import ls13.productfinder.interfaces.Scriptable.ScriptableCompiler;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class DescriptionFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(DescriptionFactory.class);
	
	private static final boolean TRACE = false;
	
	private DescriptionFactory() {}
	
	/**
	 * Creates a {{@link DescriptionGenerator}
	 * 
	 * @param code
	 * @param scriptableCompiler
	 * @param label
	 * @return description generator
	 */
	public static DescriptionGenerator createGenerator(String code, ScriptableCompiler scriptableCompiler, String label) {
		 
		try {
			if (StringUtils.isNotBlank(code)) {
				// check if we must compile a scriptable
				final Scriptable<String> scriptable = scriptableCompiler.compile(code, String.class, false, String.format("DescriptionGenerator: ", label));
				LOGGER.debug("Description of '{}' uses javascript: '{}'", label, code);
				return i -> scriptable.execute(i);
			}	
		} catch (Exception e) {
			if (TRACE) {
				LOGGER.debug("Failed to compile javascript for description: {}, code: {}", label, code, e);
			}
		}
		
		LOGGER.debug("Description of '{}' uses plain text: '{}'", label, code);
		return i -> code;	// just return as string
	}
}
