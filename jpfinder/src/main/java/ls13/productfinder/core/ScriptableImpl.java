package ls13.productfinder.core;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.JsFunction;
import ls13.productfinder.interfaces.Scriptable;

/**
 * Generic executor for JavaScript
 * @author streuch1
 */
public class ScriptableImpl<T> implements Scriptable<T> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ScriptableImpl.class);
	
	private static final String ENGINE_NAME = "JavaScript";
	
	private static final Pattern FUNCTION_PATTERN = Pattern.compile("^function ([^\\(]+)\\(.+$", 
				Pattern.MULTILINE + Pattern.DOTALL);
	
	private static final AtomicLong ID_GEN = new AtomicLong();
	
	private final Class<T> resultClazz;
	private final String code;
	private final ScriptEngine scriptEngine;
	private final CompiledScript compiledScript;
	private final ExecutionMode executionMode;
	private final EngineType engineType;
	private final boolean registerFunction;
	private final String description;

	/**
	 * @return script compiler
	 */
	public static ScriptableCompiler createCompiler() {
		return createCompiler(Collections.emptyList());
	}
	
	/**
	 * @param functionLibrary 
	 * @return script compiler
	 */
	public static ScriptableCompiler createCompiler(List<JsFunction> functionLibrary) {
		return new ScriptableCompiler() {
			@Override
			public <T> Scriptable<T> compile(String code, Class<T> resultClazz, boolean registerFunction, Object description) throws ProductFinderException {
				return new ScriptableImpl<>(code, resultClazz, registerFunction, description, functionLibrary);
			}
		};
	}
	
	/**
	 * 
	 * @param scriptCode
	 * @param resultClazz
	 * @param registerFunction
	 * @param scriptDescription
	 * @param functionLibrary
	 * @throws ProductFinderException
	 */
	public ScriptableImpl(String scriptCode, Class<T> resultClazz, boolean registerFunction, Object scriptDescription, List<JsFunction> functionLibrary) throws ProductFinderException {
		this.code = Preconditions.checkNotNull(scriptCode).trim();
		this.resultClazz = Preconditions.checkNotNull(resultClazz);
		this.registerFunction = registerFunction;
		this.description = Optional.ofNullable(scriptDescription).map(Object::toString).orElse("n/a");
		
		this.scriptEngine = new ScriptEngineManager().getEngineByName(ENGINE_NAME);
		if (scriptEngine == null) {
			throw new ProductFinderException("Could not access java script engine " + ENGINE_NAME + ". " + getAvailableEngines());
		} else {
			LOGGER.debug("Script engine for type {} created: {}. bindings: {}", ENGINE_NAME, scriptEngine, scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE).entrySet());
		}
        
		this.engineType = detectEngineType(scriptEngine);
		if (engineType == EngineType.RHINO) {
			System.setProperty("rhino.opt.level", "9");
		}
		this.executionMode = calculateExecutionMode();
		
		final boolean logOnce = ID_GEN.get() == 1;	// log only once
		if (logOnce) {
			LOGGER.info("Script engine type detected: {}, execution mode calculated: {}", engineType, executionMode);
		}
		
		// setup engine trace
		final String traceId = Long.toString(ID_GEN.incrementAndGet());
		final ScriptContext context = scriptEngine.getContext();
		context.setWriter(new LogWriter(description, "STDOUT", traceId));
        context.setErrorWriter(new LogWriter(description, "STDERR", traceId));
        
		LOGGER.info("Going to initialize script using trace-id: {}, code: {}", traceId, code);
		
		this.compiledScript = compile(code, registerFunction, functionLibrary, logOnce);
	}

	private CompiledScript compile(String code, boolean registerFunction, List<JsFunction> functionLibrary, boolean logOnce) throws ProductFinderException {
		// init code
		try {
			if (functionLibrary != null) {
				// add default functions
				compileFunctionLibrary(functionLibrary, logOnce);
			}
			
			// join scripts
			if (registerFunction) {
				final String functionName = parseFunctionName(code);
				
				LOGGER.debug("Registering function: {}, code: {}", functionName, code);
				
				scriptEngine.eval(code);
				code = functionName + "()";
			}
			
			switch (executionMode) {
			case COMPILED:
				return ((Compilable) scriptEngine).compile(code);
			case SIMPLE:
				return null;

			default:
				throw new ProductFinderException("Unsupported execution mode: " + executionMode);
			}
			
		} catch (ScriptException e) {
			throw new ProductFinderException("[%s] Error during initialization, JS engine threw an error during the evaluation of the code: %s", description, code, e);
		}
	}

	/**
	 * 
	 * @param code
	 * @return function name
	 * @throws ProductFinderException
	 */
	private String parseFunctionName(String code) throws ProductFinderException {
		final Matcher matcher = FUNCTION_PATTERN.matcher(code);
		if (!matcher.matches()) {
			throw new ProductFinderException("[%s] Cannot parse function name from code. pattern: %s, content: %s", 
					description, FUNCTION_PATTERN, code);
		}
		return matcher.group(1).trim();
	}
	
	/**
	 * 
	 * @param functionLibrary
	 * @param logOnce
	 * @throws ProductFinderException
	 */
	private void compileFunctionLibrary(List<JsFunction> functionLibrary, boolean logOnce) throws ProductFinderException {
		JsFunction currentFunction = null;
		try {
			final Set<String> functionNames = new HashSet<>();
			for (JsFunction jsFunction : functionLibrary) {
				currentFunction = jsFunction;
				
				final String functionCode = StringUtils.trimToNull(jsFunction.getFunctionCode());
				final String functionName = parseFunctionName(functionCode);
				if (!functionNames.add(functionName)) {
					if (logOnce) {
						LOGGER.error("Duplicate name: {} in function library, check configuration of function: {}", 
								currentFunction.getFunctionName(), currentFunction.getFunctionCode());
					}	
					continue;
				}
				
				scriptEngine.eval(functionCode);
				
				if (logOnce) {
					LOGGER.info("added library function: {}, code: {}", functionName, functionCode);
				}
			}
		} catch (ScriptException e) {
			throw new ProductFinderException("Error during initialization of function libarary. function: %s, %s", 
					currentFunction.getFunctionName(), currentFunction.getFunctionCode(), e);
		}
	}

	/**
	 * @return execution mode
	 */
	private ExecutionMode calculateExecutionMode() {
		return scriptEngine instanceof Compilable ? ExecutionMode.COMPILED : ExecutionMode.SIMPLE;
	}


	/**
	 * 
	 * @param variableBindings
	 * @param updateBindings if set, binding values are updated with values from script context after execution
	 * @return script result
	 * @throws ProductFinderException
	 */
	@Override
	public synchronized T execute(Map<String, Object> variableBindings, boolean updateBindings) throws ProductFinderException {

		// bind data
		final Bindings bindings = scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE);
//		bindings.clear();	// causes memory leak
		
		try {
			if (variableBindings != null) {
				for (Map.Entry<String, ?> entry : variableBindings.entrySet()) {
					bindings.put(entry.getKey(), entry.getValue());
				}
			}	
			
			// call check function, evaluate result
			final Object result;
			switch (executionMode) {
			case COMPILED:
				result = compiledScript.eval(bindings);
				break;
			case SIMPLE:
				result = scriptEngine.eval(code, bindings);
				break;
			default:
				throw new ProductFinderException("Code is running in an unknown mode: " + executionMode);
			}
			
			if (updateBindings && variableBindings != null) {
				// update bindings with values from  script context
				for (String variable : variableBindings.keySet()) {
					variableBindings.put(variable, scriptEngine.get(variable));
				}
			}
			
			if (resultClazz.isInstance(result)) {
				return resultClazz.cast(result);
			}
			
			throw new ProductFinderException("[%s] Script returned invalid result: %s, expected type: %s", 
					description, result, resultClazz);
			
		} catch (ScriptException e) {
			throw new ProductFinderException("[%s] Error during script execution. Check stack trace for details. variables: %s", 
					description, variableBindings, e);
		} finally {
			if (variableBindings != null) {
				for (String variable : variableBindings.keySet()) {
					bindings.put(variable, null);
				}
			}
		}
	}
	
	private static String getAvailableEngines() {
		StringBuilder sb = new StringBuilder();
		List<ScriptEngineFactory> engineFactories = new ScriptEngineManager().getEngineFactories();
		sb.append("Available script engines (" + engineFactories.size() + " implementation(s))").append(engineFactories.isEmpty() ? "" : "\n");
		for (Iterator<ScriptEngineFactory> iterator =  engineFactories.iterator(); iterator.hasNext();) {
			final ScriptEngineFactory factory = iterator.next();
			sb.append(factory.getEngineName()).append(" ").append(factory.getEngineVersion()).append(" ");
			sb.append("(").append(factory.getLanguageName()).append(" ").append(factory.getLanguageVersion()).append(") ");
			sb.append("registered as ").append(factory.getNames());
			
			if (iterator.hasNext()) {
				sb.append("\n");
			}
		}
		return sb.toString();
	}
	
	/**
	 * detects the type of a javascript engine
	 * @param engine engine
	 * @return engine type
	 * @throws ProductFinderException
	 */
	private EngineType detectEngineType(ScriptEngine engine) throws ProductFinderException {
		final String engineClassNameLc = engine.getClass().getName().toLowerCase();
		
		if (engineClassNameLc.contains("nashorn")) {
			return EngineType.NASHORN;
		}
		if (engineClassNameLc.contains("rhino")) {
			return EngineType.RHINO;
		}
		throw new ProductFinderException("JavaScript engine " + engineType + " does not appear to be of any of the supported types: " + Arrays.toString(EngineType.values()));
	}
	
	@Override
	public String toString() {
		return "ScriptableImpl [engineType=" + engineType 
				+ ", executionMode=" + executionMode 
				+ ", registerFunction=" + registerFunction
				+ ", description=" + description
				+ ", code=" + code + "]";
	}
	
	
	
	private static enum ExecutionMode {
		SIMPLE,
		COMPILED;
	}
	
	private static enum EngineType {
		RHINO,
		NASHORN;
	}
	
	
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	private static class LogWriter extends Writer {

		private static final Logger LOGGER = LoggerFactory.getLogger(LogWriter.class);
		
		private final String description;
		private final String stream;
		private final String codeId;
		
		private final StringBuffer buffer = new StringBuffer();

		public LogWriter(String description, String stream, String codeId) {
			this.description = description;
			this.stream = stream;
			this.codeId = codeId;
		}
		
		@Override
		public void write(char[] cbuf, int off, int len) throws IOException {
			for (int i = off; i < len; i++) {
				final char c = cbuf[i];
				if (c != '\n' && c != '\r') {
					buffer.append(c);
					continue;
				}
				
				writeBuffer();
			}
		}

		private void writeBuffer() {
			if (buffer.length() == 0) {
				return;
			}
			
			LOGGER.debug("[{}] [{}] [js-trace-{}] - {}", description, stream, codeId, buffer);
			
			// clear buffer
			buffer.delete(0, buffer.length());
			if (buffer.length() > 1024) {
				buffer.trimToSize();
			}	
		}

		@Override
		public void flush() throws IOException {
			writeBuffer();
		}

		@Override
		public void close() throws IOException {
			writeBuffer();
		}
	}
}