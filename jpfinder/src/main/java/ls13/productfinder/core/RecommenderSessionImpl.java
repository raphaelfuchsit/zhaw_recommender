package ls13.productfinder.core;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.RecommendationResult;
import ls13.productfinder.api.RecommenderEngine;
import ls13.productfinder.api.RecommenderSession;
import ls13.productfinder.api.Variable;
import ls13.productfinder.api.Variable.VarType;


/**
 * <p>Title:  Recommender session object</p>
 * <p>Description:  Objects of this class can be associated with user sessions.</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author DJ
 * @version 1.0
 */

public class RecommenderSessionImpl implements RecommenderSession {
	
	/** A handle to the recommender engine */
	private final RecommenderEngine engine;
	
	/** The list of current user inputs */
	private final Map<String, Object> userInputs = new LinkedHashMap<>();
	
	/** A handle to the last result */
	private RecommendationResult lastResult = new RecommendationResult();
	
	/**
	 * Creates a session object for a given engine
	 * @param engine the initialized engine
	 */
	public RecommenderSessionImpl(RecommenderEngine engine) {
		this.engine = engine;
		// init available variables
		clearInput();
	}
	
	@Override
	public RecommendationResult computeRecommendationResult(int relax) throws ProductFinderException {
		lastResult = engine.computeRecommendationResult(this, relax);
		return lastResult;
	}

	@Override
	public List<Product> getProductsForExpression(String expression) throws ProductFinderException {
		return engine.getProductsForFilterExpression(expression);
	}
	
	@Override
	public String getInfoText(String name, Product product) throws ProductFinderException {
		return engine.getText(this, name, product);
	}

	@Override
	public void sortResults(String property, SortOrder sortOrder) throws ProductFinderException {
		
		// use ascending as default
		sortOrder = Optional.ofNullable(sortOrder).orElse(SortOrder.ASC);
		
		// do nothing, if no result exists
		final List<Product> result = Optional.ofNullable(lastResult)
			.map(RecommendationResult::getMatchingProducts)
			.orElse(Collections.emptyList());
		
		if (result.isEmpty()) {
			return;
		}
		
		// If this is a multi-valued property or is not known, throw an exception
		final Variable variable = engine.getVariable(property);
		if (Optional.ofNullable(variable)
				.map(Variable::getType)
				.map(VarType::isArray)
				.orElse(true)) {
			throw new ProductFinderException("[FATAL] Sort variable: %s for sorting is not known or multi-valued.", property);
		}
		
		final ProductComparator pc = new ProductComparator(property, variable.getType().isNumeric());
		Collections.sort(result, SortOrder.ASC == sortOrder ? pc : pc.reversed());
	}
	
	@Override
	public List<Variable> getVariables() {
		return engine.getUserVariables();
	} 
	
	@Override
	public void setInput(String variable, Object value) {
		userInputs.put(variable, value);
	}
	
	@Override
	public RecommendationResult getLastResult() {
		return lastResult;
	}
	
	@Override
	public Map<String, Object> getUserInputs() {
		return Collections.unmodifiableMap(userInputs);
	}
	
	@Override
	public void clearInput() {
		userInputs.clear();
		for (Variable variable : engine.getUserVariables()) {
			userInputs.put(variable.getName(), null);
		}
	}
	
	@Override
	public String toString() {
		return "RecommenderSessionImpl [userInputs=" + userInputs + ", lastResult=" + lastResult + "]";
	}
}
