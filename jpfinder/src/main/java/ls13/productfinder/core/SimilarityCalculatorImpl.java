package ls13.productfinder.core;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.JsFunction;
import ls13.productfinder.interfaces.Scriptable;
import ls13.productfinder.interfaces.Scriptable.ScriptableCompiler;
import ls13.productfinder.interfaces.SimilarityCalculator;


/**
 * <p>Title:  Computation of Similarities</p>
 * <p>Description:  Contains a method to compute the similarity between two given products</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author DJ
 * @version 1.0
 */

public class SimilarityCalculatorImpl implements SimilarityCalculator {
	
	private static final String P_P1 = "p1";
	private static final String P_P2 = "p2";
	
	private final JsFunction similarityFunction;
	private final List<Product> products;
	private final Scriptable<Number> compiledSimilarityFunction;
	
	/**
	 * 
	 * @param products
	 * @param similarityFunction
	 * @throws ProductFinderException 
	 */
	public SimilarityCalculatorImpl(ScriptableCompiler scriptableCompiler, List<Product> products, JsFunction similarityFunction) throws ProductFinderException {
		this.products = products;
		this.similarityFunction = similarityFunction;
		this.compiledSimilarityFunction = compile(scriptableCompiler, similarityFunction);
	}

	/**
	 * 
	 * @param scriptableCompiler 
	 * @param similarityFunction
	 * @return
	 * @throws ProductFinderException
	 */
	private Scriptable<Number> compile(ScriptableCompiler scriptableCompiler, JsFunction similarityFunction) throws ProductFinderException{
		try {
			if (!Optional.ofNullable(similarityFunction)
					.map(JsFunction::getFunctionCode)
					.map(StringUtils::trimToNull)
					.isPresent()) {
				return null;
			}
			
			// register the function
			return scriptableCompiler.compile(similarityFunction.getFunctionCode(), Number.class, true, 
					String.format("SimilarityFunction: %s", similarityFunction.getFunctionName()));

		} catch (Exception e) {
			throw new ProductFinderException("Error in similarity function: %s", similarityFunction, e);
		}
	}

	/**
	 * Method that evaluates the scripted similarity function for two given products
	 */
	private double computeSimilarity(Product product1, Product product2) throws ProductFinderException {
		
		final Map<String, Object> variableBindings = new HashMap<>();
		variableBindings.put(P_P1, product1.getProperties());
		variableBindings.put(P_P2, product2.getProperties());
		
		final Number number = compiledSimilarityFunction.execute(variableBindings);
		return number == null ? -1 : number.doubleValue();
	}
	
	@Override
	public List<Product> getSimilarProducts(Product testProduct, int howmany) throws ProductFinderException {
		Preconditions.checkState(compiledSimilarityFunction != null);
		
		final List<SimilarityTuple> result = new ArrayList<>();
		for (Product product : products) {
			if (testProduct != product) {
				result.add(new SimilarityTuple(product, computeSimilarity(testProduct, product)));
			}
		}
		
		// we need to sort the list, highest values first
		return result.stream()
			.sorted(Comparator.comparingDouble(SimilarityTuple::getSimvalue))
			.limit(howmany <= 0 ? Integer.MAX_VALUE : howmany)
			.map(SimilarityTuple::getProduct)
			.collect(Collectors.toList());
	}
	
	
	private static final class SimilarityTuple {
		private final Product product;
		private final double simvalue;
		
		private SimilarityTuple (Product product, double simvalue) {
			this.product = product;
			this.simvalue = simvalue;
		}

		/**
		 * @return the simvalue
		 */
		public double getSimvalue() {
			return simvalue;
		}

		/**
		 * @return the product
		 */
		public Product getProduct() {
			final Map<String, Object> properties = new HashMap<>(product.getProperties());
			properties.put(Product.SIMILARITY_DENOMINATOR, simvalue);
			return Product.builder()
					.setProperties(properties)
					.build();
		}
	}

	@Override
	public String toString() {
		return "SimilarityCalculatorImpl [similarityFunction=" + similarityFunction + ", compiledSimilarityFunction="
				+ compiledSimilarityFunction + "]";
	}
}
