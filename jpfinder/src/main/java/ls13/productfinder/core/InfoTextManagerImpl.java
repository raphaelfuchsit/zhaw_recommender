package ls13.productfinder.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.InfoText;
import ls13.productfinder.beans.TextVariant;
import ls13.productfinder.interfaces.DescriptionGenerator;
import ls13.productfinder.interfaces.InfoTextManager;
import ls13.productfinder.interfaces.Scriptable;
import ls13.productfinder.interfaces.Scriptable.ScriptableCompiler;



/**
 * <p>Title:  InfoText-Manager</p>
 * <p>Description:  Class to manage variable infotexts</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * 
 * @author DJ
 * @version 1.0
 */
public class InfoTextManagerImpl implements InfoTextManager {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InfoTextManagerImpl.class);
	
	/** available info texts */
	private final Map<String, InfoTextRecord> infoTexts;
	
	public InfoTextManagerImpl(ScriptableCompiler scriptableCompiler, List<InfoText> infoTexts) throws ProductFinderException {
		this.infoTexts = compile(scriptableCompiler, infoTexts);
	}
	
	/**
	 * Precompiles the condition and the filter expression into java classes and stores
	 * script in object
	 * @param scriptableCompiler 
	 * @param infoTexts 
	 * @return 
	 * @throws ProductFinderException in case of errors in filter expressions
	 */
	private Map<String, InfoTextRecord> compile(ScriptableCompiler scriptableCompiler, List<InfoText> infoTexts) throws ProductFinderException {
		
		final Map<String, InfoTextRecord> map = new HashMap<>();
		for (InfoText infoText : infoTexts) {
			// iterate over all the variants and compile the condition
			final List<CompiledTextVariant> compiledTextVariants = new ArrayList<>();
			for (TextVariant textVariant : infoText.getTextVariants()) {
				compiledTextVariants.add(new CompiledTextVariant(textVariant.getCondition(), textVariant.getText(),
						scriptableCompiler.compile(textVariant.getCondition(), Boolean.class, false, String.format("InfoText: %s,  text variant condition", infoText.getName())),
						DescriptionFactory.createGenerator(textVariant.getText(), scriptableCompiler, "text variant")
					));
			}
			
			map.put(infoText.getName(), new InfoTextRecord(infoText.getName(), infoText.getDefaultText(), compiledTextVariants, 
							DescriptionFactory.createGenerator(infoText.getDefaultText(), scriptableCompiler, String.format("info text: %s", infoText.getName()))
						)
				);
		}
		
		return map;
	}
	
	@Override
	public String getText(String name, Map<String, Object> userInputs, Product product) throws ProductFinderException {
		
		final Map<String, Object> properties = new HashMap<>(userInputs);
		if (product != null) {
			properties.putAll(product.getProperties());
		}
		
		LOGGER.debug("prcoessing lookup for name: {}, input: {}", name, properties);
		
		final InfoTextRecord infoTextRecord = infoTexts.get(name);
		if (infoTextRecord == null) {
			throw new ProductFinderException("[ERROR] InfoText with name: %s is not defined", name);
		}
		
		for (CompiledTextVariant textVariant : infoTextRecord.getTextVariants()) {
			final Boolean result = textVariant.getCompiledCondition().execute(properties);
			if (result != null && result) {
				return textVariant.getTextGenerator().create(properties);
			}
		}
		return infoTextRecord.getDefaultTextGenerator().create(properties);
	}
	
	
	
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	private static class InfoTextRecord {
		
		private final String name;
		private final String defaultText;
		private final List<CompiledTextVariant> textVariants;
		
		private final DescriptionGenerator defaultTtextGenerator;

		public InfoTextRecord(String name, String defaultText, List<CompiledTextVariant> textVariants, DescriptionGenerator defaultTtextGenerator) {
			this.name = name;
			this.defaultText = defaultText;
			this.textVariants = textVariants;
			this.defaultTtextGenerator = defaultTtextGenerator;
		}

		/**
		 * @return the textVariants
		 */
		public List<CompiledTextVariant> getTextVariants() {
			return textVariants;
		}

		/**
		 * @return the textGenerator
		 */
		public DescriptionGenerator getDefaultTextGenerator() {
			return defaultTtextGenerator;
		}
		
		@Override
		public String toString() {
			return "InfoTextRecord [name=" + name + ", defaultText=" + defaultText + ", textVariants=" + textVariants
					+ "]";
		}
	}
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	private static class CompiledTextVariant {
		
		private final String condition;
		private final String text;
		
		private final Scriptable<Boolean> compiledCondition;
		private final DescriptionGenerator textGenerator;

		/**
		 * 
		 * @param condition
		 * @param text
		 * @param compiledCondition
		 */
		public CompiledTextVariant(String condition, String text, Scriptable<Boolean> compiledCondition, DescriptionGenerator textGenerator) {
			this.condition = condition;
			this.text = text;
			this.compiledCondition = compiledCondition;
			this.textGenerator = textGenerator;
		}

		/**
		 * @return Returns the compiledCondition.
		 */
		public Scriptable<Boolean> getCompiledCondition() {
			return compiledCondition;
		}
		
		/**
		 * @return the textGenerator
		 */
		public DescriptionGenerator getTextGenerator() {
			return textGenerator;
		}

		@Override
		public String toString() {
			return "CompiledTextVariant [condition=" + condition + ", text=" + text + "]";
		}
	}
}