package ls13.productfinder.core;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.Variable;
import ls13.productfinder.api.Variable.VarType;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class ProductParser {

//	private static final String MULTI_VALUE_SEPARATOR = "#";
	
	private ProductParser() {}
	
	/**
	 * 
	 * @param productPropertiesList product properties list
	 * @param variableMap existing variables from knowledge base 
	 * @return product 
	 * @throws ProductFinderException 
	 */
	public static final Product processProperties(List<ProductProperties> productPropertiesList, Map<String, Variable> variableMap) throws ProductFinderException {
		
		final Map<String, Object> properties = new LinkedHashMap<>();
		for (ProductProperties productProperties : productPropertiesList) {

			// Found an attribute: remember values
			final String aName = StringUtils.trimToEmpty(productProperties.getName());
			final String aValue = StringUtils.trimToEmpty(productProperties.getValue());
			final VarType varType = Optional.ofNullable(variableMap.get(aName))
					.map(Variable::getType).orElse(VarType.STRING);
			
			properties.put(aName, varType.parseValue(aName, aValue));
			
//			// Split up multi-value entries based on the separator (#)
//			// Convert elements to double if defined as doubles or ints
//			if (aValue.indexOf(MULTI_VALUE_SEPARATOR) != -1) {
//				
//				// Separate multi-valued elements
//				final List<String> sValues = new ArrayList<>();
//				final List<Double> dValues = new ArrayList<>();
//				for (String part : Splitter.on(MULTI_VALUE_SEPARATOR).split(aValue)) {
//					if (VarType.DOUBLE_ARRAY == varType) {
//						try {
//							dValues.add(Double.parseDouble(part));
//						} catch (Exception e) {
//							// TODO ignore values...
//						}
//						sValues.add(part);
//					}
//				}
//				
//				// Convert into an array
//				if (VarType.DOUBLE_ARRAY == varType) {
//					properties.put(aName, dValues.toArray(new Double[dValues.size()]));
//				} else {
//					properties.put(aName, sValues.toArray(new String[sValues.size()]));
//				}
//
//				continue;
//			}
//
//			// Single value treatment. Try to convert to double if numeric
//			if (VarType.INTEGER == varType) {
//				try {
//					properties.put(aName, Integer.parseInt(aValue));
//					continue;
//				} catch (Exception e) {
//					// TODO ignore values...
//				}
//			}
//			if (VarType.DOUBLE == varType) {
//				try {
//					properties.put(aName, Double.parseDouble(aValue));
//					continue;
//				} catch (Exception e) {
//					// TODO ignore values...
//				}
//			}
//			
//			properties.put(aName, aValue);
		}
		
		return Product.builder()
				.setProperties(Collections.unmodifiableMap(properties))
				.build();
	}

	
	
	/**
	 * 
	 * @author cst
	 *
	 */
	public static class ProductProperties {
		
		private final String name;
		private final String value;

		public ProductProperties(String name, String value) {
			this.name = name;
			this.value = value;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return "ProductProperties [name=" + name + ", value=" + value + "]";
		}
	}
}
