package ls13.productfinder.core;

import java.util.Comparator;

import ls13.productfinder.api.Product;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class ProductComparator implements Comparator<Product> {
	
	private final String property;
	private final boolean numeric;
	
	public ProductComparator(String property, boolean numeric) {
		this.property = property;
		this.numeric = numeric;
	}

	@Override
	public int compare(Product o1, Product o2) {
		
		final Object v1 = o1.getProperties().get(property);
		final Object v2 = o2.getProperties().get(property);
		
		if (v1 == null && v2 == null) {
			return 0;
		}
		if (v1 == null && v2 != null) {
			return 1;
		}
		if (v1 != null && v2 == null) {
			return -1;
		}

		// return different values, depending on the type
		if (this.numeric) {
			double d1 = ((Number) v1).doubleValue();
			double d2 = ((Number) v2).doubleValue();
			return Double.compare(d1, d2);
		}

		return ((String) v1).compareTo((String) v2);
	}
}