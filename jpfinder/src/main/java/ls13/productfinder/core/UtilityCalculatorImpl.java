package ls13.productfinder.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.JsFunction;
import ls13.productfinder.interfaces.Scriptable;
import ls13.productfinder.interfaces.Scriptable.ScriptableCompiler;
import ls13.productfinder.interfaces.UtilityCalculator;


/**
 * A class to calculate utility values for a set of products
 * @author dietmar
 *
 */
public class UtilityCalculatorImpl implements UtilityCalculator {
	
//	private static final Logger LOGGER = LoggerFactory.getLogger(UtilityCalculatorImpl.class);
	
	private final JsFunction utilityFunction;
	
	/** A handle to the compiled utility funciton */
	private final Scriptable<Number> compiledUtilityFunction;
	
	public UtilityCalculatorImpl(ScriptableCompiler scriptableCompiler, JsFunction utilityFunction) throws ProductFinderException {
		this.utilityFunction = utilityFunction;
		this.compiledUtilityFunction = compile(scriptableCompiler, utilityFunction);
	}

	private Scriptable<Number> compile(ScriptableCompiler scriptableCompiler, JsFunction utilityFunction) throws ProductFinderException {
		try {
			if (!Optional.ofNullable(utilityFunction)
					.map(JsFunction::getFunctionCode)
					.map(StringUtils::trimToNull)
					.isPresent()) {
				return null;
			}
			
			// register as function
			return scriptableCompiler.compile(utilityFunction.getFunctionCode(), Number.class, true, 
					String.format("UtilityFunction: %s", utilityFunction.getFunctionName()));
			
		} catch (Exception e) {
			throw new ProductFinderException("Error in utility function: %s", utilityFunction, e);
		}
	}

	@Override
	public boolean requiresStaticCalculation() {
		return utilityFunction != null 
				&& utilityFunction.getFunctionCode() != null 
				&& !utilityFunction.isDynamicCalculation();
	}

	@Override
	public boolean requiresDynamicCalculation() {
		return utilityFunction != null 
				&& utilityFunction.getFunctionCode() != null 
				&& utilityFunction.isDynamicCalculation();
	}
	
	@Override
	public List<Product> computeStaticUtilities(List<Product> products) throws ProductFinderException {
		// use only product properties as function input
		return computeDynamicUtilities(products, null);
	}

	@Override
	public List<Product> computeDynamicUtilities(List<Product> products, Map<String, Object> userInputs) throws ProductFinderException {
		Preconditions.checkState(compiledUtilityFunction != null);
		
		// Initialize the variables
		final Map<String, Object> variableBindings = new HashMap<>(userInputs != null ? userInputs : Collections.emptyMap());

//		LOGGER.debug("Calculating utility score using user inputs: {}", userInputs);
		
		final List<Product> result = new ArrayList<>();
		for (Product product : products) {
			// push the product values to the engine
			// do not overwrite previous settings of customer properties
			variableBindings.putAll(product.getProperties());
			
			final Number utility = compiledUtilityFunction.execute(variableBindings);
			
			final Map<String, Object> properties = new HashMap<>(product.getProperties());
			properties.put(Product.UTILITY_DENOMINATOR, Optional.ofNullable(utility).map(Number::doubleValue).orElse(null));
			
			result.add(Product.builder()
					.setProperties(properties)
					.build());
		}
		
		Collections.sort(result, UTILITY_COMPARATOR);
		
		return result;
	}

	@Override
	public String toString() {
		return "UtilityCalculatorImpl [utilityFunction=" + utilityFunction + ", compiledUtilityFunction="
				+ compiledUtilityFunction + "]";
	}
}
