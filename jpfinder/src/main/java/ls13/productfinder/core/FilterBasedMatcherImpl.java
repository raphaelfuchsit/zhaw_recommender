package ls13.productfinder.core;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

import ls13.productfinder.api.Filter;
import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.RecommendationResult;
import ls13.productfinder.beans.FilterRule;
import ls13.productfinder.interfaces.DescriptionGenerator;
import ls13.productfinder.interfaces.FilterBasedMatcher;
import ls13.productfinder.interfaces.Scriptable;
import ls13.productfinder.interfaces.Scriptable.ScriptableCompiler;


/**
 * <p>Title:  Filter-based matcher</p>
 * <p>Description:  Central class for computing matching products based on filters</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * 
 * @author DJ
 * @version 1.0
 */

public class FilterBasedMatcherImpl implements FilterBasedMatcher {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FilterBasedMatcherImpl.class);
	
	private static final boolean TRACE = false;
	
	private final ScriptableCompiler scriptableCompiler;
	
	/** product catalog */
	private final List<Product> products;
	
	/** The list of known filters */
	private final List<CompiledFilterRule> filters;
	
	/** The bitset list */
	private final Map<CompiledFilterRule, BitSet> productsForFilters;
		
	
	/**
	 * 
	 * @param scriptableCompiler
	 * @param products
	 * @param filters
	 * @throws ProductFinderException
	 */
	public FilterBasedMatcherImpl(ScriptableCompiler scriptableCompiler, List<Product> products, List<FilterRule> filters) throws ProductFinderException{
		this.products = products;
		this.scriptableCompiler = scriptableCompiler;
		this.filters = preCompileFilters(Optional.ofNullable(filters).orElse(Collections.emptyList()));
		this.productsForFilters = Collections.unmodifiableMap(initFilterResults());
	} 
	
	/**
	 * Pre-compiles all the filter conditions and filter expressions; handles to the 
	 * executable classes are stored with the filter.
	 * @param filters 
	 * @return 
	 * @throws ProductFinderException
	 */
	private List<CompiledFilterRule> preCompileFilters(List<FilterRule> filters) throws ProductFinderException {
		final List<CompiledFilterRule> list = new ArrayList<>();
		for (FilterRule fr : filters) {
			list.add(new CompiledFilterRule(fr, scriptableCompiler));
		}
		return list;
	}

	/**
	 * Inistializes the matrix of matching products per filter for those filters with no variables
	 * @throws ProductFinderException
	 */
	private Map<CompiledFilterRule, BitSet> initFilterResults() throws ProductFinderException {
		
		final Map<CompiledFilterRule, BitSet> productsForFilters = new IdentityHashMap<>();
		for (CompiledFilterRule fr : filters) {
			try  {
				// Create an empty bit-set; will be all set to zero at the beginning
				final BitSet bs = new BitSet(products.size());
				// Do not set any customer properties now
				// reset iterator
					
				int i = -1;
				for (Product product : products) {
					i++;
					// evaluating filters with user variables will fail 
					// Will be caught in outer loop, i.e., we will simply continue with the next filter
					if (productMatches(product, null, fr.getCompiledFilter())) {
						bs.set(i);
						if (TRACE) {
							LOGGER.debug("product selected by filter: {}, {}", fr.getName(), product);
						}	
					}
				}
				productsForFilters.put(fr,bs);
				
				LOGGER.debug("{} products selected by filter: {}", bs.cardinality(), fr.getName());
				
			} catch (Exception e) {
				if (TRACE) {
					LOGGER.debug("Failed to init filter: {} for products.", fr, e);
				}
				
				LOGGER.debug("skip static filter processing for: {}", fr);
			}
		}
		
		return productsForFilters;
	}

	@Override
	public List<Product> getProductsForExpression(String expression) throws ProductFinderException {
		
		final List<Product> result = new ArrayList<>();
		if (expression == null) {
			result.addAll(products);
			
		} else {
			// First compile the expression
			final Scriptable<Boolean> compiledFilter = scriptableCompiler.compile(expression, Boolean.class, false, "DynamicFilterRule");
			
			// Iterate over the products
			for (Product product : products) {
				// evaluating filters with user variables will fail 
				// Will be caught in outer loop, i.e., we will simply continue with the next filter
				if (productMatches(product, null, compiledFilter)) {
					result.add(product);
					if (TRACE) {
						LOGGER.debug("product selected by expression: {}, {}", expression, product);
					}	
				}
			}
		}
		
		LOGGER.debug("products {} selected by expression: {}", result.size(), expression);
		
		return result;
	}
	
	/**
	 * 
	 * @param product
	 * @param userInputs
	 * @param compiledFilter
	 * @return
	 * @throws ProductFinderException
	 */
	private boolean productMatches(Product product, Map<String, Object> userInputs, Scriptable<Boolean> compiledFilter) throws ProductFinderException {
		// combine user inputs and product properties
		final Map<String, Object> vars;
		if (userInputs != null) {
			// overwrite user input with product properties if there duplicate names
			vars = new LinkedHashMap<>(userInputs);
			vars.putAll(product.getProperties());
		} else {
			vars = new LinkedHashMap<>(product.getProperties());
		}
		return compiledFilter.execute(vars);
	}
		
	@Override
	public RecommendationResult computeRecommendationResult(Map<String, Object> userInputs, int relax) throws ProductFinderException {
		
		// Computes a list of applicable filters for the current user inputs
		final Set<CompiledFilterRule> applicableFilters = new LinkedHashSet<>();
		for (CompiledFilterRule fr : filters) {
			// use compiled script
			if(fr.getCompiledCondition().execute(userInputs)) {
				applicableFilters.add(fr);
				LOGGER.debug("applicabile filter: {}", fr);
			}
		}
		
		LOGGER.debug("applicabile filters: {}", applicableFilters.size());
		
		// If there are no applicable filters, return all products
		if (applicableFilters.size() == 0) {
			return new RecommendationResult(Collections.emptyList(), 
											Collections.emptyList(), 
											new ArrayList<>(products));
		}
		
		LOGGER.debug("user inputs: {}", userInputs);
		
		final Map<CompiledFilterRule, BitSet> filterBitSets = computeFilterBitsSets(userInputs, applicableFilters);
		final BitSet matching = combineBitsets(filterBitSets, null);
		
		if (relax <= 0 || matching.cardinality() >= relax) {
			return new RecommendationResult(
					toFilters(userInputs, applicableFilters), 
					Collections.emptyList(), 
					getProducts(matching)
				);
		}
		
		// relaxation has to be done
		return computeRelaxedResult1(userInputs, relax, applicableFilters, filterBitSets);
	}
	
	/**
	 * Computes a dynamic list of bitsets for the given filters
	 * 
	 * @param userInputs 
	 * @param filters the list of currently active filters
	 * @return a map mapping filters to bitsets
	 * @throws ProductFinderException
	 */
	private Map<CompiledFilterRule, BitSet> computeFilterBitsSets(Map<String, Object> userInputs, Collection<CompiledFilterRule> filters) throws ProductFinderException {

		// get the bitsets for these filters
		final Map<CompiledFilterRule, BitSet> bitsets = new LinkedHashMap<>();
		for (CompiledFilterRule fr : filters) {
			
			// Get an existing bitset
			final BitSet cachedBitset = productsForFilters.get(fr);
			if (cachedBitset != null) {
				bitsets.put(fr, (BitSet) cachedBitset.clone());
				continue;
			}
			
			// We have to compute a special bitset for the current requirements
			// Create an empty bit-set; will be all set to zero at the beginning
			final BitSet bitset = new BitSet(products.size());
			// Do not set any customer properties now
			int i = 0;
			for (Product product : products) {
				// evaluating filters with user variables will fail at the moment
				// do not worry.
				if (productMatches(product, userInputs, fr.getCompiledFilter())) {
					bitset.set(i);
					LOGGER.debug("product selected by filter: {}, {}", fr.getName(), product);
				}
				i++;
			}
			
			LOGGER.debug("{} product(s) selected by filter: {}", bitset.cardinality(), fr.getName());
			
			bitsets.put(fr, bitset);
		}
		return bitsets;
	}	
	
	/**
	 * Computes a relaxed relaxation result based on relaxation costs
	 * 
	 * @param userInputs
	 * @param relax the number of products to be retrieved (currently only one prouct is garantueed)
	 * @param applicableFilters 
	 * @param filterBitSets
	 * @return a recommendation result instance containing the relevant
	 * @throws ProductFinderException 
	 */
	private RecommendationResult computeRelaxedResult1(Map<String, Object> userInputs,
			int relax, Set<CompiledFilterRule> applicableFilters, Map<CompiledFilterRule, BitSet> filterBitSets) throws ProductFinderException {
		
		// Prepare a list of forced filters
		final Set<CompiledFilterRule> forcedFilters = filterBitSets.keySet().stream()
				.filter(CompiledFilterRule::isForced)
				.collect(Collectors.toSet());
		
		final Set<CompiledFilterRule> relaxableFilters = new LinkedHashSet<>();
		relaxableFilters.addAll(Sets.difference(filterBitSets.keySet(), forcedFilters));
		
		LOGGER.debug("starting filter relaxation. applicable-filers: {}, active-filers: {}, forced-filters: {}, relaxable-filters: {}, expected-products: {}", 
				applicableFilters.size(), filterBitSets.keySet().size(), forcedFilters.size(), relaxableFilters.size(), relax);
		
		if (!forcedFilters.isEmpty()) {
			// Check, if there remain products, if the forced filters are applied.
			final BitSet matchingForced = combineBitsets(filterBitSets, forcedFilters);
			
			if (matchingForced.cardinality() == 0 || relaxableFilters.isEmpty()) {
				// Nothing remains after application of forced filters or only forced filters are active
				// no relaxation possible -> return empty recommendation result
				return new RecommendationResult(
						toFilters(userInputs, forcedFilters), 
						toFilters(userInputs, Sets.difference(applicableFilters, forcedFilters)),
						Collections.emptyList()
					);
			}
			
			// remove all products matched by a relaxable filter which are not selected by forced filters
			final AtomicInteger updates = new AtomicInteger();
			for (int i = 0; i < products.size(); i++) {
				if (!matchingForced.get(i)) {
					// product is not selected by forced filters
					final int unforcedProduct = i;
					for (CompiledFilterRule relaxableFilter : relaxableFilters) {
						filterBitSets.computeIfPresent(relaxableFilter, (k, bitSet) -> {
							if (bitSet.get(unforcedProduct)) {
								// disable selection of unforced product
								bitSet.set(unforcedProduct, false);
								updates.incrementAndGet();
							}
							if (bitSet.cardinality() > 0) {
								return bitSet;
							}
							
							LOGGER.debug("relaxing filter: {}, contains only products not selected by forced filters.", k.getName());
							updates.incrementAndGet();
							return null;	// disable filter and remove bitset mapping
						});
					}
				}
			}
			
			if (updates.get() > 0) {
			
				LOGGER.debug("Disabled products selected by relaxable filters not matching the forced filters. active-filters: {}", filterBitSets.size());
				
				final BitSet matching = combineBitsets(filterBitSets, null);
				
				if (matching.cardinality() >= relax) {
					return new RecommendationResult(
							toFilters(userInputs, filterBitSets.keySet()), 
							toFilters(userInputs, Sets.difference(applicableFilters, filterBitSets.keySet())), 
							getProducts(matching)
						);
				}
			}
		}	
		
		// continue with filter relaxation phase 2 using recursion
		return computeRelaxedResult2(userInputs, relax, applicableFilters, filterBitSets, forcedFilters, 0);
	}
	
	/**
	 * Computes a relaxed relaxation result based on relaxation costs
	 * 
	 * @param userInputs
	 * @param relax the number of products to be retrieved (currently only one prouct is garantueed)
	 * @param applicableFilters 
	 * @param filterBitSets
	 * @param forcedFilters 
	 * @param recursionCounter 
	 * @return a recommendation result instance containing the relevant
	 * @throws ProductFinderException 
	 */
	private RecommendationResult computeRelaxedResult2(Map<String, Object> userInputs,
			int relax, Set<CompiledFilterRule> applicableFilters, Map<CompiledFilterRule, BitSet> filterBitSets, Set<CompiledFilterRule> forcedFilters, int recursionCounter) throws ProductFinderException {
		
		final Set<CompiledFilterRule> relaxableFilters = new LinkedHashSet<>();
		relaxableFilters.addAll(Sets.difference(filterBitSets.keySet(), forcedFilters));
		
		LOGGER.debug("processing filter relaxation (recursion: {}). applicable-filers: {}, active-filers: {}, forced-filters: {}, relaxable-filters: {}, expected-products: {}", 
				recursionCounter, applicableFilters.size(), filterBitSets.keySet().size(), forcedFilters.size(), relaxableFilters.size(), relax);
		
		if (filterBitSets.isEmpty()) {
			// no relaxation possible -> return all products
			LOGGER.debug("end of relaxation, removed all filters.");
			return new RecommendationResult(
					Collections.emptyList(), 
					Collections.emptyList(),
					new ArrayList<>(products)
				);
		}
		
		if (!forcedFilters.isEmpty()) {
			// Check, if there remain products, if the forced filters are applied.
			final BitSet matchingForced = combineBitsets(filterBitSets, forcedFilters);
			
			if (matchingForced.cardinality() == 0 || relaxableFilters.isEmpty()) {
				// Nothing remains after application of forced filters or only forced filters are active
				// no relaxation possible -> return empty recommendation result
				return new RecommendationResult(
						toFilters(userInputs, forcedFilters), 
						toFilters(userInputs, Sets.difference(applicableFilters, forcedFilters)),
						Collections.emptyList()
					);
			}
		}	
		
		// Find one optimal relaxation in the rest
		// Iterate over all products and determine the costs of relaxing it
		// Remember best one and subsequently get the set of products
		int minPrioritySum = 0;
		Set<CompiledFilterRule> minPriorityFilters = null;
		for (int i = 0; i < products.size(); i++) {
			// For storing the filters for one product
			final Set<CompiledFilterRule> currentFilters = new LinkedHashSet<>();
			int sumPrios = 0;
			// check if the corresponding bit is set, do not process forced filters
			for (CompiledFilterRule fr : relaxableFilters) {
				// if the bit is zero, add it to the
				final BitSet bitSet = filterBitSets.get(fr);
				if (bitSet != null && !bitSet.get(i)) {
					// add it
					currentFilters.add(fr);
					sumPrios += fr.getPriority();
				}
			}
			
			// found a new best..
			if (minPriorityFilters == null 
					|| (!currentFilters.isEmpty() && sumPrios < minPrioritySum)) {
				minPriorityFilters = currentFilters;
				minPrioritySum = sumPrios;
			}
		}
		
		final CompiledFilterRule relaxedFilter = minPriorityFilters.stream()
			.sorted(Comparator.comparingInt(CompiledFilterRule::getPriority))
			.findFirst()
			.orElse(null);
		
		if (relaxedFilter != null) {
			LOGGER.debug("removing filter: {}", relaxedFilter.getName());
			
			// Ok, now we should have the optimal relaxation - get the
			// corresponding products, by computing the set of products
			// matching the remaining filters
			filterBitSets.remove(relaxedFilter);
			
			// compute the matching products
			final BitSet matching = combineBitsets(filterBitSets, null);
			
			LOGGER.debug("relaxed filter: {}, active-filters: {}, selected-products: {}", 
					relaxedFilter.getName(), filterBitSets.keySet().size(), matching.cardinality());
			
			if (matching.cardinality() >= relax) {
				return new RecommendationResult(
						toFilters(userInputs, filterBitSets.keySet()), 
						toFilters(userInputs, Sets.difference(applicableFilters, filterBitSets.keySet())), 
						getProducts(matching)
					);
			}
		}
		
		if (applicableFilters.size() < recursionCounter) {
			LOGGER.error("Internal error during filter relaxation!!! recursion loop detected, check error information. recursion-count: {}", recursionCounter);
			LOGGER.error(" [error-information] user-input: {}", userInputs);
			LOGGER.error(" [error-information] expected-products: {}", relax);
			LOGGER.error(" [error-information] applicable-filters: {}", applicableFilters);
			LOGGER.error(" [error-information] active-filters: {}", filterBitSets.keySet());
			throw new ProductFinderException("Internal error during filter relaxation!!! recursion loop detected, check error information. recursion-count: " + recursionCounter);
		}
		
		return computeRelaxedResult2(userInputs, relax, applicableFilters, filterBitSets, forcedFilters, ++recursionCounter);
	}
	
	/**
	 * Method mapping bitset flags to products
	 * @param bitset the result bit set
	 * @return the list of products corresponding to the bitset
	 */
	private List<Product> getProducts(BitSet bitset) {
		final List<Product> result = new ArrayList<>();
		for (int i = 0; i < products.size(); i++) {
			if (bitset.get(i)) {
				result.add(products.get(i));
			}
		}
		return result;
	}
	
	/**
	 * A method that computes the conjunction of bitsets for the given filters
	 * 
	 * @param bitsets the bitsets
	 * @param filters filters to be joined or null to join all bitsets
	 * @return the joined bitset
	 */
	private BitSet combineBitsets(Map<CompiledFilterRule, BitSet> bitsets, Collection<CompiledFilterRule> filters) {
		
		final BitSet result = new BitSet(products.size());
		// initialize result with all bits set to true
		result.flip(0,result.size());
		
		if (filters != null && !filters.isEmpty()) {
			// combine only the bitsets of the given filters
			for (CompiledFilterRule fr : filters) {
				if (result.cardinality() == 0) {
					break; // all products are disabled in result
				}
				
				final BitSet bitSet = bitsets.get(fr);
				if (bitSet != null) {
					result.and(bitSet);
				}
			}
			
		} else {
			// combine all bitsets
			for (BitSet bitSet : bitsets.values()) {
				result.and(bitSet);
			}
		}
		
		LOGGER.debug("combined filters. resulting products: {}, filters: {}", result.cardinality(), Optional.ofNullable(filters).orElse(bitsets.keySet()).stream()
				.map(CompiledFilterRule::getName)
				.collect(Collectors.toList()));
		
		return result;
	}
	
	private List<Filter> toFilters(Map<String, Object> userInputs, Collection<CompiledFilterRule> filters) {
		final List<Filter> list = new ArrayList<>();
		for (CompiledFilterRule cfr : filters) {
			list.add(new Filter(cfr.getName(), 
					cfr.getPriority(), 
					userInputs, 
					cfr.getExplanationGenerator(), 
					cfr.getExcuseGenerator())
				);
		}
		return list;
	}
	
	
	
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	private static class CompiledFilterRule {

		private final FilterRule filterRule;
		private final Scriptable<Boolean> compiledCondition;
		private final Scriptable<Boolean> compiledFilter;
		private final DescriptionGenerator explanationGenerator;
		private final DescriptionGenerator excuseGenerator;
		

		public CompiledFilterRule(FilterRule filterRule, ScriptableCompiler scriptableCompiler) throws ProductFinderException {
			this.filterRule = filterRule;
			this.compiledCondition = scriptableCompiler.compile(filterRule.getCondition(), Boolean.class, false, String.format("FilterRule: %s, condition", filterRule.getName()));
			this.compiledFilter = scriptableCompiler.compile(filterRule.getFilter(), Boolean.class, false, String.format("FilterRule: %s, filter", filterRule.getName()));
			this.explanationGenerator = DescriptionFactory.createGenerator(filterRule.getExplanation(), scriptableCompiler, String.format("explanation text filter: %s", filterRule.getName()));
			this.excuseGenerator = DescriptionFactory.createGenerator(filterRule.getExcuse(), scriptableCompiler, String.format("excuse text filter: %s", filterRule.getName()));
		}

		/**
		 * @return the priority
		 */
		public int getPriority() {
			return filterRule.getPriority();
		}
		
		/**
		 * @return filter is forced or not
		 */
		public boolean isForced() {
			return filterRule.getPriority() <= 0;
		}

		/**
		 * @return filter name
		 */
		public String getName() {
			return filterRule.getName();
		}
		
		/**
		 * @return the explanationGenerator
		 */
		public DescriptionGenerator getExplanationGenerator() {
			return explanationGenerator;
		}
		
		/**
		 * @return the excuseGenerator
		 */
		public DescriptionGenerator getExcuseGenerator() {
			return excuseGenerator;
		}
		
		/**
		 * @return the compiledCondition
		 */
		public Scriptable<Boolean> getCompiledCondition() {
			return compiledCondition;
		}

		/**
		 * @return the compiledFilter
		 */
		public Scriptable<Boolean> getCompiledFilter() {
			return compiledFilter;
		}
		
		@Override
		public String toString() {
			return "CompiledFilterRule [filterRule=" + filterRule + "]";
		}
	}
}