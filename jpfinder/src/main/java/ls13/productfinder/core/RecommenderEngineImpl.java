package ls13.productfinder.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.RecommendationResult;
import ls13.productfinder.api.RecommenderEngine;
import ls13.productfinder.api.RecommenderSession;
import ls13.productfinder.api.Variable;
import ls13.productfinder.beans.KnowledgeBase;
import ls13.productfinder.beans.ProductCatalog;
import ls13.productfinder.interfaces.FilterBasedMatcher;
import ls13.productfinder.interfaces.InfoTextManager;
import ls13.productfinder.interfaces.KnowledgeBaseLoader;
import ls13.productfinder.interfaces.ProductCatalogLoader;
import ls13.productfinder.interfaces.RuleEngine;
import ls13.productfinder.interfaces.Scriptable.ScriptableCompiler;
import ls13.productfinder.interfaces.SimilarityCalculator;
import ls13.productfinder.interfaces.UtilityCalculator;


/**
 * <p>Title:  Recommender engine</p>
 * <p>Description:  Class containting engine information - product data and recommendation rules as well as handles to the 
 * filtering engine and infotext processor</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author DJ
 * @version 1.0
  */
public class RecommenderEngineImpl implements RecommenderEngine {

	private static final Logger LOGGER = LoggerFactory.getLogger(RecommenderEngineImpl.class);
	
	private final ProductCatalogLoader productCatalogLoader;
	private final KnowledgeBaseLoader knowledgeBaseLoader;
	
	/** internal engine state */
	private volatile State state;
	
	/**
	 * Creates an empty engine, remembers file locations, and initializes the filter-based matcher
	 * and the infotext engine
	 * @param kbFile filename of file where rules are defined
	 * @param productFile name of file where products are defined
	 * @throws ProductFinderException in case of parsing errors 
	 */
	public RecommenderEngineImpl(KnowledgeBaseLoader knowledgeBaseLoader, ProductCatalogLoader productCatalogLoader) throws ProductFinderException {
		this.knowledgeBaseLoader = Preconditions.checkNotNull(knowledgeBaseLoader);
		this.productCatalogLoader = Preconditions.checkNotNull(productCatalogLoader);
		
		loadModel();
	}
	
	@Override
	public RecommenderSession newRecommenderSession() {
		return new RecommenderSessionImpl(this);
	}
	
	@Override
	public synchronized void loadModel() throws ProductFinderException {
		
		LOGGER.info("reloading recommender engine");
		
		LOGGER.info("loading knowledge base using: {}", knowledgeBaseLoader);
		
		final KnowledgeBase knowledgeBase = knowledgeBaseLoader.loadKnowledgeBase();

		// Add the utility variable
		final List<Variable> allVariables = new ArrayList<>(knowledgeBase.getVariables());
		
		// init user requirements
		final List<Variable> userRequirments = Collections.unmodifiableList(allVariables.stream()
			.filter(Variable::isPreference)
			.sorted(Comparator.comparing(Variable::getName))
			.collect(Collectors.toList()));
		
		// add default properties and init product properties
		allVariables.add(UtilityCalculator.UTILITY_DOMINATOR_VAR);
		allVariables.add(SimilarityCalculator.SIMILARITY_DOMINATOR_VAR);
		
		final List<Variable> productProperties = Collections.unmodifiableList(allVariables.stream()
				.filter(e -> !e.isPreference())
				.sorted(Comparator.comparing(Variable::getName))
				.collect(Collectors.toList()));
		
		LOGGER.info("loaded knowledge base. product properties: {}, user requirements: {}", productProperties.size(), userRequirments.size());
		final Set<String> duplicateCheck = new HashSet<>();
		for (Variable variable : productProperties) {
			LOGGER.info("   product properties: {}", variable);
			duplicateCheck.add(variable.getName());
		}
		for (Variable variable : userRequirments) {
			LOGGER.info("    user requirements: {}", variable);
			if (!duplicateCheck.add(variable.getName())) {
				LOGGER.error("duplicate variable name: {}!!! Used as product property and user requirement, check knowledge base configuration.", variable.getName());
			}
		}
		
		LOGGER.info("loading product catalog using: {}", productCatalogLoader);
		
		// Put everything into the engine
        List<Product> products = productCatalogLoader.readProductData(productProperties).getProducts();
		
        LOGGER.info("Loaded product catalog. total products: {}", products.size());
        
        final ScriptableCompiler scriptableCompiler = ScriptableImpl.createCompiler(knowledgeBase.getFunctionLibrary());
        
		// Compute static utilities on startup
        final UtilityCalculator utilityCalculator = new UtilityCalculatorImpl(scriptableCompiler, knowledgeBase.getUtilityFunction());
		if (utilityCalculator.requiresStaticCalculation()) {
			products = utilityCalculator.computeStaticUtilities(products);
		}		
		
		this.state = new State(
				userRequirments, 
				products, 
				new RuleEngineImpl(scriptableCompiler, knowledgeBase.getBusinessRules(), 5), 
				new FilterBasedMatcherImpl(scriptableCompiler, products, knowledgeBase.getFilters()), 
				new InfoTextManagerImpl(scriptableCompiler, knowledgeBase.getInfoTexts()), 
				new SimilarityCalculatorImpl(scriptableCompiler, products, knowledgeBase.getSimilarityFunction()), 
				utilityCalculator,
				knowledgeBase);
	}
	
	@Override
	public List<Variable> getUserVariables() {
		return state.userVariables;
	}
	
	@Override
	public Variable getVariable(String property) {
		return state.variablesIndex.get(property);
	}
	
	@Override
	public RecommendationResult computeRecommendationResult(RecommenderSession recommenderSession, int relax) throws ProductFinderException {
			
			// copy state handle for computation of result
			// otherwise there could be synchronization errors during model reload
			final State currentState = state;
			final Map<String, Object> userInputs = recommenderSession.getUserInputs();
			
			final RecommendationResult result = currentState.filterBasedMatcher.computeRecommendationResult(userInputs, relax);
			
			// Do some sorting if this is possible
			final UtilityCalculator uc = currentState.utilityCalculator;
			if (!uc.requiresDynamicCalculation()) {
				return result;
			}
			
			// process only if dynamic calculation is required
			return new RecommendationResult(
					result.getAppliedFilters(), 
					result.getRelaxedFilters(), 
					uc.computeDynamicUtilities(result.getMatchingProducts(), userInputs));
		
	}
	
	@Override
	public List<Product> getProductsForFilterExpression(String expression) throws ProductFinderException {
		return state.filterBasedMatcher.getProductsForExpression(expression);
	}
	
	@Override
	public String getText(RecommenderSession recommenderSession, String name, Product product) throws ProductFinderException {
		return state.infoTetxtManager.getText(name, recommenderSession.getUserInputs(), product);
	}
	
	@Override
	public ProductCatalog getProductCatalog() {
		return ProductCatalog.builder()
				.setProducts(state.products)
				.build();
	}

	@Override
	public KnowledgeBase getKnowledgeBase() {
		return state.knowledgeBase;
	}
	
	@Override
	public String toString() {
		return "RecommenderEngineImpl [productCatalogLoader=" + productCatalogLoader + ", knowledgeBaseLoader="
				+ knowledgeBaseLoader + ", state=" + state + "]";
	}
	
	
	
	
	
	
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	private static final class State {
		
		private final Date loadTs = new Date();
		
		/** The handle to the session global similarity calculator */
		private final SimilarityCalculator similarityCalculator;
		
		/** A handle to the local rule engine */
		private final RuleEngine ruleEngine;
		
		/** product catalog */
		private final List<Product> products;
		
		/** user variables */
		private final List<Variable> userVariables;
		
		/** map containing user and product variables */
		private final Map<String, Variable> variablesIndex;
		
		// The filter-based matcher
		private final FilterBasedMatcher filterBasedMatcher; 
		
		// The infotext manager
		private final InfoTextManager infoTetxtManager;
		
		/** The handle to the global static utility calculator */
		private final UtilityCalculator utilityCalculator;
		
		private final KnowledgeBase knowledgeBase;
		
		/**
		 * 
		 * @param variables
		 * @param products
		 * @param ruleEngine
		 * @param filterBasedMatcher
		 * @param infoTetxtManager
		 * @param similarityCalculator
		 * @param utilityCalculator
		 * @param knowledgeBase 
		 */
		public State(List<Variable> variables, List<Product> products, RuleEngine ruleEngine,
				FilterBasedMatcher filterBasedMatcher, InfoTextManager infoTetxtManager,
				SimilarityCalculator similarityCalculator, UtilityCalculator utilityCalculator, KnowledgeBase knowledgeBase) {
			
			this.userVariables = Collections.unmodifiableList(variables.stream()
					.filter(Variable::isPreference)
					.collect(Collectors.toList())
				);
			this.variablesIndex = Maps.uniqueIndex(variables, Variable::getName);
			this.products = products;
			this.ruleEngine = ruleEngine;
			this.filterBasedMatcher = filterBasedMatcher;
			this.infoTetxtManager = infoTetxtManager;
			this.similarityCalculator = similarityCalculator;
			this.utilityCalculator = utilityCalculator;
			this.knowledgeBase = knowledgeBase;
		}

		@Override
		public String toString() {
			return "State ["
					+ "loadTs=" + loadTs + ", variables=" + userVariables + ", products=" + products + ", filterBasedMatcher="
					+ filterBasedMatcher + ", infoTetxtManager=" + infoTetxtManager + ", similarityCalculator="
					+ similarityCalculator + ", utilityCalculator=" + utilityCalculator + ", ruleEngine=" + ruleEngine
					+ "]";
		}
	}
}
