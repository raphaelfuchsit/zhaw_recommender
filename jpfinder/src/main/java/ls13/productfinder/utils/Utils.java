package ls13.productfinder.utils;

import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;


/**
 * <p>Title: Utility functions </p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * 
 * @author DJ
 * @version 1.0
 */

public class Utils {
	
	/**
	 * method to check identity of hashmaps based on values
	 * 
	 * @param a
	 *            first hashmap
	 * @param b
	 *            second hashmap
	 * @return true, if the hashmaps contain the same key value-pairs (not the same
	 *         references) todo: array checks.
	 */
	public static boolean identical (Map<String, Object> a, Map<String, Object> b) {
		
		// not the same size
		if (a.keySet().size() != b.keySet().size())
			return false;
		
		for (Entry<String, Object> aEntry : a.entrySet()) {
			final String name = aEntry.getKey();
			final Object o1 = aEntry.getValue();
			final Object o2 = b.get(name);
			
			if (o1 == null && o2 != null) return false;
			if (o1 != null && o2 == null) return false;
			
			if (o1 == null && o2 == null) continue;
			if (o1.equals(o2)) continue;
			
			if (o1.getClass() != o2.getClass()) return false;
			
			if ((o1 instanceof Integer && o2 instanceof Integer) 
				|| (o1 instanceof Double && o2 instanceof Double) 
				|| (o1 instanceof String && o2 instanceof String) 
					&& (!(o1.equals(o2)))) return false;
			
			if (o1 instanceof Object[]) {
				if (!(o2 instanceof Object[])) {
					return false;
				}
				
				final Object[] a1 = (Object[]) o1;
				final Object[] a2 = (Object[]) o2;
				if (!Arrays.equals(a1, a2)) {
					return false;
				}
			}
		}
		
		return true;
	}
}
