package ls13.productfinder.xml.loader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import ls13.productfinder.api.ProductFinderException;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class XmlFileParser {
	
	private XmlFileParser() {}

	/**
	 * Reads a JAXB annotated bean from the given xml file
	 * 
	 * @param xmlFile
	 * @param clazz
	 * @return JAXB bean
	 * @throws ProductFinderException
	 */
	public static <T> T parse(String xmlFile, Class<T> clazz) throws ProductFinderException {
		return parse(new File(xmlFile), clazz);
	}
	
	/**
	 * Reads a JAXB annotated bean from the given xml file
	 * 
	 * @param xmlFile
	 * @param clazz
	 * @return JAXB bean
	 * @throws ProductFinderException
	 */
	public static <T> T parse(File xmlFile, Class<T> clazz) throws ProductFinderException {
		try {
			return parse(new FileInputStream(xmlFile), clazz, xmlFile);
		} catch (FileNotFoundException e) {
			throw new ProductFinderException("[FATAL]: Xml file does not exists. path: %s, working directory: %s", 
					xmlFile, new File("my.file").getAbsoluteFile().getParent());
		}
	}
	
	/**
	 * Reads a JAXB annotated bean from the given xml file
	 * 
	 * @param xmlFile
	 * @param clazz
	 * @return JAXB bean
	 * @throws ProductFinderException
	 */
	public static <T> T parse(InputStream xmlIs, Class<T> clazz) throws ProductFinderException {
		return parse(xmlIs, clazz, xmlIs);
	}
	
	/**
	 * Reads a JAXB annotated bean from the given xml file
	 * 
	 * @param is
	 * @param clazz
	 * @param description
	 * @return JAXB bean
	 * @throws ProductFinderException
	 */
	private static <T> T parse(InputStream is, Class<T> clazz, Object description) throws ProductFinderException {
		try (InputStream inputStream = new BufferedInputStream(is, 32*1024)) {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			return clazz.cast(unmarshaller.unmarshal(inputStream));
			
		} catch (IOException e) {
			throw new ProductFinderException("[FATAL]: I/O-Exception when reading xml data from: %s", description, e);
		} catch (Exception e) {
			throw new ProductFinderException("[ERROR]: Error when parsing data of xml data from: %s", description, e);
		}
	}
}
