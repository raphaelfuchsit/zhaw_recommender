package ls13.productfinder.xml.loader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import ls13.productfinder.api.ProductFinderException;

/**
 * Implementation of XmlAdapter to process xml CDATA
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class XmlAdapterCDATA extends XmlAdapter<String, String> {	// TODO not used...

	private static final String CDATA_START = "<![CDATA[";
	private static final String CDATA_END = "]]>";
	
	private static final Pattern PATTERN_CDATA = Pattern.compile(
			String.format("^%s(.*)%s$", Pattern.quote(CDATA_START), Pattern.quote(CDATA_END)), 
				Pattern.MULTILINE + Pattern.DOTALL);
	
	@Override
	public String unmarshal(String v) throws Exception {
		final Matcher matcher = PATTERN_CDATA.matcher(v);
		if (!matcher.matches()) {
			throw new ProductFinderException("Cannot parse XML CDATA content. pattern: %s, content: %s", PATTERN_CDATA, v);
		}
		return matcher.group(1).trim();
	}

	@Override
	public String marshal(String v) throws Exception {
		return String.format("%s%s%s", CDATA_START, v, CDATA_END);
	}
}