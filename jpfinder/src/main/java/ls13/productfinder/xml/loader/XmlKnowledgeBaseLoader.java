package ls13.productfinder.xml.loader;

import java.io.File;

import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.beans.KnowledgeBase;
import ls13.productfinder.interfaces.KnowledgeBaseLoader;
import ls13.productfinder.xml.beans.XmlRecommendationRules;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class XmlKnowledgeBaseLoader implements KnowledgeBaseLoader {

	private final File knowledgeBaseFile;
	
	public XmlKnowledgeBaseLoader(String knowledgeBaseFile) {
		this.knowledgeBaseFile = new File(knowledgeBaseFile).getAbsoluteFile();
	}

	@Override
	public KnowledgeBase loadKnowledgeBase() throws ProductFinderException {
		return XmlFileParser.parse(knowledgeBaseFile, XmlRecommendationRules.class).toKnowledgeBase();
	}

	@Override
	public String toString() {
		return "XmlKnowledgeBaseLoader [knowledgeBaseFile=" + knowledgeBaseFile + "]";
	}
}
