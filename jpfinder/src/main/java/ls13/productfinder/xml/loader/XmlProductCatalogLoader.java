package ls13.productfinder.xml.loader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.Maps;

import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.Variable;
import ls13.productfinder.beans.ProductCatalog;
import ls13.productfinder.core.ProductParser;
import ls13.productfinder.core.ProductParser.ProductProperties;
import ls13.productfinder.interfaces.ProductCatalogLoader;
import ls13.productfinder.xml.beans.XmlProduct;
import ls13.productfinder.xml.beans.XmlProductCatalog;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class XmlProductCatalogLoader implements ProductCatalogLoader {
	
	private final File productFile;
	
	public XmlProductCatalogLoader(String productFile) {
		this.productFile = new File(productFile).getAbsoluteFile();
	}

	@Override
	public ProductCatalog readProductData(List<Variable> variables) throws ProductFinderException {
		final XmlProductCatalog pcBean = XmlFileParser.parse(productFile, XmlProductCatalog.class);
		final Map<String, Variable> variableMap = Maps.uniqueIndex(variables, Variable::getName);
		
		final List<Product> products = new ArrayList<>();
		for (XmlProduct pBean : pcBean.getProducts()) {
			products.add(ProductParser.processProperties(pBean.getAttributes().stream()
					.map(atrribute -> new ProductProperties(atrribute.getName(), atrribute.getValue()))
					.collect(Collectors.toList()), 
					variableMap));
		}
		
		return ProductCatalog.builder()
				.setProducts(products)
				.build();
	}

	@Override
	public String toString() {
		return "XmlProductCatalogLoader [productFile=" + productFile + "]";
	}
}
