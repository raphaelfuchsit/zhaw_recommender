package ls13.productfinder.xml.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ls13.productfinder.beans.BusinessRule;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "BusinessRule")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlBusinessRule {
	
	/** the name of the rule */
	@XmlAttribute
	private String name;
	/** The script code of the "if" part */
	@XmlElement
	private String codeIf;
	/** The script code of the "then" part */
	@XmlElement
	private String codeThen;
	
	public BusinessRule toBusinessRule() {
		return BusinessRule.builder()
				.setName(name)
				.setCodeIf(codeIf)
				.setCodeThen(codeThen)
				.build();
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the codeIf
	 */
	public String getCodeIf() {
		return codeIf;
	}
	/**
	 * @param codeIf the codeIf to set
	 */
	public void setCodeIf(String codeIf) {
		this.codeIf = codeIf;
	}
	/**
	 * @return the codeThen
	 */
	public String getCodeThen() {
		return codeThen;
	}
	/**
	 * @param codeThen the codeThen to set
	 */
	public void setCodeThen(String codeThen) {
		this.codeThen = codeThen;
	}
	
	@Override
	public String toString() {
		return "XmlBusinessRule [name=" + name + ", codeIf=" + codeIf + ", codeThen=" + codeThen + "]";
	}
}
