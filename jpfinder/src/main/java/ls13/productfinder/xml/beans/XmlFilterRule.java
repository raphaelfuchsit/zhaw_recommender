package ls13.productfinder.xml.beans;

import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.primitives.Ints;

import ls13.productfinder.beans.FilterRule;

/**
 * bean for XML tag <FilterRule />
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "FilterRule")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlFilterRule {

	@XmlAttribute
	private String name;
	@XmlAttribute
	private String condition;
	@XmlAttribute
	private String filter;
	@XmlAttribute(required=false)
	private String priority;
	@XmlAttribute(required=false)
	private String explanation;
	@XmlAttribute(required=false)
	private String excuse;
	
	public FilterRule toFilterRule() {
		return FilterRule.builder()
				.setName(name)
				.setCondition(condition)
				.setFilter(filter)
				.setPriority(Optional.ofNullable(priority)
						.map(Ints::tryParse)
						.orElse(0))
				.setExplanation(explanation)
				.setExcuse(excuse)
				.build();
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the condition
	 */
	public String getCondition() {
		return condition;
	}
	/**
	 * @param condition the condition to set
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}
	/**
	 * @return the filter
	 */
	public String getFilter() {
		return filter;
	}
	/**
	 * @param filter the filter to set
	 */
	public void setFilter(String filter) {
		this.filter = filter;
	}
	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}
	/**
	 * @return the explanation
	 */
	public String getExplanation() {
		return explanation;
	}
	/**
	 * @param explanation the explanation to set
	 */
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
	/**
	 * @return the excuse
	 */
	public String getExcuse() {
		return excuse;
	}
	/**
	 * @param excuse the excuse to set
	 */
	public void setExcuse(String excuse) {
		this.excuse = excuse;
	}
	
	@Override
	public String toString() {
		return "XmlFilterRule [name=" + name + ", priority=" + priority + ", condition=" + condition + ", filter=" + filter
				+ ", explanation=" + explanation + ", excuse=" + excuse + "]";
	}
}
