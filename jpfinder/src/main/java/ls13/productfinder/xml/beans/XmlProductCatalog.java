package ls13.productfinder.xml.beans;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * bean for XML tag <Products />
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "Products")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlProductCatalog {

	@XmlElement(name = "Product")
	private List<XmlProduct> products;
	
	/**
	 * @return the products
	 */
	public List<XmlProduct> getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(List<XmlProduct> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "XmlProductCatalog [products=" + products + "]";
	}	
}
