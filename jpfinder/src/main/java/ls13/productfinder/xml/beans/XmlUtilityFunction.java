package ls13.productfinder.xml.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import ls13.productfinder.beans.JsFunction;

/**
 * bean for XML tag <UtilityFunction />
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "UtilityFunction")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlUtilityFunction {

	@XmlAttribute(required = false)
	private String type;
	
	@XmlValue
	private String functionSource;

	public JsFunction toUtilityFunction() {
		return JsFunction.builder()
				.setFunctionName("utility_function")
				.setFunctionCode(functionSource)
				.setDynamicCalculation(!"static".equalsIgnoreCase(type))
				.build();
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the functionSource
	 */
	public String getFunctionSource() {
		return functionSource;
	}

	/**
	 * @param functionSource the functionSource to set
	 */
	public void setFunctionSource(String functionSource) {
		this.functionSource = functionSource;
	}

	@Override
	public String toString() {
		return "XmlUtilityFunction [type=" + type + ", functionSource=" + functionSource + "]";
	}
}
