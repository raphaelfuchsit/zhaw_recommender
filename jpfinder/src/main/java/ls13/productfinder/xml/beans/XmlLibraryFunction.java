package ls13.productfinder.xml.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import ls13.productfinder.beans.JsFunction;

/**
 * bean for XML tag <SimilarityFunction />
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "LibraryFunction")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlLibraryFunction {
	
//	@XmlJavaTypeAdapter(XmlAdapterCDATA.class)
	@XmlValue
	private String functionSource;
	
	@XmlAttribute
	private String name;

	public JsFunction toLibraryFunction() {
		return JsFunction.builder()
				.setFunctionName(name)
				.setFunctionCode(functionSource)
				.build();
	}
	
	/**
	 * @return the functionSource
	 */
	public String getFunctionSource() {
		return functionSource;
	}
	/**
	 * @param functionSource the functionSource to set
	 */
	public void setFunctionSource(String functionSource) {
		this.functionSource = functionSource;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "XmlLibraryFunction [name=" + name + ", functionSource=" + functionSource + "]";
	}
}
