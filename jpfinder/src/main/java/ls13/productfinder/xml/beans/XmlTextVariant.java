package ls13.productfinder.xml.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import ls13.productfinder.beans.TextVariant;

/**
 * bean for XML tag <TextVariant />
 * 
 * <p>
 * Copyright: Copyright (c) 2019
 * </p>
 * 
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "TextVariant")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlTextVariant {

	@XmlAttribute
	private String condition;

	@XmlAttribute
	private String text;

	public TextVariant toTextVariant() {
		return TextVariant.builder()
				.setCondition(condition)
				.setText(text)
				.build();
	}
	
	/**
	 * @return the condition
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * @param condition the condition to set
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "XmlTextVariant [condition=" + condition + ", text=" + text + "]";
	}
}
