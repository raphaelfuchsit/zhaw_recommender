package ls13.productfinder.xml.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ls13.productfinder.api.Variable;
import ls13.productfinder.beans.InfoText;
import ls13.productfinder.beans.KnowledgeBase;

/**
 * bean for XML tag <RecommendationRules />
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "RecommendationRules")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlRecommendationRules {

	@XmlElement(name = "Variable")
	private List<XmlVariable> variables;
	@XmlElement(name = "ProductProperty")
	private List<XmlProductProperty> productProperties;
	@XmlElement(name = "UserRequirement")
	private List<XmlUserRequirement> userRequirements;
	@XmlElement(name = "FilterRule")
	private List<XmlFilterRule> filterRules;
	@XmlElement(name = "InfoText", nillable = true)
	private List<XmlInfoText> infoTexts;
	@XmlElement(name = "UtilityFunction", nillable = true)
	private XmlUtilityFunction utilityFunction;
	@XmlElement(name = "BusinessRule", nillable = true)
	private List<XmlBusinessRule> businessRules;
	@XmlElement(name = "SimilarityFunction", nillable = true)
	private XmlSimilarityFunction similarityFunction;
	@XmlElement(name = "LibraryFunction", nillable = true)
	private List<XmlLibraryFunction> libaryFunctions;
	

	public KnowledgeBase toKnowledgeBase() {
		final List<Variable> allVariables = new ArrayList<>();
		if (variables != null) {
			variables.stream()
				.map(XmlVariable::toVariable)
				.forEach(allVariables::add);
		}
		if (productProperties != null) {
			productProperties.stream()
				.map(XmlProductProperty::toVariable)
				.forEach(allVariables::add);
		}
		if (userRequirements != null) {
			userRequirements.stream()
				.map(XmlUserRequirement::toVariable)
				.forEach(allVariables::add);
		}
		
		return KnowledgeBase.builder()
			.setVariables(allVariables)
			.setFilters(filterRules.stream()
					.map(XmlFilterRule::toFilterRule)
					.collect(Collectors.toList()))
			.setInfoTexts(Optional.ofNullable(infoTexts).orElse(Collections.emptyList()).stream()
					.map(XmlInfoText::toInfoText)
					.sorted(Comparator.comparing(InfoText::getOrder))
					.collect(Collectors.toList()))
			.setBusinessRules(Optional.ofNullable(businessRules).orElse(Collections.emptyList()).stream()
					.map(XmlBusinessRule::toBusinessRule)
					.collect(Collectors.toList()))
			.setUtilityFunction(Optional.ofNullable(utilityFunction)
					.map(XmlUtilityFunction::toUtilityFunction)
					.orElse(null))
			.setSimilarityFunction(Optional.ofNullable(similarityFunction)
					.map(XmlSimilarityFunction::toSimilarityFunction)
					.orElse(null))
			.setFunctionLibrary(Optional.ofNullable(libaryFunctions).orElse(Collections.emptyList()).stream()
					.map(XmlLibraryFunction::toLibraryFunction)
					.collect(Collectors.toList()))
			.build();
	}
	
	
	/**
	 * @return the variables
	 */
	public List<XmlVariable> getVariables() {
		return variables;
	}
	/**
	 * @param variables the variables to set
	 */
	public void setVariables(List<XmlVariable> variables) {
		this.variables = variables;
	}
	/**
	 * @return the filterRules
	 */
	public List<XmlFilterRule> getFilterRules() {
		return filterRules;
	}
	/**
	 * @param filterRules the filterRules to set
	 */
	public void setFilterRules(List<XmlFilterRule> filterRules) {
		this.filterRules = filterRules;
	}
	/**
	 * @return the infoTexts
	 */
	public List<XmlInfoText> getInfoTexts() {
		return infoTexts;
	}
	/**
	 * @param infoTexts the infoTexts to set
	 */
	public void setInfoTexts(List<XmlInfoText> infoTexts) {
		this.infoTexts = infoTexts;
	}
	/**
	 * @return the utilityFunction
	 */
	public XmlUtilityFunction getUtilityFunction() {
		return utilityFunction;
	}
	/**
	 * @param utilityFunction the utilityFunction to set
	 */
	public void setUtilityFunction(XmlUtilityFunction utilityFunction) {
		this.utilityFunction = utilityFunction;
	}
	/**
	 * @return the libaryFunctions
	 */
	public List<XmlLibraryFunction> getLibaryFunctions() {
		return libaryFunctions;
	}
	/**
	 * @param libaryFunctions the libaryFunctions to set
	 */
	public void setLibaryFunctions(List<XmlLibraryFunction> libaryFunctions) {
		this.libaryFunctions = libaryFunctions;
	}
	/**
	 * @return the productProperties
	 */
	public List<XmlProductProperty> getProductProperties() {
		return productProperties;
	}
	/**
	 * @param productProperties the productProperties to set
	 */
	public void setProductProperties(List<XmlProductProperty> productProperties) {
		this.productProperties = productProperties;
	}
	/**
	 * @return the userRequirements
	 */
	public List<XmlUserRequirement> getUserRequirements() {
		return userRequirements;
	}
	/**
	 * @param userRequirements the userRequirements to set
	 */
	public void setUserRequirements(List<XmlUserRequirement> userRequirements) {
		this.userRequirements = userRequirements;
	}
	/**
	 * @return the businessRules
	 */
	public List<XmlBusinessRule> getBusinessRules() {
		return businessRules;
	}
	/**
	 * @param businessRules the businessRules to set
	 */
	public void setBusinessRules(List<XmlBusinessRule> businessRules) {
		this.businessRules = businessRules;
	}
	/**
	 * @return the similarityFunction
	 */
	public XmlSimilarityFunction getSimilarityFunction() {
		return similarityFunction;
	}
	/**
	 * @param similarityFunction the similarityFunction to set
	 */
	public void setSimilarityFunction(XmlSimilarityFunction similarityFunction) {
		this.similarityFunction = similarityFunction;
	}
}
