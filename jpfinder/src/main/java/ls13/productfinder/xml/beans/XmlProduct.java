package ls13.productfinder.xml.beans;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * bean for XML tag <Product />
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "Product")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlProduct {
	
	@XmlElement(name = "Attribute")
	private List<XmlProductAtrribute> attributes;

	/**
	 * @return the attributes
	 */
	public List<XmlProductAtrribute> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(List<XmlProductAtrribute> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "XmlProduct [attributes=" + attributes + "]";
	}
}
