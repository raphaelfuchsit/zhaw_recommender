package ls13.productfinder.xml.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import ls13.productfinder.beans.JsFunction;

/**
 * bean for XML tag <SimilarityFunction />
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "SimilarityFunction")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlSimilarityFunction {
	
	@XmlValue
	private String functionSource;

	public JsFunction toSimilarityFunction() {
		return JsFunction.builder()
				.setFunctionName("similarity_function")
				.setFunctionCode(functionSource)
				.build();
	}
	
	/**
	 * @return the functionSource
	 */
	public String getFunctionSource() {
		return functionSource;
	}

	/**
	 * @param functionSource the functionSource to set
	 */
	public void setFunctionSource(String functionSource) {
		this.functionSource = functionSource;
	}

	@Override
	public String toString() {
		return "XmlSimilarityFunction [functionSource=" + functionSource + "]";
	}
}
