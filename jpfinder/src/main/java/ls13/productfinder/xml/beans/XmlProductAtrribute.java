package ls13.productfinder.xml.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * bean for XML tag <Attribute />
 * 
 * <p>
 * Copyright: Copyright (c) 2019
 * </p>
 * 
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "Attribute")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlProductAtrribute {

	@XmlAttribute
	private String name;
	@XmlAttribute
	private String value;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "XmlProductAtrribute [name=" + name + ", value=" + value + "]";
	}
}
