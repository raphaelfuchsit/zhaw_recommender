package ls13.productfinder.xml.beans;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.primitives.Ints;

import ls13.productfinder.beans.InfoText;

/**
 * bean for XML tag <InfoText />
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@XmlRootElement(name = "InfoText")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlInfoText {

	@XmlAttribute
	private String name;
	@XmlAttribute
	private String defaulttext;
	@XmlAttribute(required=false)
	private String order;
	
	@XmlElement(name = "TextVariant", nillable = true)
	private List<XmlTextVariant> textVariants;

	public InfoText toInfoText() {
		return InfoText.builder()
				.setName(name)
				.setOrder(Optional.ofNullable(order)
						.map(Ints::tryParse)
						.orElse(0))
				.setDefaultText(defaulttext)
				.setTextVariants(Optional.ofNullable(textVariants).orElse(Collections.emptyList()).stream()
						.map(XmlTextVariant::toTextVariant)
						.collect(Collectors.toList()))
				.build();
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the defaulttext
	 */
	public String getDefaulttext() {
		return defaulttext;
	}

	/**
	 * @param defaulttext the defaulttext to set
	 */
	public void setDefaulttext(String defaulttext) {
		this.defaulttext = defaulttext;
	}

	/**
	 * @return the textVariants
	 */
	public List<XmlTextVariant> getTextVariants() {
		return textVariants;
	}

	/**
	 * @param textVariants the textVariants to set
	 */
	public void setTextVariants(List<XmlTextVariant> textVariants) {
		this.textVariants = textVariants;
	}
	
	/**
	 * @return the order
	 */
	public String getOrder() {
		return order;
	}
	
	/**
	 * @param order the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "XmlInfoText [name=" + name 
				+ ", order=" + order 
				+ ", defaulttext=" + defaulttext
				+ ", textVariants=" + textVariants + "]";
	}
}
