package ls13.productfinder.xml.beans;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ls13.productfinder.api.Variable;
import ls13.productfinder.api.Variable.VarType;

/**
 * bean for XML tag <Variable />
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 * @deprecated will be removed, use {@link XmlProductProperty} or {@link XmlUserRequirement}
 */
@Deprecated
@XmlRootElement(name = "Variable")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlVariable {

	@XmlAttribute
	private String name;
	@XmlAttribute
	private String type;
	/** marks variable as customer preference (requirement) or product property */
	@XmlAttribute(required = false)
	private Boolean preference;
	@XmlElement(name = "Option", nillable = true)
	private List<String> options;
	
	public Variable toVariable() {
		return Variable.builder()
		.setName(name)
		.setType(VarType.forName(type))
		.setPreference(preference != null && preference)
//		.setOptions(null)	// TODO var processing...
		.build();
		
		
//		String contents = this.contents.toString();
//		//System.out.println("value found: " + contents);
//		String type = (String) variables.get(this.currentVarname);
//		//System.out.println("current var and type is " + this.currentVarname + " " +  type);
//		if (type.toLowerCase().startsWith("string")) { 
//			this.currentVariableDomain.add(new String(contents));
//		}
//		else if (type.toLowerCase().startsWith("double")) {
//			try {
//				this.currentVariableDomain.add(Double.parseDouble(contents));
//			} catch (Exception e) {
//				this.parsingErrors.add(new ProductFinderException(String.format("Domain value: %s for variable: %s cannot be converted to Double.", 
//						contents,  this.currentVarname), e));
//			}
//		}
		
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the options
	 */
	public List<String> getOptions() {
		return options;
	}
	/**
	 * @param options the options to set
	 */
	public void setOptions(List<String> options) {
		this.options = options;
	}
	/**
	 * @return the preference
	 */
	public Boolean getPreference() {
		return preference;
	}
	/**
	 * @param preference the preference to set
	 */
	public void setPreference(Boolean preference) {
		this.preference = preference;
	}
		
	@Override
	public String toString() {
		return "XmlVariable [name=" + name 
				+ ", type=" + type 
				+ ", preference=" + preference
				+ ", options=" + options + "]";
	}
}
