package ls13.productfinder.beans;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

/**
 * <p>Title: Filter rule handle </p>
 * <p>Description:  A class representing a filter rule</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * 
 * @author DJ
 * @version 1.0
 */
@AutoValue
public abstract class FilterRule {
	
	public abstract String getName();
	/** condition, when filter should be applied */
	public abstract String getCondition();
	/** filter condition to be checked on products */
	public abstract String getFilter();
	/** priority of filter */
	public abstract int getPriority();
	/** user-readable text when filter was successfully applied */
	@Nullable public abstract String getExplanation();
	/** the excuse for non-applied filters */
	@Nullable public abstract String getExcuse();
	
	public static Builder builder() {
		return new AutoValue_FilterRule.Builder();
	}
	
	@AutoValue.Builder
	public abstract static class Builder {
		public abstract Builder setName(String value);
		public abstract Builder setCondition(String value);
		public abstract Builder setFilter(String value);
		public abstract Builder setPriority(int value);
		public abstract Builder setExplanation(String value);
		public abstract Builder setExcuse(String value);
		public abstract FilterRule build();
	}
}
