package ls13.productfinder.beans;

import java.util.Collections;
import java.util.List;

import com.google.auto.value.AutoValue;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@AutoValue
public abstract class InfoText {

	public abstract String getName();
	public abstract String getDefaultText();
	public abstract List<TextVariant> getTextVariants();
	/** info text order */
	public abstract int getOrder();
	
	public static Builder builder() {
		return new AutoValue_InfoText.Builder()
				.setOrder(0)
				.setTextVariants(Collections.emptyList());
	}
	
	@AutoValue.Builder
	public abstract static class Builder {
		public abstract Builder setName(String value);
		public abstract Builder setOrder(int value);
		public abstract Builder setDefaultText(String value);
		public abstract Builder setTextVariants(List<TextVariant> value);
		public abstract InfoText build();
	}
}
