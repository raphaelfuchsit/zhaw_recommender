package ls13.productfinder.beans;

import com.google.auto.value.AutoValue;

import ls13.productfinder.interfaces.Scriptable;
import ls13.productfinder.interfaces.SimilarityCalculator;
import ls13.productfinder.interfaces.UtilityCalculator;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@AutoValue
public abstract class JsFunction {
	
	private Type type; 
	
	/**
	 * @return function type
	 */
	public Type getType() {
		return type;
	}
	
	public abstract String getFunctionName();
	public abstract boolean isDynamicCalculation();
	public abstract String getFunctionCode();
	
	public static Builder builder() {
		return new AutoValue_JsFunction.Builder()
				.setDynamicCalculation(false);
	}
	
	@AutoValue.Builder
	public abstract static class Builder {
		public abstract Builder setFunctionName(String value);
		public abstract Builder setDynamicCalculation(boolean value);
		public abstract Builder setFunctionCode(String value);
		abstract JsFunction autoBuild();
		
		public JsFunction build() {
			JsFunction jsFunction = autoBuild();
			jsFunction.type = Type.forName(jsFunction.getFunctionName());
			return jsFunction;
		}
	}
	
	/**
	 * 
	 * @author streuch1
	 *
	 */
	public static enum Type {
		/** used in {@link UtilityCalculator} */
		UTILITY_FUNCTION,
		/** used in {@link SimilarityCalculator} */
		SIMILARITY_FUNCTION,
		/** library function: global java script function, see {@link Scriptable} */
		LIBRARY_FUNCTION;
		
		/**
		 * 
		 * @param name
		 * @return function type
		 */
		public static final Type forName(String name) {
			if ("UTILITY_FUNCTION".equalsIgnoreCase(name)
					|| "UTILITY".equalsIgnoreCase(name)) {
				return UTILITY_FUNCTION;
			}
			if ("SIMILARITY_FUNCTION".equalsIgnoreCase(name)
					|| "SIMILARITY".equalsIgnoreCase(name)) {
				return SIMILARITY_FUNCTION;
			}
			return Type.LIBRARY_FUNCTION;
		}
	}
}
