package ls13.productfinder.beans;

import java.util.List;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

import ls13.productfinder.api.Variable;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@AutoValue
public abstract class KnowledgeBase {

	/** list of product properties */
	public abstract List<Variable> getVariables();
	/** list of filter rules */
	@Nullable
	public abstract List<FilterRule> getFilters();
	@Nullable
	public abstract List<InfoText> getInfoTexts();
	/** Business rules: List of Business rule objects */
	@Nullable
	public abstract List<BusinessRule> getBusinessRules();
	@Nullable
	public abstract JsFunction getUtilityFunction();
	@Nullable
	public abstract JsFunction getSimilarityFunction();
	@Nullable
	public abstract List<JsFunction> getFunctionLibrary();
	
	public static Builder builder() {
		return new AutoValue_KnowledgeBase.Builder();
	}
	
	@AutoValue.Builder
	public abstract static class Builder {
		public abstract Builder setVariables(List<Variable> value);
		public abstract Builder setFilters(List<FilterRule> value);
		public abstract Builder setInfoTexts(List<InfoText> value);
		public abstract Builder setBusinessRules(List<BusinessRule> value);
		public abstract Builder setUtilityFunction(JsFunction value);
		public abstract Builder setSimilarityFunction(JsFunction value);
		public abstract Builder setFunctionLibrary(List<JsFunction> value);
		public abstract KnowledgeBase build();
	}
}
