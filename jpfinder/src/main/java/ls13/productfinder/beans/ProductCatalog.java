package ls13.productfinder.beans;

import java.util.List;

import com.google.auto.value.AutoValue;

import ls13.productfinder.api.Product;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@AutoValue
public abstract class ProductCatalog {
	
	public abstract List<Product> getProducts();
	
	public static Builder builder() {
		return new AutoValue_ProductCatalog.Builder();
	}
	
	@AutoValue.Builder
	public abstract static class Builder {
		public abstract Builder setProducts(List<Product> value);
		public abstract ProductCatalog build();
	}
}
