package ls13.productfinder.beans;

import com.google.auto.value.AutoValue;

/**
 * A simple data structure for holding business rule data
 * @author dietmar
 *
 */
@AutoValue
public abstract class BusinessRule {
	
	public abstract String getName();
	/** The script code of the "if" part */
	public abstract String getCodeIf();
	/** The script code of the "then" part */
	public abstract String getCodeThen();
	
	public static Builder builder() {
		return new AutoValue_BusinessRule.Builder();
	}
	
	@AutoValue.Builder
	public abstract static class Builder {
		public abstract Builder setName(String value);
		public abstract Builder setCodeIf(String value);
		public abstract Builder setCodeThen(String value);
		public abstract BusinessRule build();
	}
}
