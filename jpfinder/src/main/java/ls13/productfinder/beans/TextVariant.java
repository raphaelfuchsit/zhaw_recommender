package ls13.productfinder.beans;

import com.google.auto.value.AutoValue;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@AutoValue
public abstract class TextVariant {
	
	public abstract String getCondition();
	public abstract String getText();
	
	public static Builder builder() {
		return new AutoValue_TextVariant.Builder();
	}
	
	@AutoValue.Builder
	public abstract static class Builder {
		public abstract Builder setCondition(String value);
		public abstract Builder setText(String value);
		public abstract TextVariant build();
	}
}
