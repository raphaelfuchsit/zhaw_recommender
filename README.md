# Digital Online Recommender

## Front-End Documentation
The Front-End is built with Vue.js and is a Single Page Application (SPA). 
It allows to navigate throught various forms and tables, where REST-Services are called, 
those get data and trigger the recommender engine.

To get more information about starting the application locally, go to the following page:

[Front-End Guide](dor-webapp/README.md)

## Back-End Documentation
The Back-End is built with Spring Boot and creates a Recommender Engine for the calculations and exposes an
API to exchange data and trigger the engine.
It also persists data within a Postgres Database.

[Back-End Guide](dor-backend/README.md)