CREATE SCHEMA RE_ENGINE;

CREATE TABLE RE_ENGINE.CAS (
	ID BIGINT PRIMARY KEY, 
	NAME VARCHAR(255) NOT NULL UNIQUE,
	DURATION INT,
	COSTS FLOAT,
	ECTS_COUNT INT,
	LANGUAGE VARCHAR(255),
	CAMPUS VARCHAR(255),
	START VARCHAR(255),
	DESCRIPTION VARCHAR(4095),
	PROMO_PRIORITY INTEGER,
    URL_WEB CHARACTER VARYING(2023),
    URL_ICON CHARACTER VARYING(2023)
	-- TODO add lable, e.g. BAN
);

INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (1, 'Business Analysis and Methods', 1, 8000.0, 12, 'Deutsch', 'Campus der ZHAW School of Management and Law, Winterthur', 'HS', 'Dieser CAS befasst sich mit der Planung und Durchführung von klassischen und agilen Business-Analysis-Projekten. Sie erhalten einen vertieften Einblick in die Unternehmens- und Anforderungsanalyse sowie Lösungsgenerierung. Bestehende Geschäftsmodelle zu analysieren und die richtigen Veränderungen mit den relevanten Anspruchsgruppen (digital) umzusetzen, ist der Kern dieses CAS. Durch die praktische Anwendung der Methoden an Fallbeispielen haben Sie die Chance, Ihre Fähigkeiten zu festigen.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-business-analysis-and-methods', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;
INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (2, 'Prozessdigitalisierung', 1, 8000.0, 12, 'Deutsch', 'Campus der ZHAW School of Management and Law, Winterthur', 'auf Anfrage', 'Prozesse sind die Brücke zwischen der strategischen Ausrichtung eines Unternehmens und der operativen Ausführung der Tätigkeiten. Prozessmanagement umfasst dabei alle Tätigkeiten, die sich mit der Analyse, Gestaltung, Optimierung, Automatisierung und Überwachung von Prozessen beschäftigt. Passend zum CAS-Titel „Prozessdigitalisierung“ legen wir dabei den Schwerpunkt auf die IT-Unterstützung des Prozessmanagements, insbesondere im Bereich der Automatisierung.  Wir erheben nicht nur den Anspruch, Ihnen eine theoretische Fundierung zu bieten, sondern die Wirkung der Konzepte durch den Einsatz entsprechender Werkzeuge praktisch erfahrbar zu machen. So nutzen Sie im Kurs zum Beispiel den Prozessmodellierungsstandard BPMN 2.0, die Robotic Process Automation-Lösung UiPath oder das ERP-System Odoo.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-prozessdigitalisierung', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;
INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (3, 'Business Modeling und Transformation', 1, 8000.0, 12, 'Deutsch', 'Campus der ZHAW School of Management and Law, Winterthur', 'HS/FS', 'Im CAS erlangen Sie die Kompetenzen, Geschäftsmodelle zu entwickeln, zu analysieren und zu optimieren. Sie erhalten die Möglichkeit, grundlegende Skills wie Business Model Pitching zu trainieren und setzen sich mit Business Transformation Management auseinander.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-business-modeling-and-transformation', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;
INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (4, 'Solution Design', 1, 8200.0, 12, 'Deutsch, Englisch', 'Campus der ZHAW School of Management and Law, Winterthur', 'auf Anfrage', 'Sie erlernen den nutzerzentrierten Problemlöseansatz des Design Thinking und wenden diesen auf eine aktuelle Problemstellung aus der Praxis an. Zudem eignen Sie sich die Prozesse und Methoden des Innovationsmanagements an.  ', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-service-design-innovation', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;
INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (5, 'Requirements Engineering', 1, 8000.0, 12, 'Deutsch', 'Campus der ZHAW School of Management and Law, Winterthur', 'FS', 'Dieser CAS beschäftigt sich mit den Methoden der Anforderungsgewinnung und –modellierung sowie Modellen für Software-Entwürfe und Software-Architektur. Zudem erhalten Sie die Möglichkeit, sich für die internationale Zertifizierung "Certified Professional for Requirements Engineering" (CPRE) vorzubereiten.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-requirements-engineering', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;
INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (6, 'Strategisches IT- und TechManagement', 1, 8000.0, 12, 'Deutsch, Englisch', 'Campus der ZHAW School of Management and Law, Winterthur', 'FS', 'Durch die steigende Bedeutung von Kundenzentriertheit, Digitalisierung und dem richtigen Einsatz von disruptiven Technologien geraten heutige IT-Organisationen zunehmend unter Druck, sich als proaktiver und business-driven Innovationspartner zu positionieren.  Um eine enge Verzahnung der Geschäftsziele mit jenen der IT zu erreichen, benötigt es ein strategisches IT-Management und moderne IT-Governance-Prinzipien. Darüber hinaus hilft eine adaptive, mit der Geschäftsstrategie abgestimmte Digital & IT-Strategie die Daten, Prozesse und Technologien eines Unternehmens nahtlos miteinander zu verbinden.  Nur eine durchgehende Abstimmung der Geschäftsarchitektur und der Wertschöpfungsketten mit den dafür notwendigen IT-Fähigkeiten, -Prozessen und -Applikationen ermöglicht eine klare Nachvollziehbarkeit der IT-Leistung und des damit messbaren IT-Wertbeitrages (IT-Value).  In diesem CAS erfahren Sie, wie eine best-in-class IT der Zukunft und die damit verbundenen strategischen Wirkungsmöglichkeiten Ihrer IT-Organisation zum Erfolg verhelfen können.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-strategisches-it-und-techmanagement', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;
INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (7, 'IT-Leadership und innovative Organisationen', 1, 8000.0, 12, 'Deutsch, Englisch', 'Campus der ZHAW School of Management and Law, Winterthur', 'FS', 'Nur ein umfassendes Verständnis der neuen IT-Führungsmodelle und der zukünftigen IT-Arbeitsweisen kann den Erfolg in der IT-Organisation weiter gewährleisten. Der CAS «IT-Leadership und innovative Organisationen» vermittelt Ihnen einen fundierten Einblick in personen- und teambezogene Handlungs- und Gestaltungsmöglichkeiten. Sie erlernen zudem wesentliche Grundlagen und Prinzipien einer strategischen und operativen Personalführung von IT-Organisationen.  Darüber hinaus werden die wichtigsten Komponenten der intelligenten Führung, Steuerung und Befähigung der IT-Organisation betrachtet. Neben essentiellen Themenblöcken wie Selbstorganisation, persönliche Handlungs- und Entwicklungsmöglichkeiten und Selbstmotivation im IT-Umfeld werden auch Schwerpunkte im Führen und Begeistern von IT-Mitarbeitern gesetzt.  Zusätzlich werden massgebliche Sichtweisen der technologiegetriebenen und virtuellen IT-Teams aufgezeigt. Es werden deren Herausforderungen thematisiert und andauernde Veränderungen sowie agile Organisationsstrukturen durchleuchtet.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-it-leadership-und-innovative-organisationen', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;
INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (8, 'Agiles IT-Projektmanagement', 1, 8000.0, 12, 'Deutsch, Englisch', 'Campus der ZHAW School of Management and Law, Winterthur', 'HS', 'Die Mehrzahl der IT-Entwicklungen wird heute nach agilen Vorgehensmodellen durchgeführt. Der CAS Agiles IT-Projektmanagement befähigt Sie, dem agilen Mindset entsprechend zu handeln und agile Techniken und Vorgehensweisen in IT-Umgebungen anzuwenden – an der Schnittstelle zum agile Requirements Engineering und ausgehend aus der Unternehmens- und IT-Strategie.  Ziel ist, dass Sie Ihre Arbeitstechniken weiterentwickeln und das Gelernte auf Teamebene (Scrum) sowie auf Gesamtorganisationsebene (SAFe) selbst umsetzen.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-agiles-it-projektmanagement', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;
INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (9, 'Psychologie in der Arbeitswelt 4.0', 1, 6950.0, 12, 'Deutsch', 'IAP Institut für Angewandte Psychologie und Grossraum Zürich', 'HS/FS', 'Der rasche technologische Wandel transformiert derzeit die Arbeitswelt. Gleichzeitig wandeln sich Berufsbilder, Geschäfts-, Kollaborations- und Führungsmodelle. Es entstehen durch Digitalisierung zunehmend Möglichkeiten, mobil und flexibel zu arbeiten. Jedoch steigen auch die Anforderungen an einzelne Mitarbeitende, sich selbst zu organisieren. Dabei werden neue Kompetenzen gefragt und digitale Kompetenzen stärker gefordert.  Der CAS Psychologie in der neuen Arbeitswelt stellt den Menschen ins Zentrum des digitalen Wandels. Er bietet eine Auseinandersetzung mit verschiedenen Dimensionen der veränderten Arbeitswelt.', null, 'https://www.zhaw.ch/de/psychologie/weiterbildung/detail/kurs/cas-psychologie-in-der-arbeitswelt-4-0', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;
INSERT INTO cas (id, name, duration, costs, ects_count, language, campus, start, description, promo_priority, url_web, url_icon) VALUES (10, 'Innovation & Leadership', 1, 8000.0, 12, 'Deutsch', 'Campus der ZHAW School of Management and Law, Winterthur', 'FS', 'Viele Unternehmen sehen sich heute mit grossem Innovations- und Wandeldruck konfrontiert. Insbesondere KMU können ihre Marktposition meist nur durch gezielte Geschäftsinnovation nachhaltig weiter entwickeln und erfolgreich ausbauen. Eine grosse Herausforderung besteht dabei darin, erfolgreiche Geschäftsmodelle nicht nur zu entwickeln sondern diese auch mit angepasster Führung effektiv umsetzen zu können.  Immer mehr sind deshalb Führungskräfte gefragt, welche sowohl die konzeptionelle und strategische Entwicklung neuer Geschäftsmodelle beherrschen als auch deren zielgerichtete Umsetzung durch innovationsgerichtete Führung beherrschen. Welches Geschäftsmodell ist am erfolgversprechendsten? Wie wirken sich Steuerungslogiken und Unternehmenskultur auf die Umsetzung des Geschäftsmodelles aus? Wie können Führung und Organisation optimal auf das neue Modell abgestimmt werden?  Dieser Lehrgang vermittelt einerseits die relevanten Instrumente, Methoden und Strategien der Geschäftsmodellentwicklung. Andererseits lernen Sie, wie Sie als Führungskraft die internen Aktivitäten und Anreizsysteme optimal und erfolgsversprechend darauf abstimmen können.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-innovation-leadership', 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg');;


CREATE TABLE RE_ENGINE.PROFESSION (
	ID BIGINT PRIMARY KEY, 
	NAME VARCHAR(255) NOT NULL UNIQUE,
	DESCRIPTION VARCHAR(4095)
);

INSERT INTO profession (id, name, description) VALUES (1, 'Business Analyse', null);
INSERT INTO profession (id, name, description) VALUES (2, 'IT-Leadership', null);
INSERT INTO profession (id, name, description) VALUES (3, 'Projektmanagement', null);
INSERT INTO profession (id, name, description) VALUES (4, 'Innovationsmanagement', null);
INSERT INTO profession (id, name, description) VALUES (5, 'Unternehmensentwicklung', null);
INSERT INTO profession (id, name, description) VALUES (6, 'Requirements-Engineering', null);
INSERT INTO profession (id, name, description) VALUES (7, 'Solution Design', null);
INSERT INTO profession (id, name, description) VALUES (8, 'Software Engineering', null);
INSERT INTO profession (id, name, description) VALUES (9, 'Agile Rollen (Scrum Master, Product Owner)', null);


CREATE TABLE RE_ENGINE.CAS_TO_PROFESSION (
	CAS_ID BIGINT NOT NULL,
	PROFESSION_ID BIGINT NOT NULL,
	PRIMARY KEY (CAS_ID, PROFESSION_ID)
);

INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (1, 3);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (1, 1);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (1, 2);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 1);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 2);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 3);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 4);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 5);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 6);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 7);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 8);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 9);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (2, 10);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (3, 4);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (3, 5);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (4, 3);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (4, 1);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (4, 6);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (4, 7);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (4, 8);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 1);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 2);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 3);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 4);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 5);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 6);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 7);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 8);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 9);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (5, 10);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (6, 2);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (7, 2);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (8, 3);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (8, 9);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 1);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 2);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 3);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 4);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 5);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 6);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 7);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 8);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 9);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (9, 10);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (10, 3);
INSERT INTO cas_to_profession (cas_id, profession_id) VALUES (10, 2);

CREATE TABLE RE_ENGINE.INTEREST (
	ID BIGINT PRIMARY KEY, 
	NAME VARCHAR(255) NOT NULL UNIQUE,
	TEXT VARCHAR(4095),
	DESCRIPTION VARCHAR(4095)
);

INSERT INTO interest (id, name, text, description) VALUES (1, 'Anforderungsanalyse', 'Analysieren Sie gerne Anforderungen?', 'Anforderungsanalyse');
INSERT INTO interest (id, name, text, description) VALUES (2, 'Projektmanagement', 'Haben Sie Interesse an der Leitung von Projekten?', 'Projektmanagement');
INSERT INTO interest (id, name, text, description) VALUES (3, 'Bewertung von Lösungen', 'Ist Ihnen die Analyse und Bewertung von bestehenden Lösungen wichtig?', 'Bewertung von Lösungen');
INSERT INTO interest (id, name, text, description) VALUES (4, 'Prozessmodellierung', 'Modellieren Sie gerne Prozesse?', 'Prozessmodellierung');
INSERT INTO interest (id, name, text, description) VALUES (5, 'Prozessanalyse', 'Analysieren Sie gerne Vorgehenweisen in Ihrem Unternehmen?', 'Prozessanalyse');
INSERT INTO interest (id, name, text, description) VALUES (6, 'Prozessoptimierung', 'Ist Ihnen die Verbesserung von bestehenden Prozessen ein Anliegen?', 'Prozessoptimierung');
INSERT INTO interest (id, name, text, description) VALUES (7, 'Strategisches Prozessmanagement', 'Sind für Sie eine Unternehmensstrategie und dazugehörige Prozesse wichtig?', 'Strategisches Prozessmanagement');
INSERT INTO interest (id, name, text, description) VALUES (8, 'Operatives Prozessmanagement', 'Ist Ihnen das Umsetzen und Verbessern von Prozessen ein Anliegen?', 'Operatives Prozessmanagement');
INSERT INTO interest (id, name, text, description) VALUES (9, 'Unternehmensanalyse', 'Ist Ihnen die Verbesserung des Unternehmens wichtig?', 'Unternehmensanalyse');
INSERT INTO interest (id, name, text, description) VALUES (10, 'Unternehmensentwicklung', 'Wollen Sie Ihr bestehendes oder zukünftiges Unternehmen weiterentwickeln?', 'Unternehmensentwicklung');
INSERT INTO interest (id, name, text, description) VALUES (11, 'Unternehmenstransformation', 'Ist Ihnen die Transformation zu einem modernen Unternehmen ein Anliegen?', 'Unternehmenstransformation');
INSERT INTO interest (id, name, text, description) VALUES (12, 'Unternehmensführung', 'Sehen Sie sich im Unternehmens-Management?', 'Unternehmensführung');
INSERT INTO interest (id, name, text, description) VALUES (13, 'Innovation', 'Sind für Sie Innovationen wichtig und interessant?', 'Innovation');
INSERT INTO interest (id, name, text, description) VALUES (14, 'Lösungskonzeption', 'Lösen Sie gerne komplexe Problemstellungen?', 'Lösungskonzeption');
INSERT INTO interest (id, name, text, description) VALUES (15, 'Lösungsaufbau', 'Wollen Sie Lösungen auf einer soliden Grundlage aufbauen?', 'Lösungsaufbau');
INSERT INTO interest (id, name, text, description) VALUES (16, 'Lösungsbeschaffung', 'Ist das Suchen nach einer Lösung für Sie spannend?', 'Lösungsbeschaffung');
INSERT INTO interest (id, name, text, description) VALUES (17, 'IT-Architektur', 'Haben Sie Interesse an solid aufgebauten IT-Architekturen?', 'IT-Architektur');
INSERT INTO interest (id, name, text, description) VALUES (18, 'Software-Architektur', 'Ist Ihnen eine saubere Software-Architektur wichtig?', 'Software-Architektur');
INSERT INTO interest (id, name, text, description) VALUES (19, 'Selbstmanagement', 'Ist Ihnen Ihre eigene Arbeitsweise ein Anliegen?', 'Selbstmanagement');
INSERT INTO interest (id, name, text, description) VALUES (20, 'IT-Organisationsformen', 'Lernen Sie gerne neue Informationen über IT-Organisationsformen?', 'IT-Organisationsformen');
INSERT INTO interest (id, name, text, description) VALUES (21, 'IT-Governance', 'Sind Ihnen rechtliche Aspekte und Richtlinien wichtig?', 'IT-Governance');
INSERT INTO interest (id, name, text, description) VALUES (22, 'IT-Strategie', 'Haben Sie das Bedürfnis einer IT mit einem klaren Ziel und einer klaren Strategie?', 'IT-Strategie');
INSERT INTO interest (id, name, text, description) VALUES (23, 'Arbeitswelt 4.0', 'Sind Ihnen Arbeitsweisen und wie man diese verbessern kann, wichtig?', 'Arbeitswelt 4.0');
INSERT INTO interest (id, name, text, description) VALUES (24, 'Psychologie in der Arbeitswelt', 'Haben Sie Interesse an psychologischen Aspekten in der Arbeitswelt?', 'Psychologie in der Arbeitswelt');
INSERT INTO interest (id, name, text, description) VALUES (25, 'People Leadership', 'Führen Sie gerne andere Mitarbeiter und helfen ihnen auf ihrem Berufsweg?', 'People Leadership');
INSERT INTO interest (id, name, text, description) VALUES (26, 'Agile Vorgehensweisen', 'Sind Sie interessiert an agilen Vorgehensweisen?', 'Agile Vorgehensweisen');
INSERT INTO interest (id, name, text, description) VALUES (27, 'Projektmanagement-Tools', 'Wie gut finden Sie einen sauberen Einsatz von Projektma-nagement-Tools?', 'Projektmanagement-Tools');


CREATE TABLE RE_ENGINE.CAS_TO_INTEREST (
	CAS_ID BIGINT NOT NULL,
	INTEREST_ID BIGINT NOT NULL,
	PRIMARY KEY (CAS_ID, INTEREST_ID)
);

INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (1, 9);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (1, 1);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (1, 2);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (1, 14);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (1, 3);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (1, 4);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (2, 4);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (2, 5);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (2, 6);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (2, 7);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (2, 8);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (3, 9);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (3, 10);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (3, 11);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (3, 13);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (4, 14);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (4, 15);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (4, 16);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (4, 17);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (5, 1);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (5, 18);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (6, 9);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (6, 10);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (6, 11);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (6, 13);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (6, 17);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (6, 21);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (6, 22);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (7, 19);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (7, 12);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (7, 20);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (8, 26);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (8, 2);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (8, 27);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (9, 23);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (9, 24);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (9, 25);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (10, 13);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (10, 10);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (10, 11);
INSERT INTO cas_to_interest (cas_id, interest_id) VALUES (10, 12);


CREATE TABLE RE_ENGINE.MAS (
	ID BIGINT PRIMARY KEY, 
	NAME VARCHAR(255) NOT NULL UNIQUE,
	DURATION INT,
	COSTS FLOAT,
	TOTAL_ECTS INT,
	CAS_COUNT INT,
	MASTER_THESIS INT,
	DESCRIPTION VARCHAR(4095),
	PROMO_PRIORITY INTEGER,
    URL_WEB CHARACTER VARYING(2023),
    URL_ICON CHARACTER VARYING(2023),
    START VARCHAR(255)
);

INSERT INTO mas (id, name, duration, costs, total_ects, cas_count, master_thesis, description, promo_priority, url_web, url_icon, start) VALUES (1, 'Business Engineering (bisher: Business Analysis)', 5, 31000.0, 60, 4, 1, 'Seit mehr als einem Jahrzehnt vermittelt der MAS Business Engineering (bisher: Business Analysis) des Instituts für Wirtschaftsinformatik praxisorientiertes Know-how zur digitalen Transformation von Unternehmen. Das Spektrum reicht dabei von der Gestaltung digitaler Strategien über die Prozessdigitalisierung bis zum agilen Entwurf von Informationssystemen. Sie lernen, wie Sie Digitalisierungsprojekte leiten und die Potenziale von Daten nutzen können.  Wir vermitteln Ihnen die Kompetenzen und Methoden, die Sie brauchen, um einen entscheidenden Beitrag zum unternehmerischen Erfolg leisten zu können.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/mas-business-engineering', 'https://www.zhaw.ch/storage/_processed_/5/2/csm_produktbild-mas-business-analysis_8bbddbfd16.jpg', 'HS/FS');;
INSERT INTO mas (id, name, duration, costs, total_ects, cas_count, master_thesis, description, promo_priority, url_web, url_icon, start) VALUES (2, 'IT-Leadership und TechManagement (ITLTM)', 5, 31000.0, 60, 4, 1, '«Beyond hypes, back to reality» – nicht Buzzwords sondern zeitgerechte IT Führungs- und Managementansätze sowie begeisterte Kunden machen den Erfolg der Zukunft aus! Liegen Ihre Herausforderungen in der Digitalen Transformation, im Wunsch einer erhöhten Agilität, in der Erarbeitung kundenzentrierter und intelligenter Lösungen, oder wünschen Sie sich mehr Effizienz, Sicherheit und Wirtschaftlichkeit in der IT?

Mit dem MAS IT-Leadership und TechManagement bringen Sie sich selbst und die IT-Organisation auf Erfolgskurs. Dafür bieten wir Ihnen einen einzigartigen und praxisorientierten Weiterbildungsstudiengang, der Sie befähigt, den Nutzen der IT zu maximieren und Ihre Karriere zu beschleunigen.

Der MAS IT-Leadership und TechManagement ersetzt den ehemaligen MAS Wirtschaftsinformatik.', null, 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/mas-it-leadership-und-techmanagement', 'https://www.zhaw.ch/storage/_processed_/1/6/csm_sml_wb_mas_it_leadership_produktbild_9a2d7b5b7d.jpg', 'HS/FS');;


CREATE TABLE RE_ENGINE.CAS_BUNDLE (
	ID BIGINT PRIMARY KEY,
	MAS_ID BIGINT NOT NULL, 
	NAME VARCHAR(255) NOT NULL,
	MIN_CAS INT,
	MAX_CAS INT,
	DESCRIPTION VARCHAR(4095),
	mandatory_bundle INT,
	UNIQUE (MAS_ID, NAME)
);

INSERT INTO cas_bundle (id, mas_id, name, max_cas, description, mandatory_bundle, min_cas) VALUES (3, 1, 'Pfilcht CAS', 1, null, 1, 1);
INSERT INTO cas_bundle (id, mas_id, name, max_cas, description, mandatory_bundle, min_cas) VALUES (5, 2, 'Pfilcht CAS', 2, null, 1, 2);
INSERT INTO cas_bundle (id, mas_id, name, max_cas, description, mandatory_bundle, min_cas) VALUES (1, 1, 'Wahlpflicht B', 2, null, 0, 0);
INSERT INTO cas_bundle (id, mas_id, name, max_cas, description, mandatory_bundle, min_cas) VALUES (2, 1, 'Wahlpflicht A', 2, null, 0, 1);
INSERT INTO cas_bundle (id, mas_id, name, max_cas, description, mandatory_bundle, min_cas) VALUES (4, 2, 'Wahlpflicht', 2, null, 0, 2);


CREATE TABLE RE_ENGINE.CAS_TO_BUNDLE (
	CAS_ID BIGINT NOT NULL,
	BUNDLE_ID BIGINT NOT NULL,
	PRIMARY KEY (CAS_ID, BUNDLE_ID)
);

INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (4, 1);
INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (5, 1);
INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (2, 2);
INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (3, 2);
INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (1, 3);
INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (8, 4);
INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (9, 4);
INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (10, 4);
INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (7, 5);
INSERT INTO cas_to_bundle (cas_id, bundle_id) VALUES (6, 5);


CREATE TABLE RE_ENGINE.CONFIG_RULES (
	ID BIGINT PRIMARY KEY, 
	RULES_XML TEXT NOT NULL
);

INSERT INTO config_rules (id, rules_xml) VALUES (7, '<?xml version="1.0" encoding="utf-8"?>
<RecommendationRules xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<!-- PRODUCT PROPERTIES -->
<ProductProperty name="p_type" type="String"/> <!-- product type: [''CAS'',''MAS''] -->
<ProductProperty name="p_id" type="long"/> <!-- product id -->
<ProductProperty name="p_name" type="String"/> <!-- product name -->
<ProductProperty name="p_promo_priority" type="int"/> <!-- products promotion priority, active when value > 0 -->
<ProductProperty name="p_req_professions" type="String[]"/> <!-- For p_type = ''MAS'', contains only professions of mandatory CASs. For p_type = ''CAS'', all entries are listed. -->
<ProductProperty name="p_req_interests" type="String[]"/> <!-- For p_type = ''MAS'', contains only interests of mandatory CASs. For p_type = ''CAS'', all entries are listed. -->
<ProductProperty name="p_opt_professions" type="String[]"/> <!-- only set for p_type = ''MAS'', contains professions of optional CASs -->
<ProductProperty name="p_opt_interests" type="String[]"/> <!-- only set for p_type = ''MAS'', contains interests of optional CASs -->

<!-- USER REQUIREMENTS -->
<UserRequirement name="c_pref_type" type="String"/>
<UserRequirement name="c_pref_ids" type="long[]"/>
<UserRequirement name="c_pref_profession" type="String"/>
<!-- users profession rating: [''not interested (-1)'', ''no preference (0)'', ''very interested (1)''] -->
<UserRequirement name="c_pref_profession_rating" type="int"/>  
<UserRequirement name="c_pref_interests_liked" type="Map[String,Integer]"/> <!-- only set when user rated interests positive, meaning [''ziemlich (1)'', ''sehr (2)''] -->
<UserRequirement name="c_pref_interests_disliked" type="Map[String,Integer]"/> <!-- only set when user rated interests negative, meaning [''wenig (-1)'', ''gar nicht (-2)''] -->

<!-- c_pref_profession_experience contains experience in current job:
	   -1 - not available
	    3 - up to 3 years
	    7 - up to 7 years
	   12 - up to 12 years
	   99 - more than 12 years -->
<UserRequirement name="c_pref_profession_experience" type="int"/> 
<UserRequirement name="c_pref_gender" type="String"/>

<!-- FILTERING RULES -->

<!-- INTERNAL PRODUCT FILTERS -->
<!-- note: all internal filters must be marked as forced using priority = -1 to disabled relaxation -->
<FilterRule 
	name="f001_internal_product_type"
	condition="c_pref_type != null" 
	filter="c_pref_type == p_type"
	priority="-1"
	explanation="Interner Filter für Product Typ: ''MAS'' oder ''CAS''"
/>
<FilterRule 
	name="f002_internal_product_ids"
	condition="c_pref_ids != null" 
	filter="containsNumber(p_id, c_pref_ids)"
	priority="-1"
	explanation="Internal Filter für Product IDs"
/>

<!-- PROFESSION FILTERS -->
<FilterRule 
	name="f100_req_profession"
	condition="c_pref_profession != null &amp;&amp; c_pref_profession_rating > 0" 
	filter="containsString(c_pref_profession, p_req_professions)"
	priority="100"
	explanation="if (c_pref_type != ''MAS'') { '''' } else { ''Dieser MAS entspricht Ihrem aktuellen Berufsfeld: '' + c_pref_profession + '' und ermöglicht Ihnen den nächsten Karriereschritt.'' }"
	excuse=""
/>
<FilterRule 
	name="f101_req_profession_not"
	condition="c_pref_profession != null &amp;&amp; c_pref_profession_rating &lt; 0" 
	filter="!containsString(c_pref_profession, p_req_professions)"
	priority="30"
	explanation="if (c_pref_type != ''MAS'') { '''' } else { ''Sie sind auf der Suche nach Veränderung. Dieser MAS ermöglicht Ihnen eine Karriere in einem neuen Berufsfeld.'' }"
	excuse=""
/>
<FilterRule 
	name="f102_opt_professions"
	condition="c_pref_profession != null &amp;&amp; c_pref_profession_rating > 0 &amp;&amp; c_pref_type == ''MAS''" 
	filter="containsString(c_pref_profession, p_opt_professions)"
	priority="70"
	explanation="if (c_pref_type != ''MAS'') { '''' } else { ''Dieser MAS entspricht Ihrem aktuellen Berufsfeld: '' + c_pref_profession + '' und ermöglicht Ihnen den nächsten Karriereschritt.'' }"
	excuse=""
/>
<FilterRule 
	name="f103_opt_professions_not"
	condition="c_pref_profession != null &amp;&amp; c_pref_profession_rating &lt; 0 &amp;&amp; c_pref_type == ''MAS''" 
	filter="!containsString(c_pref_profession, p_opt_professions)"
	priority="10"
	explanation="if (c_pref_type != ''MAS'') { '''' } else { ''Sie sind auf der Suche nach Veränderung. Dieser MAS ermöglicht Ihnen eine Karriere in einem neuen Berufsfeld.'' }"
	excuse=""
/>

<!-- INTEREST FILTERS -->
<FilterRule 
	name="f200_req_interests_liked"
	condition="c_pref_interests_liked != null" 
	filter="calculateScore(c_pref_interests_liked, p_req_interests) > 0"
	priority="90"
	explanation="if (c_pref_type != ''CAS'') { '''' } else { ''Dieser '' + c_pref_type + '' passt gut zu Ihren Interessen: '' + printKeysIn(c_pref_interests_liked, p_req_interests) }"
	excuse=""
/>
<FilterRule 
	name="f201_req_interests_disliked"
	condition="c_pref_interests_disliked != null" 
	filter="calculateScore(c_pref_interests_disliked, p_req_interests) == 0"
	priority="20"
	explanation="Diese Weiterbildung beinhaltet keine Interessen die Sie nicht mögen."
	excuse=""
/>
<FilterRule 
	name="f202_opt_interests_liked"
	condition="c_pref_interests_liked != null &amp;&amp; c_pref_type == ''MAS''" 
	filter="calculateScore(c_pref_interests_liked, p_opt_interests) > 0"
	priority="60"
	explanation="if (c_pref_type != ''CAS'') { '''' } else { ''Dieser '' + c_pref_type + '' passt gut zu Ihren Interessen: '' + printKeysIn(c_pref_interests_liked, p_opt_interests) }"
	excuse=""
/>
<FilterRule 
	name="f203_opt_interests_disliked"
	condition="c_pref_interests_disliked != null &amp;&amp; c_pref_type == ''MAS''" 
	filter="calculateScore(c_pref_interests_disliked, p_opt_interests) == 0"
	priority="1"
	explanation="Diese Weiterbildung beinhaltet keine Interessen die Sie nicht mögen."
	excuse=""
/>

<!-- PERSONALIZED INFORMATION TEXTS -->

<InfoText name="profession_experience" order="1" defaulttext="">
	<TextVariant condition="c_pref_profession_experience == null" text="Sie haben keine Angaben zu Ihrer beruflichen Erfahrung gemacht, unabhängig davon wird Ihnen eine Weiterbildung an der ZHAW neue Kenntnisse vermitteln und wichtige Vorteile generieren." />
	<TextVariant condition="c_pref_profession_experience &lt;= 3" text="Möchten Sie Ihrer jungen Karriere einen Boost geben? Dieser MAS vermittelt Ihnen neue Grundlagen und öffnet neue Türen für Sie." />
	<TextVariant condition="c_pref_profession_experience &lt;= 7" text="Sie konnten sich bereits in Ihrem Beruf beweisen und etablieren. Sind Sie bereit für neue spannende Aufgaben? Dieser MAS bereitet Sie mit neuen Theorien und Konzepten auf diese vor."/>
	<TextVariant condition="c_pref_profession_experience > 7" text="Sie verfügen über eine langjährige Berufserfahrungen, sind Sie dennoch offen für neues? Mit diesem MAS haben Sie die Möglichkeit, ihr Wissen wieder aufzufrischen und neue Konzepte zu erlernen."/>
</InfoText>

<InfoText name="interests_description" order="2" defaulttext="Sie haben leider keine Angaben bezüglich Ihrer Interessen gemacht. Trotzdem wurde für Sie ein bedeutsamer MAS mit wissenswerten CAS ausgewählt.">
	<TextVariant condition="c_pref_interests_liked != null" text="''Ihre Vorliebe für folgende Interessen: '' + printKeys(c_pref_interests_liked) + '' wurde erkannt.''" />
	<TextVariant condition="c_pref_interests_disliked != null" text="''Bei der Auswahl wird berücksichtigt, dass Sie sich für folgenden Themen: '' + printKeys(c_pref_interests_disliked) + '' nicht interessieren.''" />
</InfoText>

<!-- FUNCTION DEFINITIONS -->
<!-- user print("my debug message"); to log messages from a javascript function -->

<!-- UTILITY LIBRARY -->
<UtilityFunction type="dynamic">
<![CDATA[
	function __utility() {
	
		var interests_score = 50;
		if (c_pref_interests_liked != null) {
			interests_score += 20 * calculateScore(c_pref_interests_liked, p_req_interests);
			interests_score += 10 * calculateScore(c_pref_interests_liked, p_opt_interests);
		}
	    if (c_pref_interests_disliked != null) {
			if (calculateScore(c_pref_interests_disliked, p_req_interests) == 0 
					&& calculateScore(c_pref_interests_disliked, p_opt_interests) == 0) {
				interests_score += 20;
			} else {
				interests_score += -20;
			}
		}
		
		var score = 0;
		if (c_pref_type == ''MAS'') {
			var profession_score = 50;
			if (c_pref_profession != null) {
				if (containsString(c_pref_profession, p_req_professions)) {
					profession_score += 50 * c_pref_profession_rating;
				} else if (c_pref_profession_rating > 0 && containsString(c_pref_profession, p_opt_professions)) {
					profession_score += 50 * c_pref_profession_rating;
				} else {
					profession_score += -50 * c_pref_profession_rating;
				}
			}
			
			if (profession_score == 50) {
				score = interests_score;
			} else if (interests_score == 50) {
				score = profession_score;
			} else {
				score = (2 * profession_score + interests_score) / 3;
			}
		
		} else {
			score = interests_score;
		}
	    
	    if (score > 100) {
	    	return 100;
	    }
	    if (score < 0) {
	    	return 0;
	    }
	    return score;
	}
]]>
</UtilityFunction>


<!-- FUNCTION LIBRARY -->

<LibraryFunction name="printKeys" >
<![CDATA[
	function printKeys(map) {
		if (map == null) { return ''''; }
		var s = '''';
		var i = 0;
		var l = map.keySet().size();
		for each (var pKey in map.keySet()) {
			i++;
			if (i == l) {
				s += '' und '';
			} else if (i > 1) {
		  		s += '', '';
		  	}
			s += pKey;
		}
		return s;
	}
]]>
</LibraryFunction>

<LibraryFunction name="printKeysIn" >
<![CDATA[
	function printKeysIn(map,pArray) {
		if (map == null || pArray == null) {return '''';}
		var s = '''';
		var i = 0;
		for each (var pKey in map.keySet()) {
		  if (pKey != null) {
		  		for (var j=0;j<pArray.length;j++) {
		  			if (pKey.equalsIgnoreCase(pArray[j])) {
		  			    if (i > 0) {
		  			    	s += '', '';
		  			    }
					   	s += pKey;
					   	i++;
					   	break;
					}
				}
		  }
		}
		return s;
	}
]]>
</LibraryFunction>

<LibraryFunction name="containsString" >
<![CDATA[
	function containsString(s,sArray) {
		if (s == null || sArray == null) {return false;}
		for (var i=0;i<sArray.length;i++) {
			if (s.equalsIgnoreCase(sArray[i])) {return true;}
		}
		return false;
	}
]]>
</LibraryFunction>

<LibraryFunction name="containsNumber" >
<![CDATA[
	function containsNumber(s,sArray) {
		if (s == null || sArray == null) {return false;}
		for (var i=0;i<sArray.length;i++) {
			if (s == sArray[i]) {return true;}
		}
		return false;
	}
]]>
</LibraryFunction>

<LibraryFunction name="calculateScore" >
<![CDATA[
	function calculateScore(pMap, pArray) {
		if (pMap == null || pArray == null) {return 0;}
		var score = 0;
		for each (var pKey in pMap.keySet()) {
		  var pRating = pMap.get(pKey);
		  if (pRating != null && pKey != null) {
		  		for (var j=0;j<pArray.length;j++) {
		  			if (pKey.equalsIgnoreCase(pArray[j])) {
					   	score += pRating;
						break;
					}
				}
		  }
		}
		return score;
	}
]]>
</LibraryFunction>

</RecommendationRules>');


CREATE VIEW PRODUCT_CATALOG_VIEW AS
SELECT p_type, id p_id, name p_name, min(promo_priority) p_promo_priority, min(req_professions) p_req_professions, min(opt_professions) p_opt_professions, min(req_interests) p_req_interests, min(opt_interests) p_opt_interests, min(req_mas_ids) p_req_mas_ids from ( 

-- prepare the CAS property professions
SELECT 'CAS' p_type, c.id, c.name, min(case when c.promo_priority is null then 0 else c.promo_priority end) promo_priority, 
        string_agg(p.name, '#') req_professions, 
        null opt_professions,
        null req_interests, 
        null opt_interests,
        null req_mas_ids
   FROM RE_ENGINE.CAS c
           inner join RE_ENGINE.CAS_TO_PROFESSION cp
           on c.id = cp.cas_id
           inner join RE_ENGINE.PROFESSION p 
           on cp.profession_id = p.id
group by c.id, c.name

union all

-- prepare the CAS property interests
SELECT 'CAS' p_type, c.id, c.name, null promo_priority, 
        null req_professions, 
        null opt_professions, 
        string_agg(i.name, '#') req_interests, 
        null opt_interests,
        null req_mas_ids
   FROM RE_ENGINE.CAS c
           inner join RE_ENGINE.CAS_TO_INTEREST ci
           on c.id = ci.cas_id
           inner join RE_ENGINE.INTEREST i
           on ci.interest_id = i.id
group by c.id, c.name

union all

-- prepare the CAS property mandatory MAS IDs
SELECT 'CAS' p_type, c.id, c.name, null promo_priority, 
        null req_professions, 
        null opt_professions, 
        null req_interests, 
        null opt_interests,
        string_agg('' || cb.mas_id, '#') req_mas_ids
   FROM RE_ENGINE.CAS c
           inner join RE_ENGINE.CAS_TO_BUNDLE ccb
           on c.id = ccb.cas_id
           inner join RE_ENGINE.CAS_BUNDLE cb
           on ccb.bundle_id = cb.id and cb.mandatory_bundle > 0
group by c.id, c.name

union all

-- prepare the MAS property professions
SELECT 'MAS' p_type, id, name, min(promo_priority) promo_priority, 
        string_agg(case when mandatory = 1 then profession else null end, '#') req_professions, 
        string_agg(case when mandatory = 0 then profession else null end, '#') opt_professions, 
        null req_interests, 
        null opt_interests,
        null req_mas_ids
   FROM (
        SELECT m.id, m.name, p.name profession, 
                max(case when cb.mandatory_bundle > 0 then 1 else 0 end) mandatory, 
                min(case when m.promo_priority is null then 0 else m.promo_priority end) promo_priority 
             FROM RE_ENGINE.MAS m
                   inner join RE_ENGINE.CAS_BUNDLE cb
                   on m.id = cb.mas_id
                   inner join RE_ENGINE.CAS_TO_BUNDLE ccb
                   on cb.id = ccb.bundle_id
                   inner join RE_ENGINE.CAS_TO_PROFESSION cp
                   on ccb.cas_id = cp.cas_id
                   inner join RE_ENGINE.PROFESSION p 
                   on cp.profession_id = p.id
        group by m.id, m.name, p.name
) m1
group by id, name

union all

-- prepare the MAS property interests
SELECT 'MAS' p_type, id, name, null promo_priority, 
        null req_professions, 
        null opt_professions,
        string_agg(case when mandatory = 1 then interest else null end, '#') req_interests, 
        string_agg(case when mandatory = 0 then interest else null end, '#') opt_interests,
        null req_mas_ids
   FROM (
        SELECT m.id, m.name, i.name interest, max(case when cb.mandatory_bundle > 0 then 1 else 0 end) mandatory FROM RE_ENGINE.MAS m
           inner join RE_ENGINE.CAS_BUNDLE cb
           on m.id = cb.mas_id
           inner join RE_ENGINE.CAS_TO_BUNDLE ccb
           on cb.id = ccb.bundle_id
           inner join RE_ENGINE.CAS_TO_INTEREST ci
           on ccb.cas_id = ci.cas_id
           inner join RE_ENGINE.INTEREST i
           on ci.interest_id = i.id
        group by m.id, m.name, i.name
) m2
group by id, name

) t
group by p_type, id, name
order by p_type desc, id
