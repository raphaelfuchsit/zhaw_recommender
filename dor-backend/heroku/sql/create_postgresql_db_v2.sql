CREATE SCHEMA RE_ENGINE;

CREATE TABLE RE_ENGINE.MAS (
	ID BIGINT PRIMARY KEY,
	NAME VARCHAR(255) NOT NULL UNIQUE,
	DURATION INT,
	COSTS FLOAT,
	TOTAL_ECTS INT,
	CAS_COUNT INT,
	MASTER_THESIS INT,
	DESCRIPTION VARCHAR(4095),
	PROMO_PRIORITY INTEGER,
    URL_WEB CHARACTER VARYING(2023),
    URL_ICON CHARACTER VARYING(2023),
    START VARCHAR(255)
);
INSERT INTO RE_ENGINE.MAS (ID,NAME,DURATION,COSTS,TOTAL_ECTS,CAS_COUNT,MASTER_THESIS,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON,START) VALUES (1 , 'Business Engineering (bisher: Business Analysis)' , 5 , 31000 , 60 , 4 , 1 , 'Seit mehr als einem Jahrzehnt vermittelt der MAS Business Engineering (bisher: Business Analysis) des Instituts für Wirtschaftsinformatik praxisorientiertes Know-how zur digitalen Transformation von Unternehmen. Das Spektrum reicht dabei von der Gestaltung digitaler Strategien über die Prozessdigitalisierung bis zum agilen Entwurf von Informationssystemen. Sie lernen, wie Sie Digitalisierungsprojekte leiten und die Potenziale von Daten nutzen können.  Wir vermitteln Ihnen die Kompetenzen und Methoden, die Sie brauchen, um einen entscheidenden Beitrag zum unternehmerischen Erfolg leisten zu können.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/mas-business-engineering' , 'https://www.zhaw.ch/storage/_processed_/5/2/csm_produktbild-mas-business-analysis_8bbddbfd16.jpg' , 'HS/FS' );
INSERT INTO RE_ENGINE.MAS (ID,NAME,DURATION,COSTS,TOTAL_ECTS,CAS_COUNT,MASTER_THESIS,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON,START) VALUES (2 , 'IT-Leadership und TechManagement (ITLTM)' , 5 , 31000 , 60 , 4 , 1 , '«Beyond hypes, back to reality» – nicht Buzzwords sondern zeitgerechte IT Führungs- und Managementansätze sowie begeisterte Kunden machen den Erfolg der Zukunft aus! Liegen Ihre Herausforderungen in der Digitalen Transformation, im Wunsch einer erhöhten Agilität, in der Erarbeitung kundenzentrierter und intelligenter Lösungen, oder wünschen Sie sich mehr Effizienz, Sicherheit und Wirtschaftlichkeit in der IT?

Mit dem MAS IT-Leadership und TechManagement bringen Sie sich selbst und die IT-Organisation auf Erfolgskurs. Dafür bieten wir Ihnen einen einzigartigen und praxisorientierten Weiterbildungsstudiengang, der Sie befähigt, den Nutzen der IT zu maximieren und Ihre Karriere zu beschleunigen.

Der MAS IT-Leadership und TechManagement ersetzt den ehemaligen MAS Wirtschaftsinformatik.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/mas-it-leadership-und-techmanagement' , 'https://www.zhaw.ch/storage/_processed_/1/6/csm_sml_wb_mas_it_leadership_produktbild_9a2d7b5b7d.jpg' , 'HS/FS' );
INSERT INTO RE_ENGINE.MAS (ID,NAME,DURATION,COSTS,TOTAL_ECTS,CAS_COUNT,MASTER_THESIS,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON,START) VALUES (3 , 'Digitale Transformation' , 2.5 , 31000 , 60 , 4 , 1 , 'Die Digitalisierung eröffnet ungeahnte Möglichkeiten für innovative Unternehmen. Wer jedoch an ausgedienten Geschäftsmodellen festhält, wird rasch überholt. Bereiten Sie sich auf aktuelle und zukünftige Herausforderungen vor. Neue, digitale Geschäftsmodelle, Prozesse und Kundenbeziehungen sowie angepasste Führungssysteme sind der Schlüssel zu erfolgreicher Innovation und Transformation.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/mas-digitale-transformation/' , 'https://www.zhaw.ch/typo3temp/assets/_processed_/b/e/csm_sml_mas_digitale-transformation_rollenbild_af461b223e.jpg' , '' );


CREATE TABLE RE_ENGINE.CAS (
	ID BIGINT PRIMARY KEY,
	NAME VARCHAR(255) NOT NULL UNIQUE,
	DURATION INT,
	COSTS FLOAT,
	ECTS_COUNT INT,
	LANGUAGE VARCHAR(255),
	CAMPUS VARCHAR(255),
	START VARCHAR(255),
	DESCRIPTION VARCHAR(4095),
	PROMO_PRIORITY INTEGER,
    URL_WEB CHARACTER VARYING(2023),
    URL_ICON CHARACTER VARYING(2023)
	-- TODO add lable, e.g. BAN
);
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (1 , 'Business Analysis and Methods' , 1 , 8000 , 12 , 'Deutsch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'HS' , 'Dieser CAS befasst sich mit der Planung und Durchführung von klassischen und agilen Business-Analysis-Projekten. Sie erhalten einen vertieften Einblick in die Unternehmens- und Anforderungsanalyse sowie Lösungsgenerierung. Bestehende Geschäftsmodelle zu analysieren und die richtigen Veränderungen mit den relevanten Anspruchsgruppen (digital) umzusetzen, ist der Kern dieses CAS. Durch die praktische Anwendung der Methoden an Fallbeispielen haben Sie die Chance, Ihre Fähigkeiten zu festigen.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-business-analysis-and-methods' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (2 , 'Prozessdigitalisierung' , 1 , 8000 , 12 , 'Deutsch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'auf Anfrage' , 'Prozesse sind die Brücke zwischen der strategischen Ausrichtung eines Unternehmens und der operativen Ausführung der Tätigkeiten. Prozessmanagement umfasst dabei alle Tätigkeiten, die sich mit der Analyse, Gestaltung, Optimierung, Automatisierung und Überwachung von Prozessen beschäftigt. Passend zum CAS-Titel „Prozessdigitalisierung“ legen wir dabei den Schwerpunkt auf die IT-Unterstützung des Prozessmanagements, insbesondere im Bereich der Automatisierung.  Wir erheben nicht nur den Anspruch, Ihnen eine theoretische Fundierung zu bieten, sondern die Wirkung der Konzepte durch den Einsatz entsprechender Werkzeuge praktisch erfahrbar zu machen. So nutzen Sie im Kurs zum Beispiel den Prozessmodellierungsstandard BPMN 2.0, die Robotic Process Automation-Lösung UiPath oder das ERP-System Odoo.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-prozessdigitalisierung' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (3 , 'Business Modeling und Transformation' , 1 , 8000 , 12 , 'Deutsch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'HS/FS' , 'Im CAS erlangen Sie die Kompetenzen, Geschäftsmodelle zu entwickeln, zu analysieren und zu optimieren. Sie erhalten die Möglichkeit, grundlegende Skills wie Business Model Pitching zu trainieren und setzen sich mit Business Transformation Management auseinander.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-business-modeling-and-transformation' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (4 , 'Solution Design' , 1 , 8200 , 12 , 'Deutsch, Englisch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'auf Anfrage' , 'Sie erlernen den nutzerzentrierten Problemlöseansatz des Design Thinking und wenden diesen auf eine aktuelle Problemstellung aus der Praxis an. Zudem eignen Sie sich die Prozesse und Methoden des Innovationsmanagements an.  ' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-service-design-innovation' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (5 , 'Requirements Engineering' , 1 , 8000 , 12 , 'Deutsch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS' , 'Dieser CAS beschäftigt sich mit den Methoden der Anforderungsgewinnung und –modellierung sowie Modellen für Software-Entwürfe und Software-Architektur. Zudem erhalten Sie die Möglichkeit, sich für die internationale Zertifizierung Certified Professional for Requirements Engineering (CPRE) vorzubereiten.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-requirements-engineering' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (6 , 'Strategisches IT- und TechManagement' , 1 , 8000 , 12 , 'Deutsch, Englisch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS' , 'Durch die steigende Bedeutung von Kundenzentriertheit, Digitalisierung und dem richtigen Einsatz von disruptiven Technologien geraten heutige IT-Organisationen zunehmend unter Druck, sich als proaktiver und business-driven Innovationspartner zu positionieren.  Um eine enge Verzahnung der Geschäftsziele mit jenen der IT zu erreichen, benötigt es ein strategisches IT-Management und moderne IT-Governance-Prinzipien. Darüber hinaus hilft eine adaptive, mit der Geschäftsstrategie abgestimmte Digital & IT-Strategie die Daten, Prozesse und Technologien eines Unternehmens nahtlos miteinander zu verbinden.  Nur eine durchgehende Abstimmung der Geschäftsarchitektur und der Wertschöpfungsketten mit den dafür notwendigen IT-Fähigkeiten, -Prozessen und -Applikationen ermöglicht eine klare Nachvollziehbarkeit der IT-Leistung und des damit messbaren IT-Wertbeitrages (IT-Value).  In diesem CAS erfahren Sie, wie eine best-in-class IT der Zukunft und die damit verbundenen strategischen Wirkungsmöglichkeiten Ihrer IT-Organisation zum Erfolg verhelfen können.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-strategisches-it-und-techmanagement' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (7 , 'IT-Leadership und innovative Organisationen' , 1 , 8000 , 12 , 'Deutsch, Englisch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS' , 'Nur ein umfassendes Verständnis der neuen IT-Führungsmodelle und der zukünftigen IT-Arbeitsweisen kann den Erfolg in der IT-Organisation weiter gewährleisten. Der CAS «IT-Leadership und innovative Organisationen» vermittelt Ihnen einen fundierten Einblick in personen- und teambezogene Handlungs- und Gestaltungsmöglichkeiten. Sie erlernen zudem wesentliche Grundlagen und Prinzipien einer strategischen und operativen Personalführung von IT-Organisationen.  Darüber hinaus werden die wichtigsten Komponenten der intelligenten Führung, Steuerung und Befähigung der IT-Organisation betrachtet. Neben essentiellen Themenblöcken wie Selbstorganisation, persönliche Handlungs- und Entwicklungsmöglichkeiten und Selbstmotivation im IT-Umfeld werden auch Schwerpunkte im Führen und Begeistern von IT-Mitarbeitern gesetzt.  Zusätzlich werden massgebliche Sichtweisen der technologiegetriebenen und virtuellen IT-Teams aufgezeigt. Es werden deren Herausforderungen thematisiert und andauernde Veränderungen sowie agile Organisationsstrukturen durchleuchtet.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-it-leadership-und-innovative-organisationen' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (8 , 'Agiles IT-Projektmanagement' , 1 , 8000 , 12 , 'Deutsch, Englisch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'HS' , 'Die Mehrzahl der IT-Entwicklungen wird heute nach agilen Vorgehensmodellen durchgeführt. Der CAS Agiles IT-Projektmanagement befähigt Sie, dem agilen Mindset entsprechend zu handeln und agile Techniken und Vorgehensweisen in IT-Umgebungen anzuwenden – an der Schnittstelle zum agile Requirements Engineering und ausgehend aus der Unternehmens- und IT-Strategie.  Ziel ist, dass Sie Ihre Arbeitstechniken weiterentwickeln und das Gelernte auf Teamebene (Scrum) sowie auf Gesamtorganisationsebene (SAFe) selbst umsetzen.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-agiles-it-projektmanagement' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (9 , 'Psychologie in der Arbeitswelt 4.0' , 1 , 6950 , 12 , 'Deutsch' , 'IAP Institut für Angewandte Psychologie und Grossraum Zürich' , 'HS/FS' , 'Der rasche technologische Wandel transformiert derzeit die Arbeitswelt. Gleichzeitig wandeln sich Berufsbilder, Geschäfts-, Kollaborations- und Führungsmodelle. Es entstehen durch Digitalisierung zunehmend Möglichkeiten, mobil und flexibel zu arbeiten. Jedoch steigen auch die Anforderungen an einzelne Mitarbeitende, sich selbst zu organisieren. Dabei werden neue Kompetenzen gefragt und digitale Kompetenzen stärker gefordert.  Der CAS Psychologie in der neuen Arbeitswelt stellt den Menschen ins Zentrum des digitalen Wandels. Er bietet eine Auseinandersetzung mit verschiedenen Dimensionen der veränderten Arbeitswelt.' , null , 'https://www.zhaw.ch/de/psychologie/weiterbildung/detail/kurs/cas-psychologie-in-der-arbeitswelt-4-0' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (10 , 'Innovation & Leadership' , 1 , 8000 , 12 , 'Deutsch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS' , 'Viele Unternehmen sehen sich heute mit grossem Innovations- und Wandeldruck konfrontiert. Insbesondere KMU können ihre Marktposition meist nur durch gezielte Geschäftsinnovation nachhaltig weiter entwickeln und erfolgreich ausbauen. Eine grosse Herausforderung besteht dabei darin, erfolgreiche Geschäftsmodelle nicht nur zu entwickeln sondern diese auch mit angepasster Führung effektiv umsetzen zu können.  Immer mehr sind deshalb Führungskräfte gefragt, welche sowohl die konzeptionelle und strategische Entwicklung neuer Geschäftsmodelle beherrschen als auch deren zielgerichtete Umsetzung durch innovationsgerichtete Führung beherrschen. Welches Geschäftsmodell ist am erfolgversprechendsten? Wie wirken sich Steuerungslogiken und Unternehmenskultur auf die Umsetzung des Geschäftsmodelles aus? Wie können Führung und Organisation optimal auf das neue Modell abgestimmt werden?  Dieser Lehrgang vermittelt einerseits die relevanten Instrumente, Methoden und Strategien der Geschäftsmodellentwicklung. Andererseits lernen Sie, wie Sie als Führungskraft die internen Aktivitäten und Anreizsysteme optimal und erfolgsversprechend darauf abstimmen können.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-innovation-leadership' , 'https://www.zhaw.ch/storage/sml/institute-zentren/iwi/buehnenbild-iwi.jpg' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (11 , 'Cyber Security' , 1 , 8000 , 12 , 'Deutsch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'HS' , 'Cyber-Security ist mehr denn je relevant, da Angriffe auf Unternehmen und ihre Infrastrukturen weiter zunehmen. Traditionelle Schutzkonzepte, die rein auf Vermeidung abzielen, reichen nicht mehr aus.

Technik alleine kann keinen ausreichenden Schutz bieten. Vielmehr bedingt es Massnahmen auf Ebene des Managements, der Technik und der Mitarbeitenden, die alle ineinandergreifen. Durch die Verknüpfung der Massnahmen auf den verschiedenen Ebenen gelingt es, Angriffen vorzubeugen, diese abzuwehren und deren Schäden zu minimieren. Die Technik birgt jedoch eine fast unüberschaubare Komplexität und muss (zumindest im Ansatz) von Entscheidungsträgern verstanden werden.
Der CAS Cyber Security befähigt auf Seite der Technik und auf Seite des Managements gleichermassen:

Im Management-Teil überzeugt der CAS durch Strategien, wie die Technik gewinnbringend eingesetzt werden kann, um die Unternehmung zu schützen und Angriffe frühzeitig zu erkennen. Dabei legt der CAS einen Schwerpunkt auf die organisatorische Gestaltung, Schulung und Rolle der Mitarbeitenden und den Wissenstransfer innerhalb und ausserhalb des Unternehmens.

Im technischen Teil erzeugt der CAS ein Grundverständnis der Begriffe und des State-of-the-Art in Bezug auf Angriffsformen sowie Schutzkonzepte.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-cyber-security/' , '' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (12 , 'Digitale Strategie und Wertschöpfung ' , 1 , 8000 , 12 , 'Deutsch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS' , 'Eine zielgerichtete und langfristig angelegte Strategieentwicklung schafft Werte und erhöht die Leistungsfähigkeit eines Unternehmens. Kaum eine Branche kann dabei die durchdringende Digitalisierung ausser Acht lassen. Es gilt dem digitalen Trend entsprechende Geschäftsmodelle zu entwickeln, Produktportfolios hinsichtlich des digitalen Wandels zu analysieren, die Digitalisierung zur Effizienzsteigerung und Prozessinnovation einzusetzen sowie Führung und Organisation für das einzelne Unternehmen anzupassen.

In dieser fundierten Führungsausbildung wird der proaktive Umgang mit der digitalen Transformation sowie die Fähigkeit gestärkt, eine motivierende digitale Vision zu entwerfen.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-digitale-strategie-und-wertschoepfung/' , '' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (13 , 'Digitale Technologien und Innovation' , 1 , 8000 , 12 , 'Deutsch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS' , 'Innovieren Sie noch oder digitalisieren Sie schon? Alles, was digitalisierbar ist, wird auch digitalisiert werden. Dies ist nur eine Frage der Zeit und des Geschäftsmodells. Die Digitalisierung wird immer stärker zur treibenden Kraft für Innovationen, die traditionelle Geschäftslogiken infrage stellen. Digitale Technologien spielen eine entscheidende Rolle bei Produkt-, Service- und Geschäftsmodellinnovationen. Wie kann ein Unternehmen von den Technologietrends profitieren, um «digitale» Innovationen frühzeitig voranzutreiben?' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-digitale-technologien-und-innovation/' , '' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (14 , 'IT-Sourcing und Cloud Provider Management' , 1 , 8000 , 12 , 'Deutsch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'HS' , 'Eine ausgereifte und ganzheitliche Multi-IT- und Digital-Sourcing-Strategie ist essentiell für eine erfolgreiche und effiziente IT-Lieferorganisation.  Nur durch den intelligenten und flexiblen Einsatz von externen IT-Dienstleistern, Technologien und Cloud-Computing-Diensten wird es einer IT-Organisation möglich, sich auf neue IT- und Business-Fähigkeiten zu konzentrieren und den Anforderungen der erhöhten Agilität und Kundenorientiertheit gerecht zu werden. Ein ebenso wichtiger Trend ist die Verschiebung von reinen «Make OR Buy» zu «Make AND Buy» Entscheidungen für z.B. neue Technologien wie Big Data, AI, IoT, Blockchain oder Digitalisierungs- und digitale Plattform-Initiativen. Dabei stellt sich die Gestaltung einer erfolgreichen Multi-Sourcing-Strategie als nicht trivial heraus. Schon lange ist der beste IT-Sourcing-Mix nicht mehr durch einen exklusiven Schweizer Markt notwendig und wirtschaftlich argumentierbar. Entscheidungsmodelle zur Auswahl eines geeigneten Cloud-Anbieters sind dafür unabdingbar. Heute ist das globale IT-Sourcing und der Einsatz von internationalen, teiweise kostengünstigeren IT-Ressourcen auch für KMU und mid-size Unternehmen eine valide und betrachtungswürdige Option.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-it-sourcing-und-cloud-provider-management/' , '' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (15 , 'Agile Prototyping und Validation' , 1 , 8000 , 12 , 'Deutsch/Englisch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS/HS' , 'Die Geschwindigkeit, mit der Ideen ausgearbeitet, verwirklicht und experimentell geprüft werden können leistet einen wichtigen Beitrag zum Erfolg digitaler Produkte. Damit die Erkenntnisse aus den Experimenten in die stetige Weiterentwicklung der Ideen einfliessen können, müssen Produkte nicht nur einmalig rasch umgesetzt, sondern ebenso schnell für erneute Prüfungen angepasst werden können.

Es gibt heutzutage eine Vielzahl an Technologien, Werkzeuge und Vorgehensweisen, welche ein breites Publikum dazu befähigen, digitale Produkte prototypisch umzusetzen. Zum Beispiel werden oft sog. Mock-Ups in unterschiedlichen Reifegraden eingesetzt, um das visuelle Produkteverhalten (Benutzerschnittstellen) auszuarbeiten. Doch nur selten macht das visuelle Verhalten allein die Attraktivität eines Produkts aus. Sowohl das logische Produkteverhalten (Algorithmen) wie auch die dem Gesamtverhalten zugrundeliegenden Informationen (Daten) tragen zum gesamten Produkterlebnis bei.

Im CAS Agile Prototyping and Validation lernen Sie sorgfältig ausgewählte Technologien und Werkzeuge kennen, mit denen Sie ohne viel technischem Verständnis agil und benutzernah ganzheitliche digitale Produkte gestalten und experimentell prüfen können. Mit diesen Fähigkeiten sind Sie bei der Konzeption digitaler Produkte von Anfang an beteiligt und tragen zur Beschleunigung der Ausarbeitung von Ideen bei.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-agile-prototyping-und-validation/' , '' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (16 , 'Agiles Requirements Engineering' , 1 , 8000 , 12 , 'Deutsch/Englisch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS' , 'Die im traditionellem Requirements Engineering angewandten Methoden zum Erheben und Dokumentieren von Anforderungen verlieren im Kontext agiler Vorhaben zunehmend an Bedeutung. An Bedeutung gewinnen hingegen wertorientierte, iterative Vorgehensweisen, enge Zusammenarbeit mit Stakeholdern und rasche Auslieferungen. Dies verlangt von den an der Entwicklung beteiligten Personen ein klares, gemeinsames Werteverständnis sowie Agilität im gesamten Entwicklungszyklus. Im CAS Agile Requirements Engineering bereiten wir Sie auf diese Änderungen vor. Zudem haben Sie die Möglichkeit im Rahmen des CAS die Zertifizierung RE@Agile Primer zu absolvieren.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-agile-requirements-engineering/' , '' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (17 , 'Data Engineering' , 1 , 8000 , 12 , 'Deutsch/Englisch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS' , 'Das Phänomen der stetig zunehmenden Menge und Verbreitung von Daten begleitet uns seit Jahrzehnten. Zu Beginn dieser Entwicklung entstanden Daten in Systemen, bei denen die gewünschte Funktionalität die Form und Verarbeitung der Daten vorgab. Heutzutage verbreiten sich aber auch Systeme, in denen ein Nutzen dank dem Einbezug vorhandener Daten gestiftet wird. Mit dem Wachstum der Datenmenge haben wir immer mehr Möglichkeiten, nützliche Produkte, Dienstleistungen und effektive Betriebe zu gestalten.

Data Engineering wird je nach Definition als Teilbereich oder als Vorstufe des Data Science verstanden und befasst sich hauptsächlich mit den praktischen Aspekten der Datenbeschaffung und Zusammenführung, über den gesamten Datenaufbereitungskreislauf bis zur Analyse mit dem Ziel, eine qualitativ und quantitativ akzeptable Datengrundlage für die anschliessende Modellierung zu bieten.

Menschen in diversen Positionen, Funktionen und Rollen verfolgen ein breites Spektrum an Aufgaben, in denen Fragestellungen mit der Erfassung, Aufbereitung und Verarbeitung von Daten effektiver und effizienter beantwortet werden können. Im CAS Data Engineering vermitteln wir ein ebenso breites Spektrum an Werkzeugen und Methoden, welche Teilnehmende dazu befähigt, in ihrem beruflichen Alltag Daten mittels systematischer Techniken bereitzustellen, damit aus diesen ein konkreter Nutzen gewonnen werden kann.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-data-engineering/' , '' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (18 , 'Digital Marketing' , 1 , 8200 , 12 , 'Deutsch/Englisch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'FS/HS' , 'Das strategische Marketing-Management bildet das Fundament der Lerninhalte des CAS Digital Marketing. Sie lernen, die Digital-Marketing-Bereitschaft (Maturität) Ihres Unternehmens zu evaluieren, um Handlungsfelder und Möglichkeiten zur Mehrwert-Generierung zu erkennen.

Sie werden befähigt, eine Digital-Marketing-Strategie, abgeleitet aus der Unternehmens- und der Marketing-Strategie, zu entwickeln und erfolgreich zu implementieren. Sie setzen sich zudem mit Charakteristika und Einsatzmöglichkeiten klassischer und interaktiver digitaler Marketing-Instrumente (Social Media) auseinander und können den digitalen Marketing-Mix für Ihre Organisation aufsetzen.

Sie werden mit den Aspekten der Unternehmensführung im digitalen Zeitalter vertraut gemacht, erkennen Spannungsfelder von Veränderungsprozessen und kennen Werkzeuge, um diese aktiv zu steuern. Dabei thematisieren Sie Bereiche der Digital-Governance, der rechtlichen Rahmenbedingungen, des Change Managements sowie des Empowerments von Mitarbeitenden.
' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-digital-marketing/' , '' );
INSERT INTO RE_ENGINE.CAS (ID,NAME,DURATION,COSTS,ECTS_COUNT,LANGUAGE,CAMPUS,START,DESCRIPTION,PROMO_PRIORITY,URL_WEB,URL_ICON) VALUES (19 , 'Unternehmensentwicklung' , 1 , 8000 , 12 , 'Deutsch/Englisch' , 'Campus der ZHAW School of Management and Law, Winterthur' , 'HS' , 'Unternehmen befinden sich in einem stetigen Wandel. Dies führt zu einem steigenden Bedarf, die nötigen Veränderungen bewusst zu gestalten und den Wirkungsgrad der unternehmerischen Anpassung zu erhöhen.

Der vorliegende CAS befähigt die Teilnehmenden, Tools und Modelle zur strategischen Entscheidungsfindung einzusetzen und strategische Entscheidungen zu bewerten. Zudem werden die Teilnehmenden lernen, die mit den strategischen Entscheidungen einhergehenden Veränderungen in der Organisation und im Führungsverhalten zu reflektieren und zu gestalten. Nebst den klassischen Modellen werden auch neue Themen wie agile Strategieprozesse und disruptive Organisationsformen besprochen.' , null , 'https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-unternehmensentwicklung/' , '' );



CREATE TABLE RE_ENGINE.CAS_BUNDLE (
	ID BIGINT PRIMARY KEY,
	MAS_ID BIGINT NOT NULL,
	NAME VARCHAR(255) NOT NULL,
	MIN_CAS INT,
	MAX_CAS INT,
	DESCRIPTION VARCHAR(4095),
	mandatory_bundle INT,
	UNIQUE (MAS_ID, NAME)
);
INSERT INTO RE_ENGINE.CAS_BUNDLE (ID,MAS_ID,NAME,MIN_CAS,MAX_CAS,DESCRIPTION,MANDATORY_BUNDLE) VALUES (1,1,'Pflicht CAS',2,2,'',1);
INSERT INTO RE_ENGINE.CAS_BUNDLE (ID,MAS_ID,NAME,MIN_CAS,MAX_CAS,DESCRIPTION,MANDATORY_BUNDLE) VALUES (2,1,'Wahlpflicht CAS',2,8,'',0);
INSERT INTO RE_ENGINE.CAS_BUNDLE (ID,MAS_ID,NAME,MIN_CAS,MAX_CAS,DESCRIPTION,MANDATORY_BUNDLE) VALUES (3,2,'Pflicht CAS',2,2,'',1);
INSERT INTO RE_ENGINE.CAS_BUNDLE (ID,MAS_ID,NAME,MIN_CAS,MAX_CAS,DESCRIPTION,MANDATORY_BUNDLE) VALUES (4,2,'Wahlpflicht CAS',2,6,'',0);
INSERT INTO RE_ENGINE.CAS_BUNDLE (ID,MAS_ID,NAME,MIN_CAS,MAX_CAS,DESCRIPTION,MANDATORY_BUNDLE) VALUES (5,3,'Pflicht CAS',2,2,'',1);
INSERT INTO RE_ENGINE.CAS_BUNDLE (ID,MAS_ID,NAME,MIN_CAS,MAX_CAS,DESCRIPTION,MANDATORY_BUNDLE) VALUES (6,3,'Wahlpflicht CAS',2,8,'',0);



CREATE TABLE RE_ENGINE.CAS_TO_BUNDLE (
	CAS_ID BIGINT NOT NULL,
	BUNDLE_ID BIGINT NOT NULL,
	PRIMARY KEY (CAS_ID, BUNDLE_ID)
);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (1,1);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (2,1);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (15,2);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (16,2);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (8,2);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (19,2);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (12,2);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (3,2);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (17,2);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (11,2);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (7,3);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (6,3);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (12,4);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (8,4);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (14,4);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (13,4);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (11,4);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (9,4);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (12,5);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (13,5);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (3,6);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (10,6);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (1,6);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (2,6);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (21,6);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (14,6);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (18,6);
INSERT INTO RE_ENGINE.CAS_TO_BUNDLE (CAS_ID,BUNDLE_ID) VALUES (20,6);



CREATE TABLE RE_ENGINE.QUESTION (
	ID BIGINT PRIMARY KEY,
	TYPE VARCHAR(255),
	DIMENSION VARCHAR(255),
	FILTER_PARAMETER VARCHAR(255),
	TEXT VARCHAR(1000),
	DIMENSION_IMPORTANCE FLOAT
);

INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (1 , 'Fachlich' ,'Digitale Geschäftsprozesse','c_pref_digital_business_process', 'Analyse, Gestaltung, Optimierung, Automatisierung und Überwachung von IT-unterstützten Prozessen ist für mich ein interessantes Fachgebiet.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (2 , 'Fachlich' ,'Engineering','c_pref_engineering', 'Ich bin bereit die Managementebene  bei Bedarf zu verlassen, um mich mit technischer Konzeption und  Entwicklung von IT-Systemen und Applikationen zu beschäftigen.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (3 , 'Fachlich' ,'Führungsinteresse','c_pref_leadership', 'Ich möchte andere Personen führen, begeistern, empowern und entwickeln' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (4 , 'Fachlich' ,'Innovation','c_pref_innovation', 'Ich begeistere mich für Technologietrends und stelle im Kontext der Digitalisierung traditionelle Geschäftslogiken in Frage.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (5 , 'Fachlich' ,'Marketing/Vertrieb','c_pref_marketing', 'Digital und Social Media Marketing sind interessante Themengebiete, welche ich gerne bei der Entwicklung und Implementierung der Marketing-Strategie miteinbeziehen möchte.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (6 , 'Fachlich' ,'Operative Unternehmensorganisation','c_pref_operative_organisation', 'Ich interessiere mich für agile Methoden innerhalb der Organisation hinsichtlich von Produktentwicklung und Projektmanagement. Ich begeistere mich für die Analyse und Optimierung von Abläufen und Strukturen in einem Unternehmen. Ebenfalls Arbeite ich gerne mit Daten und Dokumentationen aus der Informatik und erstelle Reports.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (7 , 'Fachlich' ,'Unternehmensentwicklung','c_pref_business_development', 'Ich begeistere mich dafür in einer Kaderposition geschäftsführende Entscheidungen zu unterstützen oder selber zu treffen. Ebenfalls Interessiere ich mich für strategische Herausforderungen und Aufgaben.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (8 , 'Persönlich' ,'Soziale Intelligenz','c_pref_social_intelligence', 'Kollegen/Kolleginen würden mich als sehr emphatische Person mit starken sozialen Fertigkeiten beschreiben. Ebenfalls sehe ich mich selbst als mitfühlende und verständnisvolle Persönlichkeit.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (9 , 'Persönlich' ,'Extraversion','c_pref_extraversion', 'Wie extrovertiert sind Sie?|Während den Pausen bin ich lieber für mich allein und schalte ab.|Ich gehe gerne unter Leute, suche jedoch nicht aktiv das Gespräch.|Ich gehe auf Personen zu, übernehme das Wort in einer Gruppe.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (10 , 'Persönlich' ,'Rationalität/Analytische Fähigkeiten','c_pref_rationality', 'Ich gehe den Ursachen eines Problems gerne auf den Grund, um herrauszufinden wo das Problem wirklich liegt.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (11 , 'Persönlich' ,'Kreativität/Offen für Neues','c_pref_creativity', 'Ich liebe es in meiner Freizeit neue Dinge auszuprobieren und Erfahrungen zu sammeln. Generell bin ich gegenüber Veränderungen offen eingestellt.' , 0);
INSERT INTO RE_ENGINE.QUESTION (ID,TYPE,DIMENSION,FILTER_PARAMETER,TEXT,DIMENSION_IMPORTANCE) VALUES (12 , 'Persönlich' ,'Gewissenhaftigkeit','c_pref_conscience', 'Ich liebe Genauigkeit und Perfektionismuss und kontrolliere gerne zwei mal bevor ich mein erarbeitetes Ergebniss anderen mitteile.' , 0);


CREATE TABLE RE_ENGINE.FILTER_PROPERTY (
	ID BIGINT PRIMARY KEY,
	MAS_ID BIGINT,
	CAS_ID BIGINT,
	QUESTION_ID BIGINT,
	IMPORTANCE FLOAT
);

INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (1,1,null,1,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (2,1,null,2,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (3,1,null,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (4,1,null,4,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (5,1,null,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (6,1,null,6,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (7,1,null,7,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (9,1,null,8,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (10,1,null,9,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (11,1,null,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (12,1,null,11,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (13,1,null,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (14,2,null,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (15,2,null,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (16,2,null,3,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (17,2,null,4,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (18,2,null,5,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (19,2,null,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (20,2,null,7,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (22,2,null,8,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (23,2,null,9,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (24,2,null,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (25,2,null,11,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (26,2,null,12,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (27,3,null,1,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (28,3,null,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (29,3,null,3,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (30,3,null,4,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (31,3,null,5,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (32,3,null,6,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (33,3,null,7,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (35,3,null,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (36,3,null,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (37,3,null,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (38,3,null,11,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (39,3,null,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (40,null,1,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (41,null,1,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (42,null,1,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (43,null,1,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (44,null,1,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (45,null,1,6,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (46,null,1,7,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (48,null,1,8,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (49,null,1,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (50,null,1,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (51,null,1,11,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (52,null,1,12,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (53,null,2,1,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (54,null,2,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (55,null,2,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (56,null,2,4,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (57,null,2,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (58,null,2,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (59,null,2,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (61,null,2,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (62,null,2,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (63,null,2,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (64,null,2,11,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (65,null,2,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (66,null,3,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (67,null,3,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (68,null,3,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (69,null,3,4,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (70,null,3,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (71,null,3,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (72,null,3,7,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (74,null,3,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (75,null,3,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (76,null,3,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (77,null,3,11,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (78,null,3,12,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (79,null,4,1,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (80,null,4,2,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (81,null,4,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (82,null,4,4,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (83,null,4,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (84,null,4,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (85,null,4,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (87,null,4,8,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (88,null,4,9,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (89,null,4,10,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (90,null,4,11,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (91,null,4,12,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (92,null,5,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (93,null,5,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (94,null,5,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (95,null,5,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (96,null,5,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (97,null,5,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (98,null,5,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (100,null,5,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (101,null,5,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (102,null,5,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (103,null,5,11,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (104,null,5,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (105,null,6,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (106,null,6,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (107,null,6,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (108,null,6,4,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (109,null,6,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (110,null,6,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (111,null,6,7,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (113,null,6,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (114,null,6,9,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (115,null,6,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (116,null,6,11,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (117,null,6,12,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (118,null,7,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (119,null,7,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (120,null,7,3,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (121,null,7,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (122,null,7,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (123,null,7,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (124,null,7,7,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (126,null,7,8,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (127,null,7,9,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (128,null,7,10,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (129,null,7,11,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (130,null,7,12,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (131,null,8,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (132,null,8,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (133,null,8,3,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (134,null,8,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (135,null,8,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (136,null,8,6,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (137,null,8,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (139,null,8,8,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (140,null,8,9,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (141,null,8,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (142,null,8,11,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (143,null,8,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (144,null,9,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (145,null,9,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (146,null,9,3,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (147,null,9,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (148,null,9,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (149,null,9,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (150,null,9,7,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (152,null,9,8,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (153,null,9,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (154,null,9,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (155,null,9,11,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (156,null,9,12,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (157,null,10,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (158,null,10,2,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (159,null,10,3,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (160,null,10,4,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (161,null,10,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (162,null,10,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (163,null,10,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (165,null,10,8,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (166,null,10,9,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (167,null,10,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (168,null,10,11,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (169,null,10,12,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (170,null,11,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (171,null,11,2,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (172,null,11,3,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (173,null,11,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (174,null,11,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (175,null,11,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (176,null,11,7,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (178,null,11,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (179,null,11,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (180,null,11,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (181,null,11,11,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (182,null,11,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (183,null,12,1,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (184,null,12,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (185,null,12,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (186,null,12,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (187,null,12,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (188,null,12,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (189,null,12,7,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (191,null,12,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (192,null,12,9,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (193,null,12,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (194,null,12,11,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (195,null,12,12,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (196,null,13,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (197,null,13,2,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (198,null,13,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (199,null,13,4,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (200,null,13,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (201,null,13,6,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (202,null,13,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (204,null,13,8,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (205,null,13,9,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (206,null,13,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (207,null,13,11,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (208,null,13,12,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (209,null,14,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (210,null,14,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (211,null,14,3,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (212,null,14,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (213,null,14,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (214,null,14,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (215,null,14,7,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (217,null,14,8,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (218,null,14,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (219,null,14,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (220,null,14,11,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (221,null,14,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (222,null,15,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (223,null,15,2,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (224,null,15,3,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (225,null,15,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (226,null,15,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (227,null,15,6,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (228,null,15,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (230,null,15,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (231,null,15,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (232,null,15,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (233,null,15,11,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (234,null,15,12,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (235,null,16,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (236,null,16,2,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (237,null,16,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (238,null,16,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (239,null,16,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (240,null,16,6,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (241,null,16,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (243,null,16,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (244,null,16,9,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (245,null,16,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (246,null,16,11,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (247,null,16,12,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (248,null,17,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (249,null,17,2,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (250,null,17,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (251,null,17,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (252,null,17,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (253,null,17,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (254,null,17,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (256,null,17,8,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (257,null,17,9,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (258,null,17,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (259,null,17,11,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (260,null,17,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (261,null,18,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (262,null,18,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (263,null,18,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (264,null,18,4,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (265,null,18,5,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (266,null,18,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (267,null,18,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (269,null,18,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (270,null,18,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (271,null,18,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (272,null,18,11,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (273,null,18,12,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (274,null,19,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (275,null,19,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (276,null,19,3,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (277,null,19,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (278,null,19,5,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (279,null,19,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (280,null,19,7,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (282,null,19,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (283,null,19,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (284,null,19,10,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (285,null,19,11,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (286,null,19,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (287,null,20,1,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (288,null,20,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (289,null,20,3,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (290,null,20,4,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (291,null,20,5,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (292,null,20,6,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (293,null,20,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (295,null,20,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (296,null,20,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (297,null,20,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (298,null,20,11,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (299,null,20,12,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (300,null,21,1,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (301,null,21,2,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (302,null,21,3,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (303,null,21,4,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (304,null,21,5,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (305,null,21,6,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (306,null,21,7,1);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (308,null,21,8,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (309,null,21,9,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (310,null,21,10,3);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (311,null,21,11,2);
INSERT INTO RE_ENGINE.FILTER_PROPERTY (ID,MAS_ID,CAS_ID,QUESTION_ID,IMPORTANCE) VALUES (312,null,21,12,2);


CREATE TABLE RE_ENGINE.CONFIG_RULES (
	ID BIGINT PRIMARY KEY,
	RULES_XML TEXT NOT NULL
);

CREATE VIEW PRODUCT_CATALOG_VIEW AS
SELECT * FROM (
-- CAS
	SELECT
	c.ID AS p_id,
	'CAS' AS p_type,
	c.NAME AS p_name,
	fpd1.IMPORTANCE as p_d_digital_business_process,
	fpd2.IMPORTANCE as p_d_engineering,
	fpd3.IMPORTANCE as p_d_leadership,
	fpd4.IMPORTANCE as p_d_innovation,
	fpd5.IMPORTANCE as p_d_marketing,
	fpd6.IMPORTANCE as p_d_operative_organisation,
	fpd7.IMPORTANCE as p_d_business_development,
	fpd8.IMPORTANCE as p_d_social_intelligence,
	fpd9.IMPORTANCE as p_d_extraversion,
	fpd10.IMPORTANCE as p_d_rationality,
	fpd11.IMPORTANCE as p_d_creativity,
	fpd12.IMPORTANCE as p_d_conscience,
	(SELECT string_agg('' || cb.mas_id, '#')
	FROM RE_ENGINE.CAS cs
	INNER JOIN RE_ENGINE.CAS_TO_BUNDLE ccb on cs.id = ccb.cas_id
	INNER JOIN RE_ENGINE.CAS_BUNDLE cb on ccb.bundle_id = cb.id and cb.mandatory_bundle > 0
	WHERE cs.ID = c.ID
	) AS p_req_mas_ids
	FROM RE_ENGINE.CAS c
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd1 ON fpd1.CAS_ID = c.ID AND fpd1.QUESTION_ID = 1
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd2 ON fpd2.CAS_ID = c.ID AND fpd2.QUESTION_ID = 2
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd3 ON fpd3.CAS_ID = c.ID AND fpd3.QUESTION_ID = 3
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd4 ON fpd4.CAS_ID = c.ID AND fpd4.QUESTION_ID = 4
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd5 ON fpd5.CAS_ID = c.ID AND fpd5.QUESTION_ID = 5
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd6 ON fpd6.CAS_ID = c.ID AND fpd6.QUESTION_ID = 6
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd7 ON fpd7.CAS_ID = c.ID AND fpd7.QUESTION_ID = 7
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd8 ON fpd8.CAS_ID = c.ID AND fpd8.QUESTION_ID = 8
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd9 ON fpd9.CAS_ID = c.ID AND fpd9.QUESTION_ID = 9
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd10 ON fpd10.CAS_ID = c.ID AND fpd10.QUESTION_ID = 10
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd11 ON fpd11.CAS_ID = c.ID AND fpd11.QUESTION_ID = 11
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd12 ON fpd12.CAS_ID = c.ID AND fpd12.QUESTION_ID = 12

UNION ALL

-- MAS
	SELECT
	m.ID AS p_id,
	'MAS' AS p_type,
	m.NAME AS p_name,
	fpd1.IMPORTANCE as p_d_digital_business_process,
	fpd2.IMPORTANCE as p_d_engineering,
	fpd3.IMPORTANCE as p_d_leadership,
	fpd4.IMPORTANCE as p_d_innovation,
	fpd5.IMPORTANCE as p_d_marketing,
	fpd6.IMPORTANCE as p_d_operative_organisation,
	fpd7.IMPORTANCE as p_d_business_development,
	fpd8.IMPORTANCE as p_d_social_intelligence,
	fpd9.IMPORTANCE as p_d_extraversion,
	fpd10.IMPORTANCE as p_d_rationality,
	fpd11.IMPORTANCE as p_d_creativity,
	fpd12.IMPORTANCE as p_d_conscience,
	null AS p_req_mas_ids
	FROM RE_ENGINE.MAS m
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd1 ON fpd1.MAS_ID = m.ID AND fpd1.QUESTION_ID = 1
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd2 ON fpd2.MAS_ID = m.ID AND fpd2.QUESTION_ID = 2
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd3 ON fpd3.MAS_ID = m.ID AND fpd3.QUESTION_ID = 3
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd4 ON fpd4.MAS_ID = m.ID AND fpd4.QUESTION_ID = 4
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd5 ON fpd5.MAS_ID = m.ID AND fpd5.QUESTION_ID = 5
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd6 ON fpd6.MAS_ID = m.ID AND fpd6.QUESTION_ID = 6
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd7 ON fpd7.MAS_ID = m.ID AND fpd7.QUESTION_ID = 7
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd8 ON fpd8.MAS_ID = m.ID AND fpd8.QUESTION_ID = 8
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd9 ON fpd9.MAS_ID = m.ID AND fpd9.QUESTION_ID = 9
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd10 ON fpd10.MAS_ID = m.ID AND fpd10.QUESTION_ID = 10
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd11 ON fpd11.MAS_ID = m.ID AND fpd11.QUESTION_ID = 11
	LEFT JOIN RE_ENGINE.FILTER_PROPERTY fpd12 ON fpd12.MAS_ID = m.ID AND fpd12.QUESTION_ID = 12
) t;