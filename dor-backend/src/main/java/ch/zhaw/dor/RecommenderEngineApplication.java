package ch.zhaw.dor;

import java.util.Arrays;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

import ch.zhaw.dor.db.bean.Profession;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableJdbcRepositories(basePackageClasses=Profession.class)
@EnableConfigurationProperties(EngineConfig.class)
public class RecommenderEngineApplication {

    public static void main(String[] args) {
    	new SpringApplicationBuilder(RecommenderEngineApplication.class)
    		.listeners(new ConfigurationLogger())
    		.run(args);
    }

    /**
     * 
     * @author cst
     *
     */
    private static class ConfigurationLogger implements ApplicationListener<ApplicationPreparedEvent> {

    	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationLogger.class);
    	
		@Override
		public void onApplicationEvent(ApplicationPreparedEvent event) {
			
			final Environment env = event.getApplicationContext().getEnvironment();
			
			LOGGER.info("====== Environment and configuration ======");
			LOGGER.info("Active profiles: {}", Arrays.toString(env.getActiveProfiles()));
			
			final MutablePropertySources sources = ((AbstractEnvironment) env).getPropertySources();
			StreamSupport.stream(sources.spliterator(), false).filter(ps -> ps instanceof EnumerablePropertySource)
					.map(ps -> ((EnumerablePropertySource<?>) ps).getPropertyNames()).flatMap(Arrays::stream).distinct()
					.filter(prop -> !(prop.contains("credentials") || prop.contains("password")))
					.sorted()
					.forEach(prop -> LOGGER.info("{}: {}", prop, env.getProperty(prop)));
			
			LOGGER.info("===========================================");
			
		}
    }
}
