package ch.zhaw.dor.rest.beans;

import ls13.productfinder.api.Product;

import java.io.Serializable;
import java.util.List;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class Cas implements Serializable {
	
	private static final long serialVersionUID = -1462422593129933238L;
	
	private Long id;
	private String name;
	private String description;
	private String urlWeb;
	private String urlIcon;
	private String start;
	private Integer ectsCount;
	
	private long score;
	/** user-readable texts why this CAS was selected */
	private List<String> explanations;

	private transient Product product;
	private transient ch.zhaw.dor.db.bean.Cas dbBean;
	
	public static Cas fromDbBean(ch.zhaw.dor.db.bean.Cas dbBean) {
		if (dbBean == null) {
			return null;
		}
		
		final Cas cas = new Cas();
		cas.setId(dbBean.getId());
		cas.setName(dbBean.getName());
		cas.setDescription(dbBean.getDescription());
		cas.setUrlWeb(dbBean.getUrlWeb());
		cas.setUrlIcon(dbBean.getUrlIcon());
		cas.setStart(dbBean.getStart());
		cas.setEctsCount(dbBean.getEctsCount());
		cas.setDbBean(dbBean);
		return cas;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the score
	 */
	public long getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(long score) {
		this.score = score;
	}
	/**
	 * @return the urlWeb
	 */
	public String getUrlWeb() {
		return urlWeb;
	}
	/**
	 * @param urlWeb the urlWeb to set
	 */
	public void setUrlWeb(String urlWeb) {
		this.urlWeb = urlWeb;
	}
	/**
	 * @return the urlIcon
	 */
	public String getUrlIcon() {
		return urlIcon;
	}
	/**
	 * @param urlIcon the urlIcon to set
	 */
	public void setUrlIcon(String urlIcon) {
		this.urlIcon = urlIcon;
	}
	/**
	 * @return the explanations
	 */
	public List<String> getExplanations() {
		return explanations;
	}
	/**
	 * @param explanations the explanations to set
	 */
	public void setExplanations(List<String> explanations) {
		this.explanations = explanations;
	} 	
	/**
	 * @return the start
	 */
	public String getStart() {
		return start;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(String start) {
		this.start = start;
	}
	/**
	 * @return the ectsCount
	 */
	public Integer getEctsCount() {
		return ectsCount;
	}
	/**
	 * @param ectsCount the ectsCount to set
	 */
	public void setEctsCount(Integer ectsCount) {
		this.ectsCount = ectsCount;
	}	
	
	/**
	 * @param id the id to set
	 */
	public Cas withId(Long id) {
		this.id = id;
		return this;
	}
	/**
	 * @param score the score to set
	 */
	public Cas withScore(long score) {
		this.score = score;
		return this;
	}
	/**
	 * @param description the description to set
	 */
	public Cas withDescription(String description) {
		this.description = description;
		return this;
	}
	/**
	 * @param name the name to set
	 */
	public Cas withName(String name) {
		this.name = name;
		return this;
	}
	/**
	 * @param urlIcon the urlIcon to set
	 */
	public Cas withUrlIcon(String urlIcon) {
		this.urlIcon = urlIcon;
		return this;
	}
	/**
	 * @param explanations the explanations to set
	 */
	public Cas withExplanations(List<String> explanations) {
		this.explanations = explanations;
		return this;
	}
	/**
	 * @param urlWeb the urlWeb to set
	 */
	public Cas withUrlWeb(String urlWeb) {
		this.urlWeb = urlWeb;
		return this;
	}
	/**
	 * @return the dbBean
	 */
	public ch.zhaw.dor.db.bean.Cas getDbBean() {
		return dbBean;
	}
	/**
	 * @param dbBean the dbBean to set
	 */
	public void setDbBean(ch.zhaw.dor.db.bean.Cas dbBean) {
		this.dbBean = dbBean;
	}
	
	@Override
	public String toString() {
		return "Cas [id=" + id + ", name=" + name + ", score=" + score + ", explanations=" + explanations + "]";
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}