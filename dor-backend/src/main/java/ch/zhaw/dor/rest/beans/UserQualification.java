package ch.zhaw.dor.rest.beans;

import java.io.Serializable;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class UserQualification implements Serializable {

	private static final long serialVersionUID = 6079866714204507524L;
	
	private String graduation;
	/** has at least 3 years professional experience */
	private boolean professionalExperience;
	/** has pre-course "wissenschaftliches Arbeiten" completed */
	private boolean preCourse;
	
	/**
	 * @return the graduation
	 */
	public String getGraduation() {
		return graduation;
	}
	/**
	 * @param graduation the graduation to set
	 */
	public void setGraduation(String graduation) {
		this.graduation = graduation;
	}
	/**
	 * @return the professionalExperience
	 */
	public boolean isProfessionalExperience() {
		return professionalExperience;
	}
	/**
	 * @param professionalExperience the professionalExperience to set
	 */
	public void setProfessionalExperience(boolean professionalExperience) {
		this.professionalExperience = professionalExperience;
	}
	/**
	 * @return the preCourse
	 */
	public boolean isPreCourse() {
		return preCourse;
	}
	/**
	 * @param preCourse the preCourse to set
	 */
	public void setPreCourse(boolean preCourse) {
		this.preCourse = preCourse;
	}

	@Override
	public String toString() {
		return "UserQualification [graduation=" + graduation + ", professionalExperience=" + professionalExperience
				+ ", preCourse=" + preCourse + "]";
	}
}
