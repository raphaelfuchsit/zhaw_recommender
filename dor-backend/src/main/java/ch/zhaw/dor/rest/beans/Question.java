package ch.zhaw.dor.rest.beans;

import java.io.Serializable;

/**
 * Rest API bean representing an interest
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class Question implements Serializable {

	private static final long serialVersionUID = 3000806833835480900L;
	
	private static final int NO_RATING = -1;
	
	private Long id;
    private String type;
    private String dimension;
    private String filter_parameter;
    private String text;
	private Double dimension_importance;
    
	/** interest rating 
	 * (-1 - default value)
	 *   0 - not interested
	 *   1 - low
	 *   2 - medium
	 *   3 - high
	 *   4 - very high
	 */
	private Integer rating = NO_RATING;
    
    /**
     * 
     * @param dbBean
     * @return question
     */
    public static Question fromDbBean(ch.zhaw.dor.db.bean.Question dbBean) {
    	if (dbBean == null) {
    		return null;
    	}
    	
    	final Question question = new Question();
		question.setId(dbBean.getId());
		question.setType(dbBean.getType());
		question.setDimension(dbBean.getDimension());
		question.setFilterParameter(dbBean.getFilterParameter());
		question.setText(dbBean.getText());
    	return question;
    }
    
	/**
	 * @return the internal rating used by the engine
	 */
	public final int calculateInternalRating() {
		
    	// interest properties, mapping of user interest rating:
    	//
    	//         user input       |    engine input    
    	// -------------------------+-----------------------
    	//	  -1 - default value    |   * - ignore interest
    	//	   0 - not interested   |  -2 - very low
    	//	   1 - low              |  -1 - low
    	//	   2 - medium           |   0 - neutral
    	//	   3 - high             |   1 - high
    	//	   4 - very high        |   2 - very high
    	//

		if (rating == null || rating < 0) {
			return 0;
		}
		return Math.min(rating - 2, 2);
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getType() { return type; }
	public void setType(String type) { this.type = type; }

	public String getDimension() { return dimension; }
	public void setDimension(String dimension) { this.dimension = dimension; }

	public String getFilterParameter() { return filter_parameter; }
	public void setFilterParameter(String filter_parameter) { this.filter_parameter = filter_parameter; }

	public String getText() { return text; }
	public void setText(String text) { this.text = text; }

	public Integer getRating() {
		return rating == null ? NO_RATING : rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}

	
	@Override
	public String toString() {
		return "Interest [id=" + id + ", type=" + type + ", dimension=" + dimension + ", text=" + text+", rating=" + rating + "]";
	}
}
