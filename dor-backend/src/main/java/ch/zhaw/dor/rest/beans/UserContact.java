package ch.zhaw.dor.rest.beans;

import java.io.Serializable;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class UserContact implements Serializable {

	private static final long serialVersionUID = -6127418750754436014L;
	
	private String email;
	private String firstName;
	private String lastName;
	private String comment;
	private String feedback;
	private String toolRecommendation;
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the feedback
	 */
	public String getFeedback() {
		return feedback;
	}
	/**
	 * @param feedback the feedback to set
	 */
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	/**
	 * @return the toolRecommendation
	 */
	public String getToolRecommendation() {
		return toolRecommendation;
	}
	/**
	 * @param toolRecommendation the toolRecommendation to set
	 */
	public void setToolRecommendation(String toolRecommendation) {
		this.toolRecommendation = toolRecommendation;
	}
	@Override
	public String toString() {
		return "UserContact [email=" + email + ", firstName=" + firstName + ", lastName=" + lastName + ", feedback="
				+ feedback + ", toolRecommendation=" + toolRecommendation + ", comment=" + comment + "]";
	}

	public String toEmailString(){
		return "<p>Email: "+email+
				"<br> Vorname: "+firstName+
				"<br> Nachname: "+lastName+
				"<br> Fedback: "+feedback+
				"<br> Bewertung: "+toolRecommendation+
				"</p>"+
				"Kommentar des Users:"+comment;
	}
}
