package ch.zhaw.dor.rest.api;


/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */



//<!-- PRODUCT PROPERTIES -->
//<ProductProperty name="p_id" type="long"/> <!-- product id -->
//<ProductProperty name="p_type" type="String"/> <!-- product type: ['CAS','MAS'] -->
//<ProductProperty name="p_name" type="String"/> <!-- product name -->
//<ProductProperty name="p_req_mas_ids" type="long[]"/>
//<ProductProperty name="p_d_digital_business_process" type="double"/> <!-- products promotion priority, active when value > 0 -->
//<ProductProperty name="p_d_engineering" type="double"/> <!-- For p_type = 'MAS', contains only professions of mandatory CASs. For p_type = 'CAS', all entries are listed. -->
//<ProductProperty name="p_d_leadership" type="double"/> <!-- For p_type = 'MAS', contains only interests of mandatory CASs. For p_type = 'CAS', all entries are listed. -->
//<ProductProperty name="p_d_innovation" type="double"/> <!-- only set for p_type = 'MAS', contains professions of optional CASs -->
//<ProductProperty name="p_d_marketing" type="double"/> <!-- only set for p_type = 'MAS', contains interests of optional CASs -->
//<ProductProperty name="p_d_operative_organisation" type="double"/>
//<ProductProperty name="p_d_corporate_development" type="double"/>
//<ProductProperty name="p_d_corporate_organisation" type="double"/>
//<ProductProperty name="p_d_social_intelligence" type="double"/>
//<ProductProperty name="p_d_extraversion" type="double"/>
//<ProductProperty name="p_d_rationality" type="double"/>
//<ProductProperty name="p_d_creativity" type="double"/>
//<ProductProperty name="p_d_conscience" type="double"/>
//
//<!-- INTERNAL REQUIREMENTS -->
//<UserRequirement name="c_pref_type" type="String"/>
//<UserRequirement name="c_pref_mas_id" type="long"/>
//<UserRequirement name="c_pref_ids" type="long[]"/>
//<!-- USER REQUIREMENTS -->
//<ProductProperty name="c_pref_digital_business_process" type="double"/> <!-- products promotion priority, active when value > 0 -->
//<ProductProperty name="c_pref_engineering" type="double"/> <!-- For p_type = 'MAS', contains only professions of mandatory CASs. For p_type = 'CAS', all entries are listed. -->
//<ProductProperty name="c_pref_leadership" type="double"/> <!-- For p_type = 'MAS', contains only interests of mandatory CASs. For p_type = 'CAS', all entries are listed. -->
//<ProductProperty name="c_pref_innovation" type="double"/> <!-- only set for p_type = 'MAS', contains professions of optional CASs -->
//<ProductProperty name="c_pref_marketing" type="double"/> <!-- only set for p_type = 'MAS', contains interests of optional CASs -->
//<ProductProperty name="c_pref_operative_organisation" type="double"/>
//<ProductProperty name="c_pref_corporate_development" type="double"/>
//<ProductProperty name="c_pref_corporate_organisation" type="double"/>
//<ProductProperty name="c_pref_social_intelligence" type="double"/>
//<ProductProperty name="c_pref_extraversion" type="double"/>
//<ProductProperty name="c_pref_rationality" type="double"/>
//<ProductProperty name="c_pref_creativity" type="double"/>
//<ProductProperty name="c_pref_conscience" type="double"/>
//
//<!-- c_pref_profession_experience contains experience in current job:
//		-1 - not available
//		3 - up to 3 years
//		7 - up to 7 years
//		12 - up to 12 years
//		99 - more than 12 years -->
//<UserRequirement name="c_pref_profession_experience" type="int"/>
//<UserRequirement name="c_pref_gender" type="String"/>
//


public class EngineConstants {

	// user requirement names
	/** user requirement to filter products by type: MAS or CAS */
	public static final String C_PREF_TYPE = "c_pref_type";
	/** user requirement to filter products by ID: ID of MAS or CAS */
	public static final String C_PREF_IDS = "c_pref_ids";
	/** user requirement to filter products by ID of selected MAS */
	public static final String C_PREF_MAS_ID = "c_pref_mas_id";

	/** old user requirements */
	public static final String C_PREF_PROFESSION_RATING = "c_pref_profession_rating";
	public static final String C_PREF_GENDER = "c_pref_gender";

	/** user requirements based on dimensions */
	public static final String C_PREF_DIGITAL_BUSINESS_PROCESS = "p_d_digital_business_process";
	public static final String C_PREF_ENGINEERING = "p_d_engineering";
	public static final String C_PREF_LEADERSHIP = "p_d_leadership";
	public static final String C_PREF_INNOVATION = "p_d_innovation";
	public static final String C_PREF_MARKETING = "p_d_marketing";
	public static final String C_PREF_OPERATIVE_ORGANISATION = "p_d_operative_organisation";
	public static final String C_PREF_CORPORATE_DEVELOPMENT = "p_d_corporate_development";
	public static final String C_PREF_CORPORATE_ORGANISATION = "p_d_corporate_organisation";
	public static final String C_PREF_SOCIAL_INTELLIGENCE = "p_d_social_intelligence";
	public static final String C_PREF_EXTRAVERSION = "p_d_extraversion";
	public static final String C_PREF_RATIONALITY = "p_d_rationality";
	public static final String C_PREF_CREATIVITY = "p_d_creativity";
	public static final String C_PREF_CONSCIENCE = "p_d_conscience";


	// product property names
	/** product property: ID of MAS or CAS */
	public static final String P_ID = "p_id";

	// product type constants
	public static final String PRODUCT_TYPE_MAS = "MAS";
	public static final String PRODUCT_TYPE_CAS = "CAS";
}
