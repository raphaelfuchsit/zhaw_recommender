package ch.zhaw.dor.rest.api;

public class Page {
    private int id;
    private long[] dimensions;
    private String questionType;
    private String[] questionOptions;


    public Page(int id, long[] dimensions, String questionType, String[] questionOptions){
        this.id = id;
        this.dimensions = dimensions;
        this.questionType = questionType;
        this.questionOptions = questionOptions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long[] getDimensions() {
        return dimensions;
    }

    public void setDimensions(long[] dimensions) {
        this.dimensions = dimensions;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String[] getQuestionOptions() {
        return questionOptions;
    }

    public void setQuestionOptions(String[] questionOptions) {
        this.questionOptions = questionOptions;
    }
}
