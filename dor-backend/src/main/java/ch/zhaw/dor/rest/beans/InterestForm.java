package ch.zhaw.dor.rest.beans;

import java.io.Serializable;
import java.util.List;


/**
 * Rest API bean representing the content of an interest form
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class InterestForm implements Serializable {
	
	private static final long serialVersionUID = 5946042771447964605L;
	
	private int page = -1;
	private boolean hasResult;
	private boolean lastInterests;
	private List<Question> interests;
	private String questionType;
	private String[] questionOptions;

	/**
	 * @return the hasResult
	 */
	public boolean isHasResult() {
		return hasResult;
	}
	/**
	 * @param hasResult the hasResult to set
	 */
	public void setHasResult(boolean hasResult) {
		this.hasResult = hasResult;
	}
	/**
	 * @return the lastInterests
	 */
	public boolean isLastInterests() {
		return lastInterests;
	}
	/**
	 * @param lastInterests the lastInterests to set
	 */
	public void setLastInterests(boolean lastInterests) {
		this.lastInterests = lastInterests;
	}
	/**
	 * @return the interests
	 */
	public List<Question> getInterests() {
		return interests;
	}
	/**
	 * @param interests the interests to set
	 */
	public void setInterests(List<Question> interests) {
		this.interests = interests;
	}
	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}
	/**
	 * @param page the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	@Override
	public String toString() {
		return "InterestForm [page=" + page + ", hasResult=" + hasResult + ", lastInterests=" + lastInterests
				+ ", interests=" + interests + "]";
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String[] getQuestionOptions() {
		return questionOptions;
	}

	public void setQuestionOptions(String[] questionOptions) {
		this.questionOptions = questionOptions;
	}
}
