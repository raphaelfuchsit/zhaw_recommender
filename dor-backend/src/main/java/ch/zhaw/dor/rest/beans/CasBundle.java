package ch.zhaw.dor.rest.beans;

import java.io.Serializable;
import java.util.List;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class CasBundle implements Serializable {
	
	private static final long serialVersionUID = 7520966620687451825L;
	
	private String name;
	private List<Cas> recommendations;
	private List<Cas> options;
	
	private transient ch.zhaw.dor.db.bean.CasBundle dbBean;
	
	public static CasBundle fromDbBean(ch.zhaw.dor.db.bean.CasBundle dbBean) {
		if (dbBean == null) {
			return null;
		}
		
		final CasBundle casBundle = new CasBundle();
		casBundle.setName(dbBean.getName());
		casBundle.setDbBean(dbBean);
		return casBundle;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the recommendations
	 */
	public List<Cas> getRecommendations() {
		return recommendations;
	}
	/**
	 * @param recommendations the recommendations to set
	 */
	public void setRecommendations(List<Cas> recommendations) {
		this.recommendations = recommendations;
	}
	/**
	 * @return the options
	 */
	public List<Cas> getOptions() {
		return options;
	}
	/**
	 * @param options the options to set
	 */
	public void setOptions(List<Cas> options) {
		this.options = options;
	}
	
	/**
	 * @param name the name to set
	 */
	public CasBundle withName(String name) {
		this.name = name;
		return this;
	}
	/**
	 * @param recommendations the recommendations to set
	 */
	public CasBundle withRecommendations(List<Cas> recommendations) {
		this.recommendations = recommendations;
		return this;
	}
	/**
	 * @param options the options to set
	 */
	public CasBundle withOptions(List<Cas> options) {
		this.options = options;
		return this;
	}	
	/**
	 * @return the dbBean
	 */
	public ch.zhaw.dor.db.bean.CasBundle getDbBean() {
		return dbBean;
	}
	/**
	 * @param dbBean the dbBean to set
	 */
	public void setDbBean(ch.zhaw.dor.db.bean.CasBundle dbBean) {
		this.dbBean = dbBean;
	}

	@Override
	public String toString() {
		return "CasBundle [name=" + name + ", recommendations=" + recommendations + ", options=" + options + "]";
	}
}
