package ch.zhaw.dor.rest.beans;

import java.io.Serializable;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class UserProperties implements Serializable {

	private static final long serialVersionUID = 5196091742169399441L;

	/** current job */
	private String profession;
	
	/** experience in current job:
	 *  -1 - not available
	 *   3 - up to 3 years
	 *   7 - up to 7 years
	 *  12 - up to 12 years
	 *  99 - more than 12 years
	 */
	private int professionExperience = -1;
	
	/** interest to study in current job 
	 *  -1 - not interested
	 *   0 - no preference
	 *   1 - very interested 
	 * 
	 */
	private int professionRating = 0;
	
	private String gender;

	/**
	 * @return the profession
	 */
	public String getProfession() {
		return profession;
	}

	/**
	 * @param profession the profession to set
	 */
	public void setProfession(String profession) {
		this.profession = profession;
	}

	/**
	 * @return the professionExperience
	 */
	public int getProfessionExperience() {
		return professionExperience;
	}

	/**
	 * @param professionExperience the professionExperience to set
	 */
	public void setProfessionExperience(int professionExperience) {
		this.professionExperience = professionExperience;
	}

	/**
	 * @return the professionRating
	 */
	public int getProfessionRating() {
		return professionRating;
	}

	/**
	 * @param professionRating the professionRating to set
	 */
	public void setProfessionRating(int professionRating) {
		this.professionRating = professionRating;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "UserProperties [profession=" + profession + ", professionExperience=" + professionExperience
				+ ", professionRating=" + professionRating + ", gender=" + gender + "]";
	}
}
