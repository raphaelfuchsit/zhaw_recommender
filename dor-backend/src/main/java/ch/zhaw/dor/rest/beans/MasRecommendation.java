package ch.zhaw.dor.rest.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import ch.zhaw.dor.db.bean.Mas;
import ls13.productfinder.api.Product;


/**
 * Rest API bean representing a MAS recommendation
 * 
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public class MasRecommendation implements Serializable {
    
	private static final long serialVersionUID = 7610583074118609639L;
	
	private Long id;
	private String name;
	private String description;
	private String urlWeb;
	private String urlIcon;
	private String start;
	private Integer ectsCount;
	
	private long score;
	private long maxScore;
	private long absoluteScore;
	
	/** user-readable texts why this MAS was selected */
	private List<String> explanations;
	
	private CasBundle mandatoryBundle;
	private List<CasBundle> optionBundles;
	private List<Question> userConfigurations;
	
	private transient Product product;
	private transient Mas dbBean;
	
	public static MasRecommendation fromDbBean(Mas dbBean, Product masProduct) {
    	if (dbBean == null) {
    		return null;
    	}
		
		final MasRecommendation masRecommendation = new MasRecommendation();
		masRecommendation.setId(dbBean.getId());
		masRecommendation.setName(dbBean.getName());
		masRecommendation.setDescription(dbBean.getDescription());
		masRecommendation.setUrlWeb(dbBean.getUrlWeb());
		masRecommendation.setUrlIcon(dbBean.getUrlIcon());
		masRecommendation.setStart(dbBean.getStart());
		masRecommendation.setEctsCount(dbBean.getTotalEcts());
		masRecommendation.setDbBean(dbBean);
		masRecommendation.setProduct(masProduct);
		return masRecommendation;
	}
	
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the score
	 */
	public long getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(long score) {
		this.score = score;
	}
	/**
	 * @return the mandatoryBundle
	 */
	public CasBundle getMandatoryBundle() {
		return mandatoryBundle;
	}
	/**
	 * @param mandatoryBundle the mandatoryBundle to set
	 */
	public void setMandatoryBundle(CasBundle mandatoryBundle) {
		this.mandatoryBundle = mandatoryBundle;
	}
	/**
	 * @return the optionBundles
	 */
	public List<CasBundle> getOptionBundles() {
		return optionBundles;
	}
	/**
	 * @param optionBundles the optionBundles to set
	 */
	public void setOptionBundles(List<CasBundle> optionBundles) {
		this.optionBundles = optionBundles;
	}
	/**
	 * @return the urlWeb
	 */
	public String getUrlWeb() {
		return urlWeb;
	}
	/**
	 * @param urlWeb the urlWeb to set
	 */
	public void setUrlWeb(String urlWeb) {
		this.urlWeb = urlWeb;
	}
	/**
	 * @return the urlIcon
	 */
	public String getUrlIcon() {
		return urlIcon;
	}
	/**
	 * @param urlIcon the urlIcon to set
	 */
	public void setUrlIcon(String urlIcon) {
		this.urlIcon = urlIcon;
	}
	/**
	 * @return the explanations
	 */
	public List<String> getExplanations() {
		return explanations;
	}
	/**
	 * @param explanations the explanations to set
	 */
	public void setExplanations(List<String> explanations) {
		this.explanations = explanations;
	} 
	/**
	 * @return the start
	 */
	public String getStart() {
		return start;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(String start) {
		this.start = start;
	}
	/**
	 * @return the ectsCount
	 */
	public Integer getEctsCount() {
		return ectsCount;
	}
	/**
	 * @param ectsCount the ectsCount to set
	 */
	public void setEctsCount(Integer ectsCount) {
		this.ectsCount = ectsCount;
	}
	/**
	 * @return the dbBean
	 */
	public Mas getDbBean() {
		return dbBean;
	}
	/**
	 * @param dbBean the dbBean to set
	 */
	public void setDbBean(Mas dbBean) {
		this.dbBean = dbBean;
	}
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	public long getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(long maxScore) {
		this.maxScore = maxScore;
	}

	public long getAbsoluteScore() {
		return absoluteScore;
	}

	public void setAbsoluteScore(long absoluteScore) {
		this.absoluteScore = absoluteScore;
	}

	@Override
	public String toString() {
		return "MasRecommendation [id=" + id
				+ ", name=" + name
				+ ", score=" + score
				+ ", explanations=" + explanations
				+ ", mandatoryBundle=" + mandatoryBundle
				+ ", optionBundles=" + optionBundles
				+ "]";
	}


	public List<Question> getUserConfigurations() {
		return userConfigurations;
	}

	public void setUserConfigurations(List<Question> userConfigurations) {
		this.userConfigurations = userConfigurations;
	}
}
