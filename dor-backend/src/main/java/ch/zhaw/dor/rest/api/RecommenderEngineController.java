package ch.zhaw.dor.rest.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ch.zhaw.dor.db.bean.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Splitter;
import com.google.common.collect.Streams;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Longs;

import ch.zhaw.dor.EngineConfig.MailConfig;
import ch.zhaw.dor.rest.beans.Cas;
import ch.zhaw.dor.rest.beans.CasBundle;
import ch.zhaw.dor.rest.beans.Question;
import ch.zhaw.dor.rest.beans.InterestForm;
import ch.zhaw.dor.rest.beans.MasRecommendation;
import ch.zhaw.dor.rest.beans.UserContact;
import ch.zhaw.dor.rest.beans.UserProperties;
import ch.zhaw.dor.rest.beans.UserQualification;
import ls13.productfinder.api.Filter;
import ls13.productfinder.api.Product;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.RecommendationResult;
import ls13.productfinder.api.RecommenderEngine;
import ls13.productfinder.api.RecommenderSession;
import ls13.productfinder.beans.InfoText;
import ls13.productfinder.interfaces.UtilityCalculator;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static java.util.stream.Collectors.toList;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@RestController
public class RecommenderEngineController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RecommenderEngineController.class);
	
	private static final String BASE_PATH = "/recommender";
	private static final String TEST_PATH = "/test";
	
	private static final String SESSION_TOKEN = "re-session-token";
	
	private final RecommenderEngine recommenderEngine;
	private final MailSender mailSender;
	
	private final ProfessionRepository professionRepository;
	private final QuestionRepository questionRepository;
	private final MasRepository masRepository;
	private final CasRepository casRepository;
	
	/** tracks active sessions, just for monitoring / debugging information */
	private final Map<String, SessionStore> activeSessions = new LruCache<>(1024);
	private final List<String> infoTexts = new ArrayList<>();

	/** custome page structures to allow different question types */
	private final List<Page> pageStructure = new ArrayList<>();
	private static final String PT_SINGLE_LIKERT = "SINGLE_LIKERT";
	private static final String PT_SINGLE_TEXT_SELECTION = "SINGLE_TEXT_SELECTION";
	private static final String PT_MULTIPLE_PICTURE_SELECTION = "MULTIPLE_PICTURE_SELECTION";

	
	@Autowired
	public RecommenderEngineController(RecommenderEngine recommenderEngine, ProfessionRepository professionRepository,
									   QuestionRepository questionRepository, MasRepository masRepository, CasRepository casRepository, MailSender mailSender) {
		this.recommenderEngine = recommenderEngine;
		this.professionRepository = professionRepository;
		this.questionRepository = questionRepository;
		this.mailSender = mailSender;
		this.masRepository = masRepository;
		this.casRepository = casRepository;
		loadInfoTexts(recommenderEngine);
		loadPageStructure();
	}

	private void loadInfoTexts(RecommenderEngine recommenderEngine) {
		this.infoTexts.clear();
		this.infoTexts.addAll(recommenderEngine.getKnowledgeBase()
				.getInfoTexts().stream()
					.map(InfoText::getName)
					.collect(toList())
				);
	}

	private void loadPageStructure(){
		this.pageStructure.add(new Page(1, new long[] {2,3,5}, PT_MULTIPLE_PICTURE_SELECTION, new String[] {"","",""}));
		this.pageStructure.add(new Page(2, new long[] {1},PT_SINGLE_LIKERT, new String[] {"","",""}));
		this.pageStructure.add(new Page(3, new long[] {4},PT_SINGLE_LIKERT, new String[] {"","",""}));
		this.pageStructure.add(new Page(4, new long[] {6},PT_SINGLE_LIKERT, new String[] {"","",""}));
		this.pageStructure.add(new Page(5, new long[] {7},PT_SINGLE_LIKERT, new String[] {"","",""}));
		this.pageStructure.add(new Page(6, new long[] {8},PT_SINGLE_LIKERT, new String[] {"","",""}));
		this.pageStructure.add(new Page(7, new long[] {9},PT_SINGLE_TEXT_SELECTION, new String[] {"","",""}));
		this.pageStructure.add(new Page(8, new long[] {10},PT_SINGLE_LIKERT, new String[] {"","",""}));
		this.pageStructure.add(new Page(9, new long[] {11},PT_SINGLE_LIKERT, new String[] {"","",""}));
		this.pageStructure.add(new Page(10, new long[] {12},PT_SINGLE_LIKERT, new String[] {"","",""}));

	}
	
    @GetMapping("/status")
    public String status() {
        return new StringBuilder()
        		.append(recommenderEngine)
        		.toString();
    }
    
    @GetMapping("/reload")
    public String reload() throws ProductFinderException {
    	recommenderEngine.loadModel();
    	loadInfoTexts(recommenderEngine);
    	activeSessions.values().stream()
    		.forEach(s -> s.recommenderSession.clearInput());
    	return "reloaded";
    }

    @GetMapping("/showSession")
    public String showSessions(@RequestParam(value = "id", required=false) String id) {
    	final Stream<SessionStore> stream = Optional.ofNullable(id)
    			.map(activeSessions::get)
    			.map(Stream::of)
    			// default return all sessions
    			.orElse(activeSessions.values().stream());
    	
    	return stream.map(Objects::toString)
    			.collect(Collectors.joining(","));
    }
    
    @GetMapping(TEST_PATH+"/products")
    public String getProducts() {
    	return recommenderEngine.getProductCatalog().getProducts().stream()
    			.map(Objects::toString)
				.collect(Collectors.joining("\n"));
    }
    
    @GetMapping(TEST_PATH+"/rules")
    public String getRules() {
    	return ToStringBuilder.reflectionToString(recommenderEngine.getKnowledgeBase(), ToStringStyle.MULTI_LINE_STYLE);
    }

    @GetMapping(TEST_PATH+"/professions")
    public String testProfessions() {
		return Streams.stream(professionRepository.findAll())
    			.sorted(Comparator.comparing(ch.zhaw.dor.db.bean.Profession::getId))
    			.map(Objects::toString)
				.collect(Collectors.joining("\n"));
    }
	
    @GetMapping(TEST_PATH+"/interests")
    public String testInterests() {
		return Streams.stream(questionRepository.findAll())
				.sorted(Comparator.comparing(ch.zhaw.dor.db.bean.Question::getId))
    			.map(Objects::toString)
				.collect(Collectors.joining("\n"));
    }
	
    @GetMapping(TEST_PATH+"/mas")
    public String testMas() {
		return Streams.stream(masRepository.findAll())
				.sorted(Comparator.comparing(ch.zhaw.dor.db.bean.Mas::getId))
    			.map(Objects::toString)
				.collect(Collectors.joining("\n"));
    }
	
	@GetMapping(TEST_PATH+"/cas")
    public String testCas() {
		return Streams.stream(casRepository.findAll())
				.sorted(Comparator.comparing(ch.zhaw.dor.db.bean.Cas::getId))
    			.map(Objects::toString)
				.collect(Collectors.joining("\n"));
    }
	
    /**
     * Calculates result using the given properties
     * 
     * @param propertiesMap
     * @param sessionToken
     * @throws ProductFinderException 
     */
    @PostMapping(TEST_PATH+"/result")
	public String testResult(@RequestBody Map<String, Object> propertiesMap) throws ProductFinderException {

    	LOGGER.debug("Setting user requirements: {}", propertiesMap);
    	
    	final MasRecommendation masRecommendation = makeRecommendation(recommenderEngine.newRecommenderSession(), propertiesMap, null);
    	
    	return ToStringBuilder.reflectionToString(masRecommendation, ToStringStyle.MULTI_LINE_STYLE);
	}


    
    
    
    
    
    /**
     * Get the session store for the given session token
     * 
     * @param sessionToken
     * @return session store
     * @throws ProductFinderException 
     */
    private SessionStore getSessionStore(String sessionToken) throws ProductFinderException {
    	LOGGER.debug("Loading session for token: {}", sessionToken);
		return Optional.ofNullable(activeSessions.get(sessionToken))
				.orElseThrow(() -> new ProductFinderException(String.format("Invalid session token, check request header: '%s', value: %s", SESSION_TOKEN, sessionToken)));
	}
    
    /**
     * @return session store for a new session
     */
    public SessionStore create() {
    	final SessionStore sessionStore = new SessionStore(recommenderEngine);
    	activeSessions.put(Long.toString(sessionStore.id), sessionStore);
    	
    	LOGGER.debug("Created session store (id:{})", sessionStore.id);
    	
		return sessionStore;
    }
    
    /**
     * Creates a new recommender session
     * 
     * @return session token
     */
    @GetMapping(BASE_PATH+"/createSession")
    public String createSession() {
    	final SessionStore sessionStore = new SessionStore(recommenderEngine);
    	final String sessionToken = Long.toString(sessionStore.id);
		activeSessions.put(sessionToken, sessionStore);
    	
    	LOGGER.debug("Created session store (id:{})", sessionStore.id);
    	
		return sessionToken;
    }
    
    /**
     * Get list of professions
     * 
     * @return sorted list of professions
     */
    @GetMapping(BASE_PATH+"/professions")
    public List<String> getProfessions() {
    	return Streams.stream(professionRepository.findAll())
    			.map(Profession::getName)
    			.sorted()
    			.collect(toList());
    }
    
    /**
     * Initializes a recommender session using the given user properties
     * 
     * @param userProperties
     * @param sessionToken
     * @throws ProductFinderException 
     */
    @PostMapping(BASE_PATH+"/initSession")
	public void initSession(@RequestBody UserProperties userProperties, @RequestHeader(name=SESSION_TOKEN, required=false) String sessionToken) throws ProductFinderException {

    	final SessionStore sessionStore = getSessionStore(sessionToken);
    	synchronized(sessionStore) {
    		
			LOGGER.debug("Init session store (id:{}) using {}", sessionStore.id, userProperties);
			
			sessionStore.userProperties = userProperties;
    	}	
	}
    
    /**
     * Get list of interests
     * 
     * @param page
     * @param size
     * @param sessionToken
     * @return interest form
     * @throws Exception
     */
    @GetMapping(BASE_PATH+"/questions")
	public InterestForm getQuestions(@RequestParam(value = "page", defaultValue = "-1") int page,
			@RequestParam(value = "size", defaultValue = "-1") int size,
			@RequestHeader(name=SESSION_TOKEN, required=false) String sessionToken) throws Exception {
        
    	final SessionStore sessionStore = getSessionStore(sessionToken);
    	synchronized(sessionStore) {
    		
	    	LOGGER.debug("Get questions using page: {}, size: {}, session: {}", page, size, sessionStore);


	    	// 1. Return existing interesetForm (page) if already exists
	    	if (page >= 0) {
	    		// check if there was already interest for this page returned to support navigation in UI
	    		final InterestForm interestForm = sessionStore.interestForms.get(page);
	    		if (interestForm != null) {
	    			LOGGER.debug("Returning questions from session for page: {}, {}", page, interestForm);
	    			return interestForm;
	    		}
	    	}

	    	// 2. Define new interesetForm (page)
	    	final List<Question> questions = Streams.stream(questionRepository.findAll())
	    			// duplicate check for new questions
	    			.filter(e -> !sessionStore.questions.containsKey(e.getId()))
	    			.map(Question::fromDbBean)
					.collect(toList());

	    	
	    	// calculate default values
	    	if (page == -1) {
	    		page = sessionStore.interestForms.size() + 1;
	    	}

	    	final int pageId = page;
	    	Page actualPageStructure = this.pageStructure.stream()
					.filter(ps -> ps.getId() == pageId)
					.findFirst()
					.orElse(null);
			final List<Question> pageDimensions = new ArrayList<>();
	    	for (long dimensionId : actualPageStructure.getDimensions()){
				Question actualQuestion = questions.stream()
						.filter(question -> question.getId() == dimensionId)
						.findAny()
						.orElse(null);
				pageDimensions.add(actualQuestion);
				sessionStore.questions.put(actualQuestion.getId(), actualQuestion);
			}

	    	final InterestForm interestForm = new InterestForm();
	    	sessionStore.interestForms.put(page, interestForm);
	    	
	    	interestForm.setPage(page);
	    	interestForm.setHasResult(sessionStore.interestForms.size() >= 2);
	    	interestForm.setLastInterests(this.pageStructure.size() <= page);
	    	interestForm.setInterests(pageDimensions);
			interestForm.setQuestionType(actualPageStructure.getQuestionType());
	    	interestForm.setQuestionOptions(actualPageStructure.getQuestionOptions());

	    	LOGGER.debug("Generated questions form. session: {}, form: {}", sessionStore.id, interestForm);
	    	
	    	return interestForm;
    	}	
	}
    
    /**
     * Update session with interest ratings
     * 
     * @param interests
     * @param sessionToken
     * @throws ProductFinderException 
     */
    @PostMapping(BASE_PATH+"/updateSession")
	public void updateSession(@RequestBody List<Question> interests, @RequestHeader(name=SESSION_TOKEN, required=false) String sessionToken) throws ProductFinderException {
    	
    	final SessionStore sessionStore = getSessionStore(sessionToken);
    	synchronized(sessionStore) {
    		
			LOGGER.debug("Updating session store (id:{}) using interests: {}", sessionStore.id, interests);
			
			for (Question interest : interests) {
				if (interest == null) {
					continue;
				}
				
				sessionStore.questions.computeIfPresent(interest.getId(), (id, sessionInterest) -> {
					sessionInterest.setRating(interest.getRating());
					return sessionInterest;
				});
			}
    	}	
	}
    
    /**
     * Get the recommendation result
     * 
     * @param sessionToken
     * @return result
     * @throws ProductFinderException 
     */
    @GetMapping(BASE_PATH+"/result")
	public MasRecommendation getResult(@RequestHeader(name=SESSION_TOKEN, required=false) String sessionToken) throws ProductFinderException {
        
    	final SessionStore sessionStore = getSessionStore(sessionToken);
    	synchronized(sessionStore) {
    		
	    	LOGGER.debug("Calculating recommendation for session: {}", sessionStore);
	    	
	    	// set user requirements in recommender session
	    	final Map<String, Object> userInputs = new LinkedHashMap<>();
//	    	if (sessionStore.userProperties != null) {
//	    		// profession properties
//	    		userInputs.put(EngineConstants.C_PREF_PROFESSION, sessionStore.userProperties.getProfession());
//	    		userInputs.put(EngineConstants.C_PREF_PROFESSION_RATING, sessionStore.userProperties.getProfessionRating());
//	    		userInputs.put(EngineConstants.C_PREF_PROFESSION_EXPERIENCE, sessionStore.userProperties.getProfessionExperience());
//
//	    		userInputs.put(EngineConstants.C_PREF_GENDER, sessionStore.userProperties.getGender());
//	    	}
	    	
	    	// interest properties, mapping of user interest rating:
	    	//
	    	//         user input       |    engine input    
	    	// -------------------------+-----------------------
	    	//	  -1 - default value    |   * - ignore interest
	    	//	   0 - not interested   |  -2 - very low
	    	//	   1 - low              |  -1 - low
	    	//	   2 - medium           |   0 - neutral
	    	//	   3 - high             |   1 - high
	    	//	   4 - very high        |   2 - very high
	    	//
//	    	final Map<String, Integer> interestsLiked = new HashMap<>();
//	    	final Map<String, Integer> interestsDisliked = new HashMap<>();
//	    	sessionStore.questions.values().stream()
//	    		.filter(e -> e.getRating() >= 0) // use only interests with user rating
//	    		.forEach(e -> {
//	    			if (e.calculateInternalRating() > 0) { // only positive rating
//	    				interestsLiked.put(e.getDimension(), e.calculateInternalRating());
//	    			}
//	    			if (e.calculateInternalRating() < 0) { // only negative rating
//	    				interestsDisliked.put(e.getDimension(), e.calculateInternalRating());
//	    			}
//	    		});
//
//	    	if (!interestsLiked.isEmpty()) {
//	    		userInputs.put(EngineConstants.C_PREF_INTERESTS_LIKED, interestsLiked);
//	    	}
//	    	if (!interestsDisliked.isEmpty()) {
//	    		userInputs.put(EngineConstants.C_PREF_INTERESTS_DISLIKED, interestsDisliked);
//	    	}

			sessionStore.questions.values().stream()
	    		.forEach(e -> {
					userInputs.put(e.getFilterParameter(), e.getRating());
	    		});

	    	
	    	final MasRecommendation result = makeRecommendation(sessionStore.recommenderSession, userInputs, sessionStore.questions.values().stream().collect(toList()));
	    	
	    	LOGGER.debug("Calculated MAS recommendation for session: {}, {}", sessionStore, result);
	    	
	    	sessionStore.result = result;
	    	
	    	return result;
    	}	
	}

    /**
     * Calculates the MAS recommendation
     * 
     * @param recommenderSession
     * @param userInputs
     * @return MAS recommendation
     * @throws ProductFinderException
     */
    private MasRecommendation makeRecommendation(RecommenderSession recommenderSession, Map<String, Object> userInputs, List<Question> questions) throws ProductFinderException {
        
    	// set user inputs
    	recommenderSession.clearInput();
    	for (Entry<String, Object> entry : userInputs.entrySet()) {
    		recommenderSession.setInput(entry.getKey(), entry.getValue());
		}
	    	
    	// we use a two-step approach to get recommendation result:
    	//    1. Calculate best matching MAS using c_pref_type = 'MAS'
    	//    2. Calculate best matching CAS selections for all CAS bundle except 'Pflicht CAS' (must be always selected) 
    	//       using c_pref_type = 'CAS' and c_pref_ids of CAS options for this MAS 
	    	
    	// MAS selection
    	recommenderSession.setInput(EngineConstants.C_PREF_TYPE, EngineConstants.PRODUCT_TYPE_MAS);
	    	
    	LOGGER.debug("Calculating MAS...");
	    	
    	final RecommendationResult masResult = recommenderSession.computeRecommendationResult(1);
    	final Product masProduct = masResult.getBestProduct();
	    
    	LOGGER.debug("Engine selected MAS: {}", masProduct);

    	final long masId = getIdOfProduct(masProduct);
    	final ch.zhaw.dor.db.bean.Mas mas = masRepository.findById(masId)
				.orElseThrow(() -> new ProductFinderException(String.format("Failed to load MAS entry from DB using ID: %s", masId)));

		final ch.zhaw.dor.db.bean.CasBundle mandatoryBundle = mas.getCasBundles().stream()
				.filter(e -> e.getMandatoryBundle() != null && e.getMandatoryBundle() == 1)
				.findFirst()
				.orElse(null);
		
    	// generate explanation...
		final List<String> masExplanation = createFilterExplanation(masProduct, masResult, recommenderSession);
		
    	// get IDs of CAS options 
		final Set<Long> casIds = mas.getCasBundles().stream()
				.flatMap(e -> e.getCasOptions().stream())
				.map(e -> e.getId())
				.collect(Collectors.toSet());
			
		// CAS selection
    	recommenderSession.setInput(EngineConstants.C_PREF_TYPE, EngineConstants.PRODUCT_TYPE_CAS);
    	recommenderSession.setInput(EngineConstants.C_PREF_IDS, casIds.toArray(new Long[1]));
    	recommenderSession.setInput(EngineConstants.C_PREF_MAS_ID, masId);
	
    	LOGGER.debug("Calculating CAS options using CAS-IDs: {}", casIds);
    	
    	// search for CASs without relaxation
    	final RecommendationResult firstResult = recommenderSession.computeRecommendationResult(-1);
    	
    	LOGGER.debug("Engine selected CAS options - no relaxation: {}", firstResult);
    	
    	final Map<Long, CasResult> casMap = new LinkedHashMap<>();
    	final Map<Long, IndexEntry> casIndex = createCasIndex(mas);
		for (Product casProduct : firstResult.getMatchingProducts()) {
			final long id = getIdOfProduct(casProduct);
			
			final IndexEntry indexEntry = casIndex.get(id);
			if (indexEntry == null) {	// should never happen...
				throw new ProductFinderException(String.format("Failed to map product to CAS entry. product: %s", casProduct));
			}
			
			LOGGER.debug("Engine selected CAS: {}", casProduct);
			
			final Cas cas = indexEntry.cas;
			cas.setScore(getUtilityScore(casProduct));
			cas.setExplanations(createFilterExplanation(casProduct, firstResult, recommenderSession));
			cas.setProduct(casProduct);
			
			casMap.put(id, new CasResult(id, cas, indexEntry.casBundle));
			casIds.remove(id);
		}    	
    	
		LOGGER.debug("Processing remaining CAS-IDs: {} with filter relaxation", casIds);
		
		for (Long id : casIds) {
			
			recommenderSession.setInput(EngineConstants.C_PREF_IDS, new long[] { id });
			
			final RecommendationResult result = recommenderSession.computeRecommendationResult(1);
			if (result.getMatchingProducts().isEmpty()) {
				continue;
			}
			
			final Product casProduct = result.getBestProduct();
			final long casId = getIdOfProduct(casProduct);
			if (casMap.containsKey(casId)) {
				continue;
			}
			
			final IndexEntry indexEntry = casIndex.get(id);
			if (indexEntry == null) {	// should never happen...
				throw new ProductFinderException(String.format("Failed to map product to CAS entry. product: %s", casProduct));
			}
			
			LOGGER.debug("Engine selected CAS: {}", casProduct);
			
			final Cas cas = indexEntry.cas;
			cas.setScore(getUtilityScore(casProduct));
			cas.setExplanations(createFilterExplanation(casProduct, result, recommenderSession));
			
			casMap.put(id, new CasResult(id, cas, indexEntry.casBundle));
		}
		
		final Comparator<CasResult> comparator = Comparator.comparing(e -> e.cas.getScore());
		final List<CasResult> casResults = casMap.values().stream()
				.filter(e -> e.casBundle.getDbBean().getMandatoryBundle() != null && e.casBundle.getDbBean().getMandatoryBundle() != 1)
				.sorted(comparator.reversed())
				.collect(toList());
		
		// process CAS recommendations
		final Map<CasBundle, Map<Long, Cas>> bundleRecommendactions = new HashMap<>();
		// 1. select CASs for each bundle to fit min CAS count
		// 2. select CASs for each bundle until max CAS count is exceeded 
    	// optional CAS count = total count - mandatory count
    	final int optionalCasCount = Optional.ofNullable(mas.getCasCount()).orElse(4) - Optional.ofNullable(mandatoryBundle.getMinCas()).orElse(2);
		long casCount = processRecommendedCasOptions(bundleRecommendactions, optionalCasCount, casResults, true);
		casCount = processRecommendedCasOptions(bundleRecommendactions, optionalCasCount, casResults, false);
			
		if (casCount < optionalCasCount) {
			LOGGER.debug("Need to force CAS options to meet total CAS count: {} of MAS. current count: {}", optionalCasCount, casCount);
			casCount = processForcedCasOptions(bundleRecommendactions, optionalCasCount, true);
			casCount = processForcedCasOptions(bundleRecommendactions, optionalCasCount, false);
		}
		
		final MasRecommendation result = MasRecommendation.fromDbBean(mas, masProduct);
		result.setMandatoryBundle(createMandatoryCasBundle(mandatoryBundle, casMap.values().stream()
				.filter(e -> e.casBundle.getDbBean().getMandatoryBundle() != null && e.casBundle.getDbBean().getMandatoryBundle() == 1)
				.sorted(comparator.reversed())
				.collect(toList())));
		result.setOptionBundles(createOptionalCasBundles(bundleRecommendactions, casResults));
    	result.setExplanations(masExplanation);
    	result.setScore(getUtilityScore(masProduct));
    	result.setUserConfigurations(questions);
		
    	LOGGER.debug("Calculated MAS recommendation: {}", result);
	    	
    	return result;
	}

	private Map<Long, IndexEntry> createCasIndex(Mas mas) {
		
		final Map<Long, IndexEntry> index = new HashMap<>();
		final Map<Long, CasBundle> duplicateCheck = new HashMap<>();
		for (ch.zhaw.dor.db.bean.CasBundle casBundleBean : mas.getCasBundles()) {
			for (ch.zhaw.dor.db.bean.Cas casBean : casBundleBean.getCasOptions()) {
				// note: create only one casBundle object for each ID, required for further procession!
				index.put(casBean.getId(), new IndexEntry(Cas.fromDbBean(casBean), 
						duplicateCheck.computeIfAbsent(casBundleBean.getId(), k -> CasBundle.fromDbBean(casBundleBean))));
			}
		}
		return index;
	}

	private List<String> createFilterExplanation(Product product, RecommendationResult result, RecommenderSession recommenderSession) throws ProductFinderException {
		
		final List<String> list = new ArrayList<>();
		for (String infoText : infoTexts) {
			list.add(recommenderSession.getInfoText(infoText, product));
		}
		
		for (Filter filter : result.getAppliedFilters()) {
			if (filter.getPriority() > 0) {	// ignore forced filters
				list.add(filter.getExplanation(product));
			}
		}
		
		for (Filter filter : result.getRelaxedFilters()) {
			if (filter.getPriority() > 0) {	// ignore forced filters
				list.add(filter.getExcuse(product));
			}
		}
		
		return list.stream()
				.map(StringUtils::trimToNull)
				.filter(Objects::nonNull)
				.distinct()
				.collect(toList());
	}

	private CasBundle createMandatoryCasBundle(ch.zhaw.dor.db.bean.CasBundle mandatoryBundle, List<CasResult> list) {
		
		final CasBundle casBundle = CasBundle.fromDbBean(mandatoryBundle);
		casBundle.setRecommendations(list.stream()
				.map(e -> e.cas)
				.map(e -> e.withScore(e.getScore()))
				.collect(toList()));
		return casBundle;
	}

	private List<CasBundle> createOptionalCasBundles(Map<CasBundle, Map<Long, Cas>> bundleRecommendactions, Collection<CasResult> casResults) {
		
		final List<CasBundle> casBundles = new ArrayList<>();
		for (Entry<CasBundle, Map<Long, Cas>> entry : bundleRecommendactions.entrySet()) {
			final CasBundle casBundle = entry.getKey();
			final Map<Long, Cas> casMap = entry.getValue();
			
			casBundle.setRecommendations(casMap.values().stream().collect(toList()));
			
			final Map<Long, Cas> options = new LinkedHashMap<>();
			for (CasResult result : casResults) {
				if (casBundle.equals(result.casBundle) && !casMap.containsKey(result.id) && !options.containsKey(result.id)) {
					options.put(result.id, result.cas);
					LOGGER.debug("Selected optional CAS({}): '{}' for bundle: '{}'", result.cas.getId(), result.cas.getName(), result.casBundle.getName());
				}
			}
			for (ch.zhaw.dor.db.bean.Cas casBean : casBundle.getDbBean().getCasOptions()) {
				if (!casMap.containsKey(casBean.getId()) && !options.containsKey(casBean.getId())) {
					options.put(casBean.getId(), Cas.fromDbBean(casBean ));
					LOGGER.debug("Added optional CAS({}): '{}' for bundle: '{}' from DB entries.", casBean.getId(), casBean.getName(), casBundle.getName());
				}
			}
			
			casBundle.setOptions(options.values().stream().collect(toList()));
			
			casBundles.add(casBundle);
		}
		
		return casBundles;
	}

	private long processForcedCasOptions(Map<CasBundle, Map<Long, Cas>> bundleRecommendactions, int maxCasCount,
			boolean processMinCas) {
		
		long casCount = bundleRecommendactions.values().stream()
				.flatMap(e -> e.keySet().stream())
				.count();
		
		for (CasBundle casBundle : new HashSet<>(bundleRecommendactions.keySet())) {
			if (casCount == maxCasCount) {
				break;
			}
			
			// force CAS selection
			final Map<Long, Cas> casMap = bundleRecommendactions.computeIfAbsent(casBundle, e -> new LinkedHashMap<>());
			for (ch.zhaw.dor.db.bean.Cas casBean : casBundle.getDbBean().getCasOptions()) {
				if (processMinCas && 
						casMap.size() >= casBundle.getDbBean().getMinCas()) {
					continue;
				}
				if (!processMinCas && 
						casMap.size() >= casBundle.getDbBean().getMaxCas()) {
					continue;
				}
				
				if (casMap.putIfAbsent(casBean.getId(), Cas.fromDbBean(casBean)) == null) {
					casCount++;
					LOGGER.debug("Forced CAS({}): {} for bundle: {}", casBean.getId(), casBean.getName(), casBundle.getName());
				}
			}
		}
		
		return casCount;
	}
    
	private long processRecommendedCasOptions(Map<CasBundle, Map<Long, Cas>> bundleRecommendactions, int maxCasCount,
			Collection<CasResult> casResults, boolean processMinCas) {
		
		long casCount = bundleRecommendactions.values().stream()
				.flatMap(e -> e.keySet().stream())
				.count();
		
		for (CasResult result : casResults) {
			if (casCount == maxCasCount) {
				break;
			}
			
			final Map<Long, Cas> casMap = bundleRecommendactions.computeIfAbsent(result.casBundle, e -> new LinkedHashMap<>());
			if (processMinCas && 
					casMap.size() >= result.casBundle.getDbBean().getMinCas()) {
				continue;
			}
			if (!processMinCas && 
					casMap.size() >= result.casBundle.getDbBean().getMaxCas()) {
				continue;
			}
			
			if (casMap.putIfAbsent(result.id, result.cas) == null) {
				casCount++;
				LOGGER.debug("Selected CAS({}): '{}' for bundle: '{}'", result.cas.getId(), result.cas.getName(), result.casBundle.getName());
			}
		}
		return casCount;
	}

	private long getUtilityScore(final Product product) {
		return Optional.ofNullable(product)
				.map(p -> p.get(UtilityCalculator.UTILITY_DOMINATOR_VAR.getName()))
				.map(Objects::toString)
				.map(Doubles::tryParse)
				.map(Double::longValue)
				.orElse(50L);
	}

    /**
     * 
     * @param product
     * @return product ID
     * @throws ProductFinderException
     */
	private long getIdOfProduct(final Product product) throws ProductFinderException {
		return Optional.ofNullable(product)
    			.map(e -> e.get(EngineConstants.P_ID))
    			.map(Object::toString)
    			.map(Longs::tryParse)
    			.orElseThrow(() -> new ProductFinderException(String.format("Failed to get ID of product: %s", product)));
	}
    
    /**
     * Update session with user qualification
     * 
     * @param userQualification
     * @param sessionToken
     * @throws ProductFinderException 
     */
    @PostMapping(BASE_PATH+"/updateQualification")
	public void updateQualification(@RequestBody UserQualification userQualification, @RequestHeader(name=SESSION_TOKEN, required=false) String sessionToken) throws ProductFinderException {
    	
    	final SessionStore sessionStore = getSessionStore(sessionToken);
    	synchronized(sessionStore) {
    		
			LOGGER.debug("Updating session store (id:{}) using qualification: {}", sessionStore.id, userQualification);
			
			sessionStore.userQualification = userQualification;
    	}	
	}
    
    /**
     * Submits the user information
     * 
     * @param userContact
     * @param sessionToken
     * @throws ProductFinderException 
     */
    @PostMapping(BASE_PATH+"/submit")
	public void submitResult(@RequestBody UserContact userContact, @RequestHeader(name=SESSION_TOKEN, required=false) String sessionToken) throws ProductFinderException {
    	
    	final SessionStore sessionStore = getSessionStore(sessionToken);
    	synchronized(sessionStore) {
			LOGGER.debug("Submitting result for session store (id:{}) using contact: {}", sessionStore.id, userContact);
			sessionStore.userContact = userContact;
			mailSender.sendHtml(null, userContact.toEmailString());
    	}	
	}    

    
    
    
    
    /**
     * 
     * @author cst
     *
     * @param <K>
     * @param <V>
     */
    private static class LruCache<K, V> extends LinkedHashMap<K, V> {

		private static final long serialVersionUID = -1613640676770764993L;

		private final int limit;
		
		public LruCache(int limit) {
			this.limit = Math.max(1, limit);
		}

		@Override
		protected boolean removeEldestEntry(Entry<K, V> eldest) {
			if (size() > limit) {
				LOGGER.debug("Closing session: {}", eldest.getValue());
				return true;
			}	
			return false;
		}
    }
    
    /**
     * Container to store user session properties
     * 
     * @author cst
     *
     */
    private static class SessionStore {
    	
    	private static final AtomicLong ID_GEN = new AtomicLong();
    	
    	private final long id;
		private final RecommenderSession recommenderSession;
		private final Map<Long, Question> questions = new LinkedHashMap<>();
		private final Map<Integer, InterestForm> interestForms = new LinkedHashMap<>();
		
		private UserProperties userProperties;
		private UserQualification userQualification;
		private UserContact userContact;
		private MasRecommendation result;
    	
		/**
		 * 
		 * @param recommenderEngine
		 */
    	public SessionStore(RecommenderEngine recommenderEngine) {
    		this.id = ID_GEN.incrementAndGet();
			this.recommenderSession = recommenderEngine.newRecommenderSession();
		}
    	
		@Override
		public String toString() {
			return "SessionStore [id=" + id 
					+ ", userProperties=" + userProperties 
					+ ", userQualification=" + userQualification 
					+ ", userContact=" + userContact
					+ ", interestForms=" + interestForms 
					+ ", recommenderSession=" + recommenderSession
					+ ", result=" + result
					+ "]";
		}
    }
    
    public static class MailSender {
    	
    	private final JavaMailSender javaMailSender;
    	private final MailConfig mailConfig;
    	private final String[] sendTo;
    	
    	public MailSender(JavaMailSender javaMailSender, MailConfig mailConfig) {
    		this.javaMailSender = javaMailSender;
    		this.mailConfig = mailConfig;
    		this.sendTo = Splitter.on(Pattern.compile("[,; ]+")).splitToList(mailConfig.getSendTo()).toArray(new String[1]);
    	}
    	
    	public void send(String subject, String text) {
    		
    		final SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo(sendTo);

            msg.setSubject(mailConfig.getSubject());
            msg.setText(mailConfig.getText() + text);

            LOGGER.debug("Sending mail: {}", msg);
            
            javaMailSender.send(msg);
    	}

    	public void sendHtml(String subject, String html){
    		try{
				MimeMessage mimeMessage = javaMailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
				helper.setText(html, true);
				helper.setTo(sendTo);
				helper.setSubject(mailConfig.getSubject());
				javaMailSender.send(mimeMessage);
			}   catch (MessagingException ex) {
				LOGGER.error("Sending mail: {}", ex);
			}
		}
    }
    
    private static class CasResult {
		private final long id;
    	private final Cas cas;
    	private final CasBundle casBundle;
    	
    	public CasResult(long id, Cas cas, CasBundle casBundle) {
			this.id = id;
			this.cas = cas;
			this.casBundle = casBundle;
		}    	
    }
    
    private static class IndexEntry {
    	private final Cas cas;
    	private final CasBundle casBundle;
    	
    	public IndexEntry(Cas cas, CasBundle casBundle) {
			this.cas = cas;
			this.casBundle = casBundle;
		}
    }
}
