package ch.zhaw.dor.rest.api;

import java.security.SecureRandom;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * <p>
 * Copyright: Copyright (c) 2019
 * </p>
 * 
 * @author streuch1
 * @version 1.0
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		// enable basic authentication for all requests
		http.csrf().disable()
			.authorizeRequests().anyRequest().authenticated()
			.and()
			.httpBasic();
		
		// required for H2 console
		http.headers().frameOptions().disable();
		
		// The cors() method will add the Spring-provided CorsFilter 
		// to the application context which in turn bypasses the 
		// authorization checks for OPTIONS requests.
		http.cors();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		// curl -H "Authorization: Basic ZG9yLWFkbWluOiVSWzl3NzU2Vi4zUjJqdFE=" ...
		
		final PasswordEncoder encoder = new BCryptPasswordEncoder(4, new SecureRandom());
        auth.inMemoryAuthentication()
        	.passwordEncoder(encoder)
	        .withUser("dor-admin")
	        .password(encoder.encode("%R[9w756V.3R2jtQ"))
	        .roles("USER");
	}
}