package ch.zhaw.dor.db.bean;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@Entity
@Table(name = "CAS_BUNDLE", schema = "RE_ENGINE")
public class CasBundle {

	@Id
	private long id;

	private long masId;
	private String name;
	/** minimum CAS count */
	private Integer minCas;
	/** maximum CAS count */
	private Integer maxCas;
	private String description;
	/** Set 1 for mandatory bundles: 'Pflicht CAS' */
	private Integer mandatoryBundle;
	
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "cas_to_bundle", 
      joinColumns = @JoinColumn(name = "bundle_id", referencedColumnName="id"),
      inverseJoinColumns = @JoinColumn(name = "cas_id", referencedColumnName="id"))
    private Set<Cas> casOptions;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the masId
	 */
	public long getMasId() {
		return masId;
	}
	/**
	 * @param masId the masId to set
	 */
	public void setMasId(long masId) {
		this.masId = masId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the maxCas
	 */
	public Integer getMaxCas() {
		return maxCas;
	}
	/**
	 * @param maxCas the maxCas to set
	 */
	public void setMaxCas(Integer maxCas) {
		this.maxCas = maxCas;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the casOptions
	 */
	public Set<Cas> getCasOptions() {
		return casOptions;
	}
	/**
	 * @param casOptions the casOptions to set
	 */
	public void setCasOptions(Set<Cas> casOptions) {
		this.casOptions = casOptions;
	}
	/**
	 * @return the mandatoryBundle
	 */
	public Integer getMandatoryBundle() {
		return mandatoryBundle;
	}
	/**
	 * @param mandatoryBundle the mandatoryBundle to set
	 */
	public void setMandatoryBundle(Integer mandatoryBundle) {
		this.mandatoryBundle = mandatoryBundle;
	}
	/**
	 * @return the minCas
	 */
	public Integer getMinCas() {
		return minCas;
	}
	/**
	 * @param minCas the minCas to set
	 */
	public void setMinCas(Integer minCas) {
		this.minCas = minCas;
	}

	@Override
	public String toString() {
		return "CasBundle [id=" + id 
				+ ", name=" + name + ", masId=" + masId 
				+ ", minCas=" + minCas
				+ ", maxCas=" + maxCas
				+ ", mandatoryBundle=" + mandatoryBundle 
				+ ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CasBundle other = (CasBundle) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
