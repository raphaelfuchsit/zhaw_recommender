package ch.zhaw.dor.db.bean;

import org.springframework.data.repository.CrudRepository;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
public interface MasRepository extends CrudRepository<Mas, Long> {

}
