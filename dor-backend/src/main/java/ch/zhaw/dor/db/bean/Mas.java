package ch.zhaw.dor.db.bean;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@Entity
@Table(name = "MAS", schema = "RE_ENGINE")
public class Mas {

	@Id
	private long id;

	private String name;
	private String description;
	private Integer duration;
	private Double costs;
	private Integer totalEcts;
	private Integer casCount;
	private Integer masterThesis;
	private Integer promoPriority;
	private String urlWeb;
	private String urlIcon;
	private String start;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "cas_bundle", 
	    joinColumns = @JoinColumn(name = "mas_id", referencedColumnName="id"),
	    inverseJoinColumns = @JoinColumn(name = "id", referencedColumnName="id"))
    private Set<CasBundle> casBundles;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the duration
	 */
	public Integer getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	/**
	 * @return the costs
	 */
	public Double getCosts() {
		return costs;
	}
	/**
	 * @param costs the costs to set
	 */
	public void setCosts(Double costs) {
		this.costs = costs;
	}
	/**
	 * @return the totalEcts
	 */
	public Integer getTotalEcts() {
		return totalEcts;
	}
	/**
	 * @param totalEcts the totalEcts to set
	 */
	public void setTotalEcts(Integer totalEcts) {
		this.totalEcts = totalEcts;
	}
	/**
	 * @return the casCount
	 */
	public Integer getCasCount() {
		return casCount;
	}
	/**
	 * @param casCount the casCount to set
	 */
	public void setCasCount(Integer casCount) {
		this.casCount = casCount;
	}
	/**
	 * @return the masterThesis
	 */
	public Integer getMasterThesis() {
		return masterThesis;
	}
	/**
	 * @param masterThesis the masterThesis to set
	 */
	public void setMasterThesis(Integer masterThesis) {
		this.masterThesis = masterThesis;
	}
	/**
	 * @return the promoPriority
	 */
	public Integer getPromoPriority() {
		return promoPriority;
	}
	/**
	 * @param promoPriority the promoPriority to set
	 */
	public void setPromoPriority(Integer promoPriority) {
		this.promoPriority = promoPriority;
	}
	/**
	 * @return the urlWeb
	 */
	public String getUrlWeb() {
		return urlWeb;
	}
	/**
	 * @param urlWeb the urlWeb to set
	 */
	public void setUrlWeb(String urlWeb) {
		this.urlWeb = urlWeb;
	}
	/**
	 * @return the urlIcon
	 */
	public String getUrlIcon() {
		return urlIcon;
	}
	/**
	 * @param urlIcon the urlIcon to set
	 */
	public void setUrlIcon(String urlIcon) {
		this.urlIcon = urlIcon;
	}
	/**
	 * @return the start
	 */
	public String getStart() {
		return start;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(String start) {
		this.start = start;
	}
	/**
	 * @return the casBundles
	 */
	public Set<CasBundle> getCasBundles() {
		return casBundles;
	}
	/**
	 * @param casBundles the casBundles to set
	 */
	public void setCasBundles(Set<CasBundle> casBundles) {
		this.casBundles = casBundles;
	}
	
	@Override
	public String toString() {
		return "Mas [id=" + id + ", name=" + name + ", duration=" + duration + ", costs=" + costs + ", totalEcts="
				+ totalEcts + ", casCount=" + casCount + ", masterThesis=" + masterThesis + ", promoPriority="
				+ promoPriority + ", start=" + start + ", description=" + description + ", urlWeb=" + urlWeb
				+ ", urlIcon=" + urlIcon + "]";
	}	
}
