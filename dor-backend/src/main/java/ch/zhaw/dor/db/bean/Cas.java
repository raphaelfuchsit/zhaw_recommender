package ch.zhaw.dor.db.bean;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@Entity
@Table(name = "CAS", schema = "RE_ENGINE")
public class Cas {

	@Id
	private long id;

	private String name;
	private String description;
	private Integer duration;
	private Double costs;
	private Integer ectsCount;
	private String language;
	private String campus;
	private String start;
	private Integer promoPriority;
	private String urlWeb;
	private String urlIcon;
	
//	@ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "cas_to_interest",
//      joinColumns = @JoinColumn(name = "cas_id", referencedColumnName="id"),
//      inverseJoinColumns = @JoinColumn(name = "interest_id", referencedColumnName="id"))
//    private Set<Question> interests;
//
//	@ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "cas_to_profession",
//      joinColumns = @JoinColumn(name = "cas_id", referencedColumnName="id"),
//      inverseJoinColumns = @JoinColumn(name = "profession_id", referencedColumnName="id"))
//    private Set<Profession> professions;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the duration
	 */
	public Integer getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	/**
	 * @return the costs
	 */
	public Double getCosts() {
		return costs;
	}
	/**
	 * @param costs the costs to set
	 */
	public void setCosts(Double costs) {
		this.costs = costs;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @return the campus
	 */
	public String getCampus() {
		return campus;
	}
	/**
	 * @param campus the campus to set
	 */
	public void setCampus(String campus) {
		this.campus = campus;
	}
	/**
	 * @return the start
	 */
	public String getStart() {
		return start;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(String start) {
		this.start = start;
	}
	/**
	 * @return the promoPriority
	 */
	public Integer getPromoPriority() {
		return promoPriority;
	}
	/**
	 * @param promoPriority the promoPriority to set
	 */
	public void setPromoPriority(Integer promoPriority) {
		this.promoPriority = promoPriority;
	}
	/**
	 * @return the urlWeb
	 */
	public String getUrlWeb() {
		return urlWeb;
	}
	/**
	 * @param urlWeb the urlWeb to set
	 */
	public void setUrlWeb(String urlWeb) {
		this.urlWeb = urlWeb;
	}
	/**
	 * @return the urlIcon
	 */
	public String getUrlIcon() {
		return urlIcon;
	}
	/**
	 * @param urlIcon the urlIcon to set
	 */
	public void setUrlIcon(String urlIcon) {
		this.urlIcon = urlIcon;
	}
	/**
	 * @return the ectsCount
	 */
	public Integer getEctsCount() {
		return ectsCount;
	}
	/**
	 * @param ectsCount the ectsCount to set
	 */
	public void setEctsCount(Integer ectsCount) {
		this.ectsCount = ectsCount;
	}
//	/**
//	 * @return the interests
//	 */
//	public Set<Question> getInterests() {
//		return interests;
//	}
//	/**
//	 * @param interests the interests to set
//	 */
//	public void setInterests(Set<Question> interests) {
//		this.interests = interests;
//	}
//	/**
//	 * @return the professions
//	 */
//	public Set<Profession> getProfessions() {
//		return professions;
//	}
//	/**
//	 * @param professions the professions to set
//	 */
//	public void setProfessions(Set<Profession> professions) {
//		this.professions = professions;
//	}
	
	@Override
	public String toString() {
		return "Cas [id=" + id + ", name=" + name + ", duration=" + duration + ", costs=" + costs + ", ectsCount="
				+ ectsCount + ", promoPriority=" + promoPriority + ", start=" + start + ", language=" + language
				+ ", campus=" + campus + ", urlWeb=" + urlWeb + ", urlIcon=" + urlIcon + ", description="
				+ description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cas other = (Cas) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
