package ch.zhaw.dor.db.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>Copyright: Copyright (c) 2019</p>
 * @author streuch1
 * @version 1.0
 */
@Entity
@Table(name = "QUESTION", schema = "RE_ENGINE")
public class Question {

    @Id
    private long id;
    
    private String type;
    private String dimension;
    private String filter_parameter;
    private String text;
	private Double dimension_importance;
    
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return
	 */
	public String getDimension() {
		return dimension;
	}
	/**
	 * @param dimension
	 */
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	/**
	 * @return
	 */
	public String getFilterParameter() {
		return filter_parameter;
	}

	/**
	 * @param filter_parameter
	 */
	public void setFilterParameter(String filter_parameter) {
		this.filter_parameter = filter_parameter;
	}

	/**
	 * @return
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return
	 */
	public Double getDimension_importance() {
		return dimension_importance;
	}
	/**
	 * @param dimension_importance
	 */
	public void setDimension_importance(Double dimension_importance) {
		this.dimension_importance = dimension_importance;
	}

	@Override
	public String toString() {
		return "QUESTION [id=" + id
				+ ", type=" + type
				+ ", dimension=" + dimension
				+ ", text=" + text
				+ ", dimension_importance = " + dimension_importance
				+ "]";
	}
}
