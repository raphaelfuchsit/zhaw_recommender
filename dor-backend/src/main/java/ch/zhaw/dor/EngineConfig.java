package ch.zhaw.dor;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import ch.zhaw.dor.rest.api.RecommenderEngineController.MailSender;
import ls13.productfinder.api.ProductFinderException;
import ls13.productfinder.api.RecommenderEngine;
import ls13.productfinder.core.RecommenderEngineImpl;
import ls13.productfinder.db.loader.JdbcKnowledgeBaseLoader;
import ls13.productfinder.db.loader.JdbcProductCatalogLoader;
import ls13.productfinder.interfaces.KnowledgeBaseLoader;
import ls13.productfinder.interfaces.ProductCatalogLoader;
import ls13.productfinder.xml.loader.XmlKnowledgeBaseLoader;
import ls13.productfinder.xml.loader.XmlProductCatalogLoader;

/**
 * <p>
 * Copyright: Copyright (c) 2019
 * </p>
 * 
 * @author streuch1
 * @version 1.0
 */
@Component
@ConfigurationProperties(prefix = "engine")
public class EngineConfig {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EngineConfig.class);
	
	private static final String KNOWLEDGE_BASE_DB = "knowledge-base-db";
	private static final String PRODUCT_CATALOG_DB = "product-catalog-db";
	
	private final ProductCatalog productCatalog = new ProductCatalog();
	
	private final KnowledgeBase knowledgeBase = new KnowledgeBase();
	
	private final MailConfig mailConfig = new MailConfig();
			
	/**
	 * @return the productCatalog
	 */
	public ProductCatalog getProductCatalog() {
		return productCatalog;
	}

	/**
	 * @return the knowledgeBase
	 */
	public KnowledgeBase getKnowledgeBase() {
		return knowledgeBase;
	}
	
	/**
	 * @return the mailConfig
	 */
	public MailConfig getMailConfig() {
		return mailConfig;
	}
	
	/**
	 * 
	 * @param knowledgeBaseLoader
	 * @param productCatalogLoader
	 * @return
	 * @throws ProductFinderException
	 */
	@Bean
	public RecommenderEngine createRecommenderEngine(KnowledgeBaseLoader knowledgeBaseLoader, ProductCatalogLoader productCatalogLoader) throws ProductFinderException {
		return new RecommenderEngineImpl(knowledgeBaseLoader, productCatalogLoader);
	}
	
	/**
	 * 
	 * @param javaMailSender 
	 * @return the mail sender
	 */
	@Bean()
	public MailSender getMailSender(JavaMailSender javaMailSender) {
		return new MailSender(javaMailSender, mailConfig);
	}
	
	@Bean(name = KNOWLEDGE_BASE_DB)
	@ConfigurationProperties(prefix="engine.knowledge-base.datasource")
	public DataSource kbDataSource() {
		LOGGER.debug("Creating knowledge base jdbc data source.");
		return DataSourceBuilder.create().build();
	}
	
	/**
	 * 
	 * @param knowledgeBase
	 * @return knowledge base loader
	 */
	@Bean()
	public KnowledgeBaseLoader createKnowledgeBaseLoader(@Qualifier(KNOWLEDGE_BASE_DB) DataSource dataSource) {
		if (StringUtils.trimToNull(knowledgeBase.loadSql) != null) {
			return new JdbcKnowledgeBaseLoader(dataSource, knowledgeBase.loadSql);
		}
		return new XmlKnowledgeBaseLoader(knowledgeBase.xmlFile);
	}

	@Bean(name = PRODUCT_CATALOG_DB)
	@Primary	// use as primary data source
	@ConfigurationProperties(prefix="engine.product-catalog.datasource")
	public DataSource pcDataSource() {
		LOGGER.debug("Creating product catalog jdbc data source.");
		return DataSourceBuilder.create().build();
	}
	
	/**
	 * 
	 * @param productCatalog
	 * @return product catalog loader
	 */
	@Bean()
	public ProductCatalogLoader createProductCatalogLoader(@Qualifier(PRODUCT_CATALOG_DB) DataSource dataSource) {
		if (StringUtils.trimToNull(productCatalog.loadSql) != null) {
			if (productCatalog.dbPath != null) {
				// TODO load derby db...
			}
			return new JdbcProductCatalogLoader(dataSource, productCatalog.loadSql);
		}
		return new XmlProductCatalogLoader(productCatalog.xmlFile);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*");
			}
		};
	}
	
	
	
	
	
	
	
	
	
	/**
	 * product catalog configuration
	 * 
	 * @author streuch1
	 *
	 */
	static class ProductCatalog {
		
		String xmlFile;
		String dbPath;
		String loadSql;

		/**
		 * @return the xmlFile
		 */
		public String getXmlFile() {
			return xmlFile;
		}
		/**
		 * @param xmlFile the xmlFile to set
		 */
		public void setXmlFile(String xmlFile) {
			this.xmlFile = xmlFile;
		}
		/**
		 * @return the dbPath
		 */
		public String getDbPath() {
			return dbPath;
		}
		/**
		 * @param dbPath the dbPath to set
		 */
		public void setDbPath(String dbPath) {
			this.dbPath = dbPath;
		}
		/**
		 * @return the loadSql
		 */
		public String getLoadSql() {
			return loadSql;
		}
		/**
		 * @param loadSql the loadSql to set
		 */
		public void setLoadSql(String loadSql) {
			this.loadSql = loadSql;
		}
	}
	
	
	
	
	/**
	 * knowledge base configuration
	 * 
	 * @author streuch1
	 *
	 */
	static class KnowledgeBase {
		
		String xmlFile;
		String loadSql;
		
		/**
		 * @return the xmlFile
		 */
		public String getXmlFile() {
			return xmlFile;
		}
		/**
		 * @param xmlFile the xmlFile to set
		 */
		public void setXmlFile(String xmlFile) {
			this.xmlFile = xmlFile;
		}
		/**
		 * @return the loadSql
		 */
		public String getLoadSql() {
			return loadSql;
		}
		/**
		 * @param loadSql the loadSql to set
		 */
		public void setLoadSql(String loadSql) {
			this.loadSql = loadSql;
		}
	}
	
	
	public static class MailConfig {
		
		private String sendTo;
		private String subject;
		private String text;
		
		/**
		 * @return the sendTo
		 */
		public String getSendTo() {
			return sendTo;
		}
		/**
		 * @param sendTo the sendTo to set
		 */
		public void setSendTo(String sendTo) {
			this.sendTo = sendTo;
		}
		/**
		 * @return the subject
		 */
		public String getSubject() {
			return subject;
		}
		/**
		 * @param subject the subject to set
		 */
		public void setSubject(String subject) {
			this.subject = subject;
		}
		/**
		 * @return the text
		 */
		public String getText() {
			return text;
		}
		/**
		 * @param text the text to set
		 */
		public void setText(String text) {
			this.text = text;
		}
		
		@Override
		public String toString() {
			return "Mail [sendTo=" + sendTo + ", subject=" + subject + ", text=" + text + "]";
		}
	}
}
