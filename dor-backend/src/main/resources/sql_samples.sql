-- user: re_engine
-- pwd:  myReEngineDb

select c.id, c.name, p.id, p.name from RE_ENGINE.CAS_PROFESSION cp
 inner join  re_engine.cas c
 on cp.cas_id = c.id
 inner join  re_engine.profession p
 on cp.profession_id = p.id
 
 
select c.id, c.name, i.id, i.name from RE_ENGINE.CAS_INTEREST ci
 inner join  re_engine.cas c
 on ci.cas_id = c.id
 inner join  re_engine.INTEREST i
 on ci.interest_id = i.id
 
 
 -- query to populate the product catalog
select cas p_cas, min(mas) p_mas, min(bundle) p_bundle, min(professions) p_professions, min(interests) p_interests from ( 

SELECT c.name cas, m.name mas, b.name bundle, null professions, null interests FROM RE_ENGINE.CAS c
   inner join RE_ENGINE.CAS_TO_BUNDLE cb
   on c.id = cb.cas_id
   inner join RE_ENGINE.CAS_BUNDLE b
   on cb.bundle_id = b.id
   inner join RE_ENGINE.MAS m
   on b.mas_id = m.id

union all

SELECT c.name, null, null, LISTAGG(p.name, '#') professions, null FROM RE_ENGINE.CAS c
   inner join RE_ENGINE.CAS_TO_PROFESSION cp
   on c.id = cp.cas_id
   inner join RE_ENGINE.PROFESSION p 
   on cp.profession_id = p.id
group by c.name

union all

SELECT c.name, null, null, null, LISTAGG(i.name, '#') interests FROM RE_ENGINE.CAS c
   inner join RE_ENGINE.CAS_TO_INTEREST ci
   on c.id = ci.cas_id
   inner join RE_ENGINE.INTEREST i
   on ci.interest_id = i.id
group by c.name

) 

group by cas