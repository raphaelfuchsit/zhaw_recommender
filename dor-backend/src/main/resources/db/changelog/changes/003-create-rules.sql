--changeset streuch1:3

INSERT INTO config_rules (id, rules_xml) VALUES (7, '<?xml version="1.0" encoding="utf-8"?>
<RecommendationRules xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<!-- PRODUCT PROPERTIES -->
<ProductProperty name="p_type" type="String"/> <!-- product type: [''CAS'',''MAS''] -->
<ProductProperty name="p_id" type="long"/> <!-- product id -->
<ProductProperty name="p_name" type="String"/> <!-- product name -->
<ProductProperty name="p_promo_priority" type="int"/> <!-- products promotion priority, active when value > 0 -->
<ProductProperty name="p_req_professions" type="String[]"/> <!-- For p_type = ''MAS'', contains only professions of mandatory CASs. For p_type = ''CAS'', all entries are listed. -->
<ProductProperty name="p_req_interests" type="String[]"/> <!-- For p_type = ''MAS'', contains only interests of mandatory CASs. For p_type = ''CAS'', all entries are listed. -->
<ProductProperty name="p_opt_professions" type="String[]"/> <!-- only set for p_type = ''MAS'', contains professions of optional CASs -->
<ProductProperty name="p_opt_interests" type="String[]"/> <!-- only set for p_type = ''MAS'', contains interests of optional CASs -->

<!-- USER REQUIREMENTS -->
<UserRequirement name="c_pref_type" type="String"/>
<UserRequirement name="c_pref_ids" type="long[]"/>
<UserRequirement name="c_pref_profession" type="String"/>
<!-- users profession rating: [''not interested (-1)'', ''no preference (0)'', ''very interested (1)''] -->
<UserRequirement name="c_pref_profession_rating" type="int"/>  
<UserRequirement name="c_pref_interests_liked" type="Map[String,Integer]"/> <!-- only set when user rated interests positive, meaning [''ziemlich (1)'', ''sehr (2)''] -->
<UserRequirement name="c_pref_interests_disliked" type="Map[String,Integer]"/> <!-- only set when user rated interests negative, meaning [''wenig (-1)'', ''gar nicht (-2)''] -->

<!-- c_pref_profession_experience contains experience in current job:
	   -1 - not available
	    3 - up to 3 years
	    7 - up to 7 years
	   12 - up to 12 years
	   99 - more than 12 years -->
<UserRequirement name="c_pref_profession_experience" type="int"/> 
<UserRequirement name="c_pref_gender" type="String"/>

<!-- FILTERING RULES -->

<!-- INTERNAL PRODUCT FILTERS -->
<!-- note: all internal filters must be marked as forced using priority = -1 to disabled relaxation -->
<FilterRule 
	name="f001_internal_product_type"
	condition="c_pref_type != null" 
	filter="c_pref_type == p_type"
	priority="-1"
	explanation="Interner Filter für Product Typ: ''MAS'' oder ''CAS''"
/>
<FilterRule 
	name="f002_internal_product_ids"
	condition="c_pref_ids != null" 
	filter="containsNumber(p_id, c_pref_ids)"
	priority="-1"
	explanation="Internal Filter für Product IDs"
/>

<!-- PROFESSION FILTERS -->
<FilterRule 
	name="f100_req_profession"
	condition="c_pref_profession != null &amp;&amp; c_pref_profession_rating > 0" 
	filter="containsString(c_pref_profession, p_req_professions)"
	priority="100"
	explanation="if (c_pref_type != ''MAS'') { '''' } else { ''Dieser MAS entspricht Ihrem aktuellen Berufsfeld: '' + c_pref_profession + '' und ermöglicht Ihnen den nächsten Karriereschritt.'' }"
	excuse=""
/>
<FilterRule 
	name="f101_req_profession_not"
	condition="c_pref_profession != null &amp;&amp; c_pref_profession_rating &lt; 0" 
	filter="!containsString(c_pref_profession, p_req_professions)"
	priority="30"
	explanation="if (c_pref_type != ''MAS'') { '''' } else { ''Sie sind auf der Suche nach Veränderung. Dieser MAS ermöglicht Ihnen eine Karriere in einem neuen Berufsfeld.'' }"
	excuse=""
/>
<FilterRule 
	name="f102_opt_professions"
	condition="c_pref_profession != null &amp;&amp; c_pref_profession_rating > 0 &amp;&amp; c_pref_type == ''MAS''" 
	filter="containsString(c_pref_profession, p_opt_professions)"
	priority="70"
	explanation="if (c_pref_type != ''MAS'') { '''' } else { ''Dieser MAS entspricht Ihrem aktuellen Berufsfeld: '' + c_pref_profession + '' und ermöglicht Ihnen den nächsten Karriereschritt.'' }"
	excuse=""
/>
<FilterRule 
	name="f103_opt_professions_not"
	condition="c_pref_profession != null &amp;&amp; c_pref_profession_rating &lt; 0 &amp;&amp; c_pref_type == ''MAS''" 
	filter="!containsString(c_pref_profession, p_opt_professions)"
	priority="10"
	explanation="if (c_pref_type != ''MAS'') { '''' } else { ''Sie sind auf der Suche nach Veränderung. Dieser MAS ermöglicht Ihnen eine Karriere in einem neuen Berufsfeld.'' }"
	excuse=""
/>

<!-- INTEREST FILTERS -->
<FilterRule 
	name="f200_req_interests_liked"
	condition="c_pref_interests_liked != null" 
	filter="calculateScore(c_pref_interests_liked, p_req_interests) > 0"
	priority="90"
	explanation="if (c_pref_type != ''CAS'') { '''' } else { ''Dieser '' + c_pref_type + '' passt gut zu Ihren Interessen: '' + printKeysIn(c_pref_interests_liked, p_req_interests) }"
	excuse=""
/>
<FilterRule 
	name="f201_req_interests_disliked"
	condition="c_pref_interests_disliked != null" 
	filter="calculateScore(c_pref_interests_disliked, p_req_interests) == 0"
	priority="20"
	explanation="Diese Weiterbildung beinhaltet keine Interessen die Sie nicht mögen."
	excuse=""
/>
<FilterRule 
	name="f202_opt_interests_liked"
	condition="c_pref_interests_liked != null &amp;&amp; c_pref_type == ''MAS''" 
	filter="calculateScore(c_pref_interests_liked, p_opt_interests) > 0"
	priority="60"
	explanation="if (c_pref_type != ''CAS'') { '''' } else { ''Dieser '' + c_pref_type + '' passt gut zu Ihren Interessen: '' + printKeysIn(c_pref_interests_liked, p_opt_interests) }"
	excuse=""
/>
<FilterRule 
	name="f203_opt_interests_disliked"
	condition="c_pref_interests_disliked != null &amp;&amp; c_pref_type == ''MAS''" 
	filter="calculateScore(c_pref_interests_disliked, p_opt_interests) == 0"
	priority="1"
	explanation="Diese Weiterbildung beinhaltet keine Interessen die Sie nicht mögen."
	excuse=""
/>

<!-- PERSONALIZED INFORMATION TEXTS -->

<InfoText name="profession_experience" order="1" defaulttext="">
	<TextVariant condition="c_pref_profession_experience == null" text="Sie haben keine Angaben zu Ihrer beruflichen Erfahrung gemacht, unabhängig davon wird Ihnen eine Weiterbildung an der ZHAW neue Kenntnisse vermitteln und wichtige Vorteile generieren." />
	<TextVariant condition="c_pref_profession_experience &lt;= 3" text="Möchten Sie Ihrer jungen Karriere einen Boost geben? Dieser MAS vermittelt Ihnen neue Grundlagen und öffnet neue Türen für Sie." />
	<TextVariant condition="c_pref_profession_experience &lt;= 7" text="Sie konnten sich bereits in Ihrem Beruf beweisen und etablieren. Sind Sie bereit für neue spannende Aufgaben? Dieser MAS bereitet Sie mit neuen Theorien und Konzepten auf diese vor."/>
	<TextVariant condition="c_pref_profession_experience > 7" text="Sie verfügen über eine langjährige Berufserfahrungen, sind Sie dennoch offen für neues? Mit diesem MAS haben Sie die Möglichkeit, ihr Wissen wieder aufzufrischen und neue Konzepte zu erlernen."/>
</InfoText>

<InfoText name="interests_description" order="2" defaulttext="Sie haben leider keine Angaben bezüglich Ihrer Interessen gemacht. Trotzdem wurde für Sie ein bedeutsamer MAS mit wissenswerten CAS ausgewählt.">
	<TextVariant condition="c_pref_interests_liked != null" text="''Ihre Vorliebe für folgende Interessen: '' + printKeys(c_pref_interests_liked) + '' wurde erkannt.''" />
	<TextVariant condition="c_pref_interests_disliked != null" text="''Bei der Auswahl wird berücksichtigt, dass Sie sich für folgenden Themen: '' + printKeys(c_pref_interests_disliked) + '' nicht interessieren.''" />
</InfoText>

<!-- FUNCTION DEFINITIONS -->
<!-- user print("my debug message"); to log messages from a javascript function -->

<!-- UTILITY LIBRARY -->
<UtilityFunction type="dynamic">
<![CDATA[
	function __utility() {
	
		var interests_score = 50;
		if (c_pref_interests_liked != null) {
			interests_score += 20 * calculateScore(c_pref_interests_liked, p_req_interests);
			interests_score += 10 * calculateScore(c_pref_interests_liked, p_opt_interests);
		}
	    if (c_pref_interests_disliked != null) {
			if (calculateScore(c_pref_interests_disliked, p_req_interests) == 0 
					&& calculateScore(c_pref_interests_disliked, p_opt_interests) == 0) {
				interests_score += 20;
			} else {
				interests_score += -20;
			}
		}
		
		var score = 0;
		if (c_pref_type == ''MAS'') {
			var profession_score = 50;
			if (c_pref_profession != null) {
				if (containsString(c_pref_profession, p_req_professions)) {
					profession_score += 50 * c_pref_profession_rating;
				} else if (c_pref_profession_rating > 0 && containsString(c_pref_profession, p_opt_professions)) {
					profession_score += 50 * c_pref_profession_rating;
				} else {
					profession_score += -50 * c_pref_profession_rating;
				}
			}
			
			if (profession_score == 50) {
				score = interests_score;
			} else if (interests_score == 50) {
				score = profession_score;
			} else {
				score = (2 * profession_score + interests_score) / 3;
			}
		
		} else {
			score = interests_score;
		}
	    
	    if (score > 100) {
	    	return 100;
	    }
	    if (score < 0) {
	    	return 0;
	    }
	    return score;
	}
]]>
</UtilityFunction>


<!-- FUNCTION LIBRARY -->

<LibraryFunction name="printKeys" >
<![CDATA[
	function printKeys(map) {
		if (map == null) { return ''''; }
		var s = '''';
		var i = 0;
		var l = map.keySet().size();
		for each (var pKey in map.keySet()) {
			i++;
			if (i == l) {
				s += '' und '';
			} else if (i > 1) {
		  		s += '', '';
		  	}
			s += pKey;
		}
		return s;
	}
]]>
</LibraryFunction>

<LibraryFunction name="printKeysIn" >
<![CDATA[
	function printKeysIn(map,pArray) {
		if (map == null || pArray == null) {return '''';}
		var s = '''';
		var i = 0;
		for each (var pKey in map.keySet()) {
		  if (pKey != null) {
		  		for (var j=0;j<pArray.length;j++) {
		  			if (pKey.equalsIgnoreCase(pArray[j])) {
		  			    if (i > 0) {
		  			    	s += '', '';
		  			    }
					   	s += pKey;
					   	i++;
					   	break;
					}
				}
		  }
		}
		return s;
	}
]]>
</LibraryFunction>

<LibraryFunction name="containsString" >
<![CDATA[
	function containsString(s,sArray) {
		if (s == null || sArray == null) {return false;}
		for (var i=0;i<sArray.length;i++) {
			if (s.equalsIgnoreCase(sArray[i])) {return true;}
		}
		return false;
	}
]]>
</LibraryFunction>

<LibraryFunction name="containsNumber" >
<![CDATA[
	function containsNumber(s,sArray) {
		if (s == null || sArray == null) {return false;}
		for (var i=0;i<sArray.length;i++) {
			if (s == sArray[i]) {return true;}
		}
		return false;
	}
]]>
</LibraryFunction>

<LibraryFunction name="calculateScore" >
<![CDATA[
	function calculateScore(pMap, pArray) {
		if (pMap == null || pArray == null) {return 0;}
		var score = 0;
		for each (var pKey in pMap.keySet()) {
		  var pRating = pMap.get(pKey);
		  if (pRating != null && pKey != null) {
		  		for (var j=0;j<pArray.length;j++) {
		  			if (pKey.equalsIgnoreCase(pArray[j])) {
					   	score += pRating;
						break;
					}
				}
		  }
		}
		return score;
	}
]]>
</LibraryFunction>

</RecommendationRules>');