# Deployment and Notices
## Important Links
Tutorial for Java SpringBoot: https://spring.io/guides/gs/rest-service/

BootRun in Intellj to Debug: https://www.baeldung.com/spring-debugging#:~:text=IntelliJ%20offers%20first%2Dclass%20support,a%20Run%20Dashboard%20tool%20window.

Development live reload: https://stackoverflow.com/questions/52092504/spring-boot-bootrun-with-continuous-build

## Questions

- Wo werden die SQL-Files geladen?


# dor-backend

digital online recommender engine

## heroku deploment

Username: zhaw.dor.mailer2@gmail.com
Password: asfsad.fDFSDfi33

1. checkout heroku git repository

git clone https://git.heroku.com/zhaw-dor-backend.git

2. run deployment using heroku_deploy.sh script:

./dor-backend/heroku_deploy.sh local

## dor-backend rest api

Es wird automatisch eine Dokumentation der REST API generiert (Swagger und OpenAPI) und unter folgendem Link zur Verfügung gestellt: 

http://localhost:8080/swagger-ui.html


REST Calls:

1. Create a new recommender session

curl -X GET http://localhost:8080/recommender/createSession

result: session token, e.g.: 2

Set the session token as request header "re-session-token" to reference this session in all API calls


2. Get a list of all professions

curl -X GET http://localhost:8080/recommender/professions

result:

["Agile Rollen (Scrum Master, Product Owner)","Business Analyse","IT-Leadership","Innovationsmanagement","Projektmanagement","Requirements-Engineering","Software Engineering","Solution Design","Unternehmensentwicklung"]

3. Init recommender session

curl -H "Content-Type: application/json" -X POST http://localhost:8080/recommender/initSession -d '{
	"profession": "myJob",
	"professionExperience": 3,
	"professionRating": 1,
	"gender": null
}'

4. Get a list of all interests

curl -X GET http://localhost:8080/recommender/interests?page=1&size=5

result:

{ 
"page":1,
"hasResult":false,
"lastInterests":false,
"interests":[ 
	{ 
	"id":13,
	"name":"Innovation",
	"text":null,
	"description":null,
	"rating":-1
	},
	{ 
	"id":15,
	"name":"Lösungsaufbau",
	"text":null,
	"description":null,
	"rating":-1
	},
	{ 
	"id":14,
	"name":"Lösungskonzeption",
	"text":"Lösen Sie gerne komplexe Problemstellungen?",
	"description":null,
	"rating":-1
	},
	{ 
	"id":1,
	"name":"Anforderungsanalyse",
	"text":"Analysieren Sie gerne Anforderungen?",
	"description":null,
	"rating":-1
	},
	{ 
	"id":2,
	"name":"Projektmanagement",
	"text":"Haben Sie Interesse an der Leitung von Projekten?",
	"description":null,
	"rating":-1
	}
]
}

5. Update recommender session with interests

curl -H "Content-Type: application/json" -X POST http://localhost:8080/recommender/updateSession -d '[
	{"id":13,"name":"Innovation","text":null,"rating":2},
	{"id":12,"name":"Unternehmensführung","text":null,"rating":2},
	{"id":6,"name":"Prozessoptimierung","text":null,"rating":4},
	{"id":21,"name":"IT-Governance","text":null,"rating":4},
	{"id":4,"name":"Prozessmodellierung","text":null,"rating":0}
]'

6. Get the recommendation result

curl -X GET http://localhost:8080/recommender/result

result: 

{ 
"id":1,
"name":"IT-Leadership und TechManagement",
"description":"dummy text",
"urlWeb":"https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/mas-it-leadership-und-techmanagement",
"urlIcon":"https://www.zhaw.ch/storage/_processed_/1/6/csm_sml_wb_mas_it_leadership_produktbild_9a2d7b5b7d.jpg",
"score":100,
"explanations":[ 
	"Platzhalter 1 Begründung",
	"Platzhalter 2 Begründung",
	"Platzhalter 2 Begründung"
],
"mandatoryBundle":{ 
	"name":"Pflicht-CAS",
	"recommendations":[ 
		{ 
		"id":1,
		"name":"IT-Leadership und innovative Organisationen",
		"description":null,
		"urlWeb":"https://www.zhaw.ch/de/sml/weiterbildung/detail/kurs/cas-it-leadership-und-innovative-organisationen/",
		"urlIcon":null,
		"score":100,
		"explanations":[ 
			"Platzhalter 1 Begründung",
			"Platzhalter 2 Begründung",
			"Platzhalter 2 Begründung"
		]
		},
		{ 
		"id":2,
		"name":"Strategisches IT- und TechManagement",
		"description":null,
		"urlWeb":"https://my-dummy-url.com",
		"urlIcon":null,
		"score":100,
		"explanations":[ 
		"Platzhalter 1 Begründung",
		"Platzhalter 2 Begründung",
		"Platzhalter 2 Begründung"
		]
		}
	],
	"options":null
},
"optionBundles":[ 
	{ 
	"name":"Wahlpflicht-CAS",
	"recommendations":[ 
		{ 
		"id":3,
		"name":"IT-Sourcing und Cloud Provider Management",
		"description":null,
		"urlWeb":"https://my-dummy-url.com",
		"urlIcon":null,
		"score":100,
		"explanations":[ 
			"Platzhalter 1 Begründung",
			"Platzhalter 2 Begründung",
			"Platzhalter 2 Begründung"
		]
		},
		{ 
		"id":4,
		"name":"Cyber Security",
		"description":null,
		"urlWeb":"https://my-dummy-url.com",
		"urlIcon":null,
		"score":100,
		"explanations":[ 
			"Platzhalter 1 Begründung",
			"Platzhalter 2 Begründung",
			"Platzhalter 2 Begründung"
		]
		}
	],
	"options":[ 
		{ 
		"id":5,
		"name":"Psychologie in der Arbeitswelt 4.0",
		"description":null,
		"urlWeb":"https://my-dummy-url.com",
		"urlIcon":null,
		"score":100,
		"explanations":[ 
		"Platzhalter 1 Begründung",
		"Platzhalter 2 Begründung",
		"Platzhalter 2 Begründung"
		]
		},
		{ 
		"id":6,
		"name":"Agiles IT-Projektmanagement",
		"description":null,
		"urlWeb":"https://my-dummy-url.com",
		"urlIcon":null,
		"score":100,
		"explanations":[ 
		"Platzhalter 1 Begründung",
		"Platzhalter 2 Begründung",
		"Platzhalter 2 Begründung"
		]
		},
		{ 
		"id":7,
		"name":"Digitale Strategie und Wertschöpfung",
		"description":null,
		"urlWeb":"https://my-dummy-url.com",
		"urlIcon":null,
		"score":100,
		"explanations":[ 
		"Platzhalter 1 Begründung",
		"Platzhalter 2 Begründung",
		"Platzhalter 2 Begründung"
		]
		},
		{ 
		"id":8,
		"name":"Digitale Technologien und Innovationen",
		"description":null,
		"urlWeb":"https://my-dummy-url.com",
		"urlIcon":null,
		"score":100,
		"explanations":[ 
		"Platzhalter 1 Begründung",
		"Platzhalter 2 Begründung",
		"Platzhalter 2 Begründung"
		]
		}
	]
	},
	{ 
	"name":"Wahlpflicht-CAS 2",
	"recommendations":[ 
		{ 
		"id":7,
		"name":"Digitale Strategie und Wertschöpfung",
		"description":null,
		"urlWeb":"https://my-dummy-url.com",
		"urlIcon":null,
		"score":100,
		"explanations":[ 
		"Platzhalter 1 Begründung",
		"Platzhalter 2 Begründung",
		"Platzhalter 2 Begründung"
		]
		}
	],
	"options":[ 
	{ 
	"id":8,
	"name":"Digitale Technologien und Innovationen",
	"description":null,
	"urlWeb":"https://my-dummy-url.com",
	"urlIcon":null,
	"score":100,
	"explanations":[ 
	"Platzhalter 1 Begründung",
	"Platzhalter 2 Begründung",
	"Platzhalter 2 Begründung"
	]
	}
	]
	}
]
}

7. Update recommender session with user qualification

curl -H "Content-Type: application/json" -X POST http://localhost:8080/recommender/updateQualification -d '{
	"graduation": "FH/Uni",
	"professionalExperience": true,
	"preCourse": false
}'

8. Submit recommender result

curl -H "Content-Type: application/json" -X POST http://localhost:8080/recommender/submit -d '{
	"email": "dummy.mail@web.ch",
	"firstName": "Reto",
	"lastName": "Suter",
	"comment": "my comment"
}'
