#!/bin/bash
APP="dor-backend"
JPFINDER="jpfinder"

# vars for local heroku deployment
eval LOCAL_HEROKU_REPO="~/heroku/zhaw-dor-backend"
eval LOCAL_GIT_REPO="~/git/digital-online-recommender"

echo "creating heroku artefact for app: $APP"

if [ "$1" == "" ]; then
	cp -vp $APP/heroku/* .
	tar -czvf $APP.tgz *.gradle gradle* $APP/src/ $APP/build.gradle $JPFINDER/src/ $JPFINDER/build.gradle Procfile application.properties logback-spring.xml
else
	# cleanup heroku app directory
	echo "starting local heroku deployment using heroku app directory: $LOCAL_HEROKU_REPO"
	rm -R $LOCAL_HEROKU_REPO/*
	
	# prepare heroku app directory
	mkdir -p $LOCAL_HEROKU_REPO/$APP/src/
	mkdir -p $LOCAL_HEROKU_REPO/$JPFINDER/src/
	
	cp -vp  $LOCAL_GIT_REPO/*.gradle $LOCAL_HEROKU_REPO/
	cp -vRp $LOCAL_GIT_REPO/gradle* $LOCAL_HEROKU_REPO/
	cp -vRp $LOCAL_GIT_REPO/$APP/src $LOCAL_HEROKU_REPO/$APP/
	cp -vp  $LOCAL_GIT_REPO/$APP/build.gradle $LOCAL_HEROKU_REPO/$APP/
	cp -vp  $LOCAL_GIT_REPO/$APP/heroku/* $LOCAL_HEROKU_REPO/
	cp -vRp $LOCAL_GIT_REPO/$JPFINDER/src $LOCAL_HEROKU_REPO/$JPFINDER/
	cp -vp  $LOCAL_GIT_REPO/$JPFINDER/build.gradle $LOCAL_HEROKU_REPO/$JPFINDER/
	
    echo "going to deploy app: $APP"
    
    # starting heroku deployment
    cd $LOCAL_HEROKU_REPO
    git add .
    git commit . -m "deployment dor-backend"
    git push https://git.heroku.com/zhaw-dor-backend.git master
fi

echo "created heroku artefact for app: $APP"

exit 0